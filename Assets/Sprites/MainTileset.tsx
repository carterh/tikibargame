<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.1" name="MainTileset" tilewidth="16" tileheight="16" tilecount="256" columns="16">
 <image source="Tilemap.png" width="258" height="258"/>
 <tile id="1">
  <animation>
   <frame tileid="1" duration="250"/>
   <frame tileid="17" duration="250"/>
  </animation>
 </tile>
 <tile id="3">
  <animation>
   <frame tileid="3" duration="250"/>
   <frame tileid="4" duration="250"/>
  </animation>
 </tile>
 <tile id="6">
  <animation>
   <frame tileid="6" duration="200"/>
   <frame tileid="7" duration="200"/>
  </animation>
 </tile>
 <tile id="17">
  <animation>
   <frame tileid="17" duration="250"/>
   <frame tileid="1" duration="250"/>
  </animation>
 </tile>
 <tile id="18">
  <properties>
   <property name="ItemName" value="Bar"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4" width="16" height="12"/>
  </objectgroup>
 </tile>
 <tile id="34">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="50">
  <properties>
   <property name="ItemName" value="MargGlass"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="1" y="0" width="14" height="16"/>
  </objectgroup>
 </tile>
 <tile id="65">
  <properties>
   <property name="ItemName" value="GreenMarg"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="66">
  <properties>
   <property name="ItemName" value="RedMarg"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="83">
  <properties>
   <property name="ItemName" value="Rum"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="84">
  <properties>
   <property name="ItemName" value="Vodka"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="85">
  <properties>
   <property name="ItemName" value="TallGlass"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="1" y="0" width="14" height="16"/>
  </objectgroup>
 </tile>
 <tile id="86">
  <properties>
   <property name="ItemName" value="Fridge"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="2" y="0" width="12" height="16"/>
  </objectgroup>
 </tile>
 <tile id="87">
  <properties>
   <property name="ItemName" value="ShotGlass"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="1" y="1.625" width="14" height="14.375"/>
  </objectgroup>
 </tile>
 <tile id="88">
  <properties>
   <property name="ItemName" value="Fridge"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4" width="16" height="12"/>
  </objectgroup>
 </tile>
 <tile id="96">
  <properties>
   <property name="ItemName" value="Coconut"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="1" width="16" height="15"/>
  </objectgroup>
 </tile>
 <tile id="97">
  <properties>
   <property name="ItemName" value="Knife"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="5" width="16" height="11"/>
  </objectgroup>
 </tile>
 <tile id="98">
  <properties>
   <property name="ItemName" value="Register"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="100">
  <properties>
   <property name="ItemName" value="Juice"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="101">
  <properties>
   <property name="ItemName" value="Pineapple"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="102">
  <properties>
   <property name="ItemName" value="Blue"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="1" width="16" height="15"/>
  </objectgroup>
 </tile>
 <tile id="104">
  <properties>
   <property name="ItemName" value="Fridge"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="112">
  <properties>
   <property name="ItemName" value="Lime"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="1" width="16" height="15"/>
  </objectgroup>
 </tile>
 <tile id="113">
  <properties>
   <property name="ItemName" value="Umbrella"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="1" width="16" height="15"/>
  </objectgroup>
 </tile>
 <tile id="115">
  <properties>
   <property name="ItemName" value="Trash"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="2" y="4" width="12" height="12"/>
  </objectgroup>
 </tile>
</tileset>
