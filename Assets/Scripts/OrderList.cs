using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderList : MonoBehaviour
{
    public List<GameObject> masterList;
    public List<GameObject> orders;
    // Start is called before the first frame update
    void Start()
    {
        DataManager dm = GameObject.Find("DataManager").GetComponent<DataManager>();
        int orderSize = 1;
        // Day is 0 indexed. This is all 1 behind the actual day.
        if (dm.day == 1)
        {
            orderSize = 3;
        }
        else if (dm.day == 2)
        {
            orderSize = 5;
        }
        else if (dm.day == 3)
        {
            orderSize = 6;
        }
        else if (dm.day == 4)
        {
            orderSize = 7;
        }
        else if (dm.day == 5)
        {
            orderSize = 8;
        }
        else if (dm.day == 6)
        {
            orderSize = 9;
        }
        else if (dm.day == 7)
        {
            orderSize = 15;
        }

        for (int i = 0; i < orderSize; ++i)
        {
            orders.Add(masterList[i]);
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
