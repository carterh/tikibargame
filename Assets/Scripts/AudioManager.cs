using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioManager : MonoBehaviour
{
    public AudioClip pickUp;
    public AudioClip putDown;
    public AudioClip error;
    private AudioSource audioSource;
    public UnityEvent e_pickUp;
    public UnityEvent e_putDown;
    public UnityEvent e_error;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        e_pickUp.AddListener(() => PlayAudio(pickUp));
        e_putDown.AddListener(() => PlayAudio(putDown));
        e_error.AddListener(() => PlayAudio(error));
        GameObject.Destroy(GameObject.Find("Music"));
    }

    private void PlayAudio(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}
