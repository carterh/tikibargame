using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSwapper : MonoBehaviour
{
    Vector2 previousPosition;
    public Sprite up;
    public Sprite down;
    public Sprite left;
    public Sprite right;
    SpriteRenderer renderer;
    // Start is called before the first frame update
    public enum Direction{
        Up,
        Down,
        Left,
        Right
    }
    public bool autoswapEnabled;

    void Start()
    {
        previousPosition = transform.position;
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!autoswapEnabled)
        {
            return;
        }
        Vector2 currentPosition = transform.position;
        
        if (currentPosition.y < previousPosition.y)
        {
            SwapSprite(Direction.Down);
        }
        else if (currentPosition.y > previousPosition.y)
        {
            SwapSprite(Direction.Up);
        }
        else if (currentPosition.x < previousPosition.x)
        {
            SwapSprite(Direction.Left);
        }
        else if (currentPosition.x > previousPosition.x)
        {
            SwapSprite(Direction.Right);
        }

        previousPosition = currentPosition;
    }

    public void SwapSprite(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up:
                renderer.sprite = up;
            break;
            case Direction.Left:
                renderer.sprite = left;
            break;
            case Direction.Right:
                renderer.sprite = right;
            break;
            default:
                renderer.sprite = down;
            break;
        }

    }
}
