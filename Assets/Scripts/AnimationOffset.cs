using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationOffset : MonoBehaviour
{
    public string stateName;
    // Start is called before the first frame update
    void Start()
    {
        Animator anim = GetComponent<Animator>();
        anim.Play(stateName, 0, Random.Range(0, anim.GetCurrentAnimatorStateInfo(0).length));

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
