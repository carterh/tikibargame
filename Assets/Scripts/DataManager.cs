using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public int day;
    public bool endless = false;
    public enum Dificulty
    {
        easy,
        normal,
        hard
    }
    public Dificulty dificulty;
    // Start is called before the first frame update
    void Awake()
    {
        if (GameObject.FindGameObjectsWithTag("DataManager").Length > 1)
        {
            GameObject.Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
