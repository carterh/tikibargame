using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UIManager : MonoBehaviour
{
    private PositionManager positionManager;
    public GameObject pickup;
    public GameObject action;
    // Start is called before the first frame update
    void Start()
    {
        positionManager = GameObject.Find("PositionManager").GetComponent<PositionManager>();
        positionManager.e_lookingAt.AddListener((s, _) => UpdateHelper(s));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateHelper(string s)
    {
        switch(s)
        {
            case "":
                action.SetActive(false);
                pickup.SetActive(false);
                break;
            case "Fridge":
            case "MargGlass":
            case "ShotGlass":
            case "TallGlass":
            case "Coconut":
                pickup.SetActive(true);
                action.SetActive(false);
                break;
            default:
                pickup.SetActive(false);
                action.SetActive(true);
                break;
        }

    }
}
