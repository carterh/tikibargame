using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.SceneManagement;

public class WinManager : MonoBehaviour
{
    public PixelPerfectCamera cam;
    public bool isWin;
    public float zoomRate;
    public float ppu = 0;
    public GameObject winCanvas;

    public LightingManager lightingManager;
    // Start is called before the first frame update
    void Start()
    {
        ppu = cam.assetsPPU;
    }

    // Update is called once per frame
    void Update()
    {
        if (isWin && cam.assetsPPU > 1)
        {
            ppu -= Time.deltaTime * zoomRate;
            cam.assetsPPU = Mathf.RoundToInt(ppu);
            if (ppu <= 300)
            {
                winCanvas.SetActive(true);
            }

            if (ppu <= 2)
            {
                GameObject.Destroy(GameObject.FindGameObjectWithTag("Music"));
                SceneManager.LoadScene("MainMenu");
            }
        }
    }

    public void Go()
    {
        isWin = true;
        lightingManager.lightSpeed = 0.05f;
    }
}
