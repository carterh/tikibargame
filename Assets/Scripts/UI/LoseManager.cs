using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseManager : MonoBehaviour
{
    public float step1Wait;
    public float step2Wait;

    private float timer = 0;
    private bool step1 = true;
    public GameObject step2Obj;

    // Update is called once per frame
    void Update()
    {
        if (step1)
        {
            if (timer > step1Wait)
            {
                step1 = false;
                timer = 0;
                step2Obj.SetActive(true);
            }
        }
        else
        {
            if (timer > step2Wait)
            {
                SceneManager.LoadScene("MainMenu");
            }
        }

        timer += Time.deltaTime;
    }
}
