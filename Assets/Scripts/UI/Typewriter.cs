using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Typewriter : MonoBehaviour
{
    private TMP_Text tmp;
    private string text;
    public float speed;
    private float timer;
    private int charCount;
    // Start is called before the first frame update
    void Awake()
    {
        tmp = GetComponent<TMP_Text>();
        text = tmp.text; 
        tmp.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (timer >= speed && charCount < text.Length)
        {
            charCount++;
            timer = 0;
            UpdateText();
        }
        timer += Time.deltaTime;
    }

    void UpdateText()
    {
        string newText = "";
        for (int i = 0; i < charCount; i++)
        {
            newText += text[i];
        }
        tmp.text = newText;
    }

    public bool IsDone()
    {
        return charCount == text.Length;
    }

    public void Complete()
    {
        charCount = text.Length;
        UpdateText();
    }
}
