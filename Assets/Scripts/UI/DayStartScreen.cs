using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayStartScreen : MonoBehaviour
{
    public List<GameObject> days;

    public List<GameObject> expositions;

    private int day;
    // Start is called before the first frame update
    void Start()
    {
        day =  GameObject.Find("DataManager").GetComponent<DataManager>().day;
        expositions[day].SetActive(true);
    }

    public bool MoveToDay()
    {
        Typewriter expos = expositions[day].GetComponent<Typewriter>();
        if (expos.IsDone())
        {
            expositions[day].SetActive(false);
            days[day].SetActive(true);
            return false;
        }
        else
        {
            expos.Complete();
            return true;
        }
    }
}
