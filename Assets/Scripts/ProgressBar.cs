using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProgressBar : MonoBehaviour
{
    public List<Sprite> sprites;
    public SpriteRenderer bar;
    private float countdownSpeed = 1.00f;
    private float timer;
    private int index;
    private bool counting;
    private bool increase;
    public UnityEvent e_patienceOut;

    // Update is called once per frame
    void Update()
    {
       if (counting && timer >= countdownSpeed) 
       {
            if (increase && index > 0)
            {
                index-=2;
            }
            else
            {
                index++;
            }
            timer = 0;
            bar.sprite = sprites[index];
       }
       if (index == sprites.Count - 1)
       {
            e_patienceOut.Invoke();
       }
       timer += Time.deltaTime;
    }

    public void StartCountdown()
    {
        if (!counting)
        {
            counting = true;
            index = 0;
            bar.gameObject.SetActive(true);
        }
    }
    public void StopCountdown()
    {
        if (counting)
        {
            counting = false;
            index = 0;
            bar.gameObject.SetActive(false);
        }
    }

    public void StartIncrease()
    {
        increase = true;
    }
    public void StopIncrease()
    {
        increase = false;
    }
}
