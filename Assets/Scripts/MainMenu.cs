using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject PlayButton;
    public GameObject OptionsButton;
    public GameObject StoryModeButton;
    public GameObject EndlessModeButton;
    public List<GameObject> dancingPeople;
    public GameObject optionsPanel;
    public GameObject easyCheck;
    public GameObject normalCheck;
    public GameObject hardCheck;
    private DataManager dm;
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject obj in dancingPeople)
        {
            obj.GetComponent<Animator>().SetBool("isHappy", true);
        }
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Music"))
        {
            GameObject.Destroy(obj);
        }
        dm = GameObject.Find("DataManager").GetComponent<DataManager>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPlay()
    {
        PlayButton.SetActive(false);
        OptionsButton.SetActive(false);
        StoryModeButton.SetActive(true);
        EndlessModeButton.SetActive(true);
    }

    public void OnStory()
    {
        dm.endless = false;
        dm.day = 0;
        SceneManager.LoadScene("DayStart");
    }

    public void OnEndless()
    {
        dm.endless = true;
        dm.day = 7;
        SceneManager.LoadScene("MainScene");
    }

    public void OnOptions()
    {
        optionsPanel.SetActive(true);

    }

    public void OnEasy()
    {
        dm.dificulty = DataManager.Dificulty.easy;
        easyCheck.SetActive(true);
        normalCheck.SetActive(false);
        hardCheck.SetActive(false);

    }

    public void OnNormal()
    {
        dm.dificulty = DataManager.Dificulty.normal;
        easyCheck.SetActive(false);
        normalCheck.SetActive(true);
        hardCheck.SetActive(false);
    }

    public void OnHard()
    {
        dm.dificulty = DataManager.Dificulty.hard;
        easyCheck.SetActive(false);
        normalCheck.SetActive(false);
        hardCheck.SetActive(true);
    }

    public void OnOptionsClose()
    {
        optionsPanel.SetActive(false);
    }
}
