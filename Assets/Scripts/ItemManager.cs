using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public Dictionary<string, GameObject> items;
    public Dictionary<string, string> tile2Item;
    public List<GameObject> itemList;
    // Start is called before the first frame update
    void Start()
    {
        items = new Dictionary<string, GameObject>();
        tile2Item = new Dictionary<string, string>();

        foreach (GameObject obj in itemList)
        {
            items.Add(obj.GetComponent<Item>().name, obj);
            Debug.Log("Adding: " + obj.GetComponent<Item>().name);
        }

        // This is only for things that can be picked up from the environment.
        tile2Item.Add("Fridge", "Beer");
        tile2Item.Add("MargGlass", "MargGlass");
        tile2Item.Add("ShotGlass", "ShotGlass");
        tile2Item.Add("TallGlass", "TallGlass");
        tile2Item.Add("Coconut", "Coconut");
    }

    public GameObject GetItem(string tilename)
    {
        return items[tile2Item[tilename]];
    }

}
