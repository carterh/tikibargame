using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// TODO: Have customers who get stuck in the queue broadcast that they are first, but stuck, so that they don't keep pushing each other.
public class Customer : MonoBehaviour
{
    public Vector2 target;
    public float speed;
    public CustomerSpawner spawner;
    private bool firstInQueue = false;
    private bool hasOrdered = false;
    private bool hasBeenServed = false;
    private bool isReadyToDance = false;
    private bool pauseMovement = false;
    private bool onTheFloor = false;
    public SpriteRenderer orderBubble;
    public SpriteRenderer order;
    private BarSlots barSlots;
    private PositionManager posManager;
    private GameObject orderItem;
    public UnityEvent e_Dancing;
    private ProgressBar progressBar;
    public UnityEvent e_patienceOut;

    // The slot where the customer will stand at the bar
    public Slot slot;
    // Start is called before the first frame update
    void Start()
    {
        target = spawner.GetNextTarget(this, firstInQueue);
        barSlots = GameObject.Find("BarPlacement").GetComponent<BarSlots>();
        posManager = GameObject.Find("PositionManager").GetComponent<PositionManager>();
        progressBar = GetComponentInChildren<ProgressBar>();
        progressBar.e_patienceOut.AddListener(() => e_patienceOut.Invoke());
    }

    // Don't look at this.
    // Don't touch this.
    // This is a mess.
    // I am afraid.
    void Update()
    {
        if ((!pauseMovement || firstInQueue) && !onTheFloor)
        {
            transform.position = Vector2.MoveTowards(transform.position, target, Time.deltaTime * speed);
            if (!firstInQueue)
            {
                progressBar.StartIncrease();
            }
        }

        if (pauseMovement)
        {
            progressBar.StartCountdown();
            progressBar.StopIncrease();
        }

        if (!firstInQueue && transform.position.y - target.y < 0.1)
        {
            firstInQueue = true;
            Vector2 newTarget = spawner.GetNextTarget(this, firstInQueue);
            if (newTarget != target)
            {
                target = newTarget;
                GetComponent<BoxCollider2D>().enabled = false;
                progressBar.StopCountdown();
            }
            else{
                // This is a dumb hack. This forces customers to continue to check the customer spawner to wait for a free bar slot.
                firstInQueue = false;
                progressBar.StartCountdown();
                progressBar.StopIncrease();
            }
        }

        if (firstInQueue && !hasOrdered && Mathf.Abs(transform.position.y - target.y) < 0.1 && Mathf.Abs(transform.position.x - target.x) < 0.1)
        {
            PlaceOrder();
        }

        if (hasOrdered && !hasBeenServed)
        {
            int cellPos = posManager.GetCellPos(transform.position).x;
            GameObject inSlot = barSlots.CheckSlot(cellPos);
            if (inSlot != null)
            if (inSlot != null && inSlot.GetComponent<Item>().name == orderItem.GetComponent<Item>().name)
            {
                barSlots.ClearSlot(cellPos);
                slot.isFilled = false;
                target = spawner.targetB.transform.position;
                hasBeenServed = true;
                orderBubble.enabled = false;
                order.enabled = false;
            }
        }

        if (hasBeenServed && !isReadyToDance)
        {
            if (new Vector2(transform.position.x, transform.position.y) == target)
            {
                target = spawner.GetDanceLocation();
                isReadyToDance = true;
            }

        }

        if (isReadyToDance && !onTheFloor)
        {
            if (new Vector2(transform.position.x, transform.position.y) == target)
            {
                onTheFloor = true;
                GetComponent<Animator>().SetBool("isHappy", true);
                e_Dancing.Invoke();
            }

        }
    }

    public void SetSpawner(CustomerSpawner s)
    {
        spawner = s;
    }

    private void PlaceOrder()
    {
        OrderList orderList = GameObject.Find("OrderList").GetComponent<OrderList>();
        orderItem = orderList.orders[Random.Range(0, orderList.orders.Count)];
        orderBubble.enabled = true;
        order.sprite = orderItem.GetComponent<SpriteRenderer>().sprite;
        order.enabled = true;
        hasOrdered = true;
    }

    public void OnCollisionEnter2D()
    {
        pauseMovement = true;
    }

    public void OnCollisionExit2D()
    {
        pauseMovement = false;
    }
}
