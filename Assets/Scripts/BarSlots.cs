using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarSlots : MonoBehaviour
{
    public List<GameObject> slots;
    public List<GameObject> itemsInSlot;
    public List<bool> isFilled;

    void Start()
    {
    }

    public bool PlaceOnSlot(int xOffset, GameObject obj)
    {
        int slotIndex = Mathf.FloorToInt(xOffset / 2) - 2;
        if (!isFilled[slotIndex])
        {
            GameObject slot = slots[slotIndex];
            itemsInSlot[slotIndex] = obj;
            obj.transform.parent = slot.transform;
            obj.transform.position = obj.transform.parent.transform.position;
            isFilled[slotIndex] = true;
            return true;
        }

        return false;
    }

    public GameObject PickupFromSlot(int xOffset)
    {
        int slotIndex = Mathf.FloorToInt(xOffset / 2) - 2;
        if (isFilled[slotIndex])
        {
            GameObject slot = slots[slotIndex];
            isFilled[slotIndex] = false;
            return slot.transform.GetChild(0).gameObject;
        }

        return null;
    }
    public GameObject CheckSlot(int xOffset)
    {
        int slotIndex = Mathf.FloorToInt(xOffset / 2) - 2;
        if (slotIndex >= 0 && slotIndex < isFilled.Count && isFilled[slotIndex])
        {
            GameObject slot = slots[slotIndex];
            return slot.transform.GetChild(0).gameObject;
        }

        return null;
    }

    public void ClearSlot(int xOffset)
    {
        int slotIndex = Mathf.FloorToInt(xOffset / 2) - 2;
        if (isFilled[slotIndex])
        {
            GameObject slot = slots[slotIndex];
            isFilled[slotIndex] = false;
            GameObject.Destroy(itemsInSlot[slotIndex]);
        }
    }
}
