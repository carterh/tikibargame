using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueHawaiiUngarnished: Item
{
    ItemManager itemManager;

    public BlueHawaiiUngarnished() : base()
    {
        base.name = "BlueHawaiiUngarnished";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Pineapple")
        {
            return itemManager.items["BlueHawaiiPineapple"];
        }
        else if (obj == "Umbrella")
        {
            return itemManager.items["BlueHawaiiUmbrella"];
        }
        return null;
    }
}
