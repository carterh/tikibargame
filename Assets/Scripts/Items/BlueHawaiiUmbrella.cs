using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueHawaiiUmbrella: Item
{
    ItemManager itemManager;

    public BlueHawaiiUmbrella() : base()
    {
        base.name = "BlueHawaiiUmbrella";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Pineapple")
        {
            return itemManager.items["BlueHawaii"];
        }
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["VodkaShot"];
        //}
        return null;
    }
}
