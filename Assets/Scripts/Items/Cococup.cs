using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cococup : Item
{
    ItemManager itemManager;

    public Cococup() : base()
    {
        base.name = "Cococup";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["CococupRum"];
        }
        else if (obj == "Pineapple")
        {
            return itemManager.items["CococupPineapple"];
        }
        return null;
    }
}
