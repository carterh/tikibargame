using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassPineapple : Item
{
    ItemManager itemManager;

    public TallGlassPineapple() : base()
    {
        base.name = "TallGlassPineapple";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["TallGlassPineappleRum"];
        }
        else if (obj == "Lime")
        {
            return itemManager.items["TallGlassLimePineapple"];
        }
        return null;
    }
}
