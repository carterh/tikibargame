using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassJuice : Item
{
    ItemManager itemManager;

    public TallGlassJuice() : base()
    {
        base.name = "TallGlassJuice";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["BayBreezeUngarnished"];
        }
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["VodkaShot"];
        //}
        return null;
    }
}
