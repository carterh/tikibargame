using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Item : MonoBehaviour
{
    //public Sprite sprite;
    public string name;

    public abstract GameObject Interact(string item);
}
