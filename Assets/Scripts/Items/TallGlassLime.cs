using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassLime : Item
{
    ItemManager itemManager;

    public TallGlassLime() : base()
    {
        base.name = "TallGlassLime";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["TallGlassLimeRum"];
        }
        else if (obj == "Pineapple")
        {
            return itemManager.items["TallGlassLimePineapple"];
        }
        return null;
    }
}
