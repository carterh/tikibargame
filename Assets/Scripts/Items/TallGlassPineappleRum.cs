using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassPineappleRum : Item
{
    ItemManager itemManager;

    public TallGlassPineappleRum() : base()
    {
        base.name = "TallGlassPineappleRum";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Lime")
        {
            return itemManager.items["MojitoUngarnished"];
        }
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["VodkaShot"];
        //}
        return null;
    }
}
