using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassLimeRum : Item
{
    ItemManager itemManager;

    public TallGlassLimeRum() : base()
    {
        base.name = "TallGlassLimeRum";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Pineapple")
        {
            return itemManager.items["MojitoUngarnished"];
        }
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["VodkaShot"];
        //}
        return null;
    }
}
