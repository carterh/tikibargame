using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassRum : Item
{
    ItemManager itemManager;

    public TallGlassRum() : base()
    {
        base.name = "TallGlassRum";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Juice")
        {
            return itemManager.items["BayBreezeUngarnished"];
        }
        else if (obj == "Lime")
        {
            return itemManager.items["TallGlassLimeRum"];
        }
        else if (obj == "Pineapple")
        {
            return itemManager.items["TallGlassPineappleRum"];
        }
        else if (obj == "Vodka")
        {
            return itemManager.items["TallGlassRumVodka"];
        }
        else if (obj == "Blue")
        {
            return itemManager.items["TallGlassBlueRum"];
        }
        return null;
    }
}
