using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CococupRum : Item
{
    ItemManager itemManager;

    public CococupRum() : base()
    {
        base.name = "CococupRum";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Pineapple")
        {
            return itemManager.items["PinaColadaUngarnished"];
        }
        //else if (obj == "GreenMarg")
        //{
        //    return itemManager.items["GreenMarg"];
        //}
        return null;
    }
}
