using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VodkaShot : Item
{
    ItemManager itemManager;

    public VodkaShot() : base()
    {
        base.name = "VodkaShot";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        return null;
    }
}
