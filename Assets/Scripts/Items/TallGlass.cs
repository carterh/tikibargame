using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlass : Item
{
    ItemManager itemManager;

    public TallGlass() : base()
    {
        base.name = "TallGlass";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["TallGlassRum"];
        }
        else if (obj == "Vodka")
        {
            return itemManager.items["TallGlassVodka"];
        }
        else if (obj == "Juice")
        {
            return itemManager.items["TallGlassJuice"];
        }
        else if (obj == "Lime")
        {
            return itemManager.items["TallGlassLime"];
        }
        else if (obj == "Pineapple")
        {
            return itemManager.items["TallGlassPineapple"];
        }
        else if (obj == "Blue")
        {
            return itemManager.items["TallGlassBlue"];
        }
        return null;
    }
}
