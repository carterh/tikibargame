using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassBlue : Item
{
    ItemManager itemManager;

    public TallGlassBlue() : base()
    {
        base.name = "TallGlassBlue";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["TallGlassBlueRum"];
        }
        else if (obj == "Vodka")
        {
            return itemManager.items["TallGlassBlueVodka"];
        }
        //else if (obj == "Pineapple")
        //{
        //    return itemManager.items["TallGlassPineappleRum"];
        //}
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["TallGlassRumVodka"];
        //}
        return null;
    }
}
