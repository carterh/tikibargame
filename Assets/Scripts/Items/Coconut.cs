using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coconut : Item
{
    ItemManager itemManager;

    public Coconut() : base()
    {
        base.name = "Coconut";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Knife")
        {
            return itemManager.items["Cococup"];
        }
        //else if (obj == "GreenMarg")
        //{
        //    return itemManager.items["GreenMarg"];
        //}
        return null;
    }
}
