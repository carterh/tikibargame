using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueHawaiiPineapple: Item
{
    ItemManager itemManager;

    public BlueHawaiiPineapple() : base()
    {
        base.name = "BlueHawaiiPineapple";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Umbrella")
        {
            return itemManager.items["BlueHawaii"];
        }
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["VodkaShot"];
        //}
        return null;
    }
}
