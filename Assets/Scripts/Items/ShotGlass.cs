using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGlass : Item
{
    ItemManager itemManager;

    public ShotGlass() : base()
    {
        base.name = "ShotGlass";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["RumShot"];
        }
        else if (obj == "Vodka")
        {
            return itemManager.items["VodkaShot"];
        }
        return null;
    }
}
