using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassLimePineapple : Item
{
    ItemManager itemManager;

    public TallGlassLimePineapple() : base()
    {
        base.name = "TallGlassLimePineapple";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["MojitoUngarnished"];
        }
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["VodkaShot"];
        //}
        return null;
    }
}
