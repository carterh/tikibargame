using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MargGlass : Item
{
    ItemManager itemManager;

    public MargGlass() : base()
    {
        base.name = "MargGlass";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "RedMarg")
        {
            return itemManager.items["RedMarg"];
        }
        else if (obj == "GreenMarg")
        {
            return itemManager.items["GreenMarg"];
        }
        return null;
    }
}
