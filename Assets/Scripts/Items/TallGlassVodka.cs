using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassVodka : Item
{
    ItemManager itemManager;

    public TallGlassVodka() : base()
    {
        base.name = "TallGlassVodka";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["TallGlassRumVodka"];
        }
        else if (obj == "Blue")
        {
            return itemManager.items["TallGlassBlueVodka"];
        }
        return null;
    }
}
