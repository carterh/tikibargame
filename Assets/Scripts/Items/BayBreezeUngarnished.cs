using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BayBreezeUngarnished : Item
{
    ItemManager itemManager;

    public BayBreezeUngarnished() : base()
    {
        base.name = "BayBreezeUngarnished";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Pineapple")
        {
            return itemManager.items["BayBreeze"];
        }
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["VodkaShot"];
        //}
        return null;
    }
}
