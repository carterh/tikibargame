using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CococupPineapple : Item
{
    ItemManager itemManager;

    public CococupPineapple() : base()
    {
        base.name = "CococupPineapple";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["PinaColadaUngarnished"];
        }
        //else if (obj == "GreenMarg")
        //{
        //    return itemManager.items["GreenMarg"];
        //}
        return null;
    }
}
