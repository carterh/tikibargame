using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassBlueRum : Item
{
    ItemManager itemManager;

    public TallGlassBlueRum() : base()
    {
        base.name = "TallGlassBlueRum";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Vodka")
        {
            return itemManager.items["BlueHawaiiUngarnished"];
        }
        //else if (obj == "Lime")
        //{
        //    return itemManager.items["TallGlassLimeRum"];
        //}
        //else if (obj == "Pineapple")
        //{
        //    return itemManager.items["TallGlassPineappleRum"];
        //}
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["TallGlassRumVodka"];
        //}
        return null;
    }
}
