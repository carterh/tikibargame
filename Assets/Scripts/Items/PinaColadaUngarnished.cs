using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinaColadaUngarnished : Item
{
    ItemManager itemManager;

    public PinaColadaUngarnished() : base()
    {
        base.name = "PinaColadaUngarnished";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Umbrella")
        {
            return itemManager.items["PinaColada"];
        }
        //else if (obj == "Pineapple")
        //{
        //    return itemManager.items["CococupPineapple"];
        //}
        return null;
    }
}
