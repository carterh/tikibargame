using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MojitoUngarnished: Item
{
    ItemManager itemManager;

    public MojitoUngarnished() : base()
    {
        base.name = "MojitoUngarnished";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Lime")
        {
            return itemManager.items["Mojito"];
        }
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["VodkaShot"];
        //}
        return null;
    }
}
