using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassBlueVodka : Item
{
    ItemManager itemManager;

    public TallGlassBlueVodka() : base()
    {
        base.name = "TallGlassBlueVodka";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Rum")
        {
            return itemManager.items["BlueHawaiiUngarnished"];
        }
        //else if (obj == "Lime")
        //{
        //    return itemManager.items["TallGlassLimeRum"];
        //}
        //else if (obj == "Pineapple")
        //{
        //    return itemManager.items["TallGlassPineappleRum"];
        //}
        //else if (obj == "Vodka")
        //{
        //    return itemManager.items["TallGlassRumVodka"];
        //}
        return null;
    }
}
