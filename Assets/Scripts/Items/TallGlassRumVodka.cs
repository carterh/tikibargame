using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TallGlassRumVodka : Item
{
    ItemManager itemManager;

    public TallGlassRumVodka() : base()
    {
        base.name = "TallGlassRumVodka";
    }

    void Start()
    {
        itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
    }

    override public GameObject Interact(string obj)
    {
        Debug.Log(obj);
        if (obj == "Blue")
        {
            return itemManager.items["BlueHawaiiUngarnished"];
        }
        //else if (obj == "Lime")
        //{
        //    return itemManager.items["TallGlassLimeRum"];
        //}
        //else if (obj == "Pineapple")
        //{
        //    return itemManager.items["TallGlassPineappleRum"];
        //}
        return null;
    }
}
