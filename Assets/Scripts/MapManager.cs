using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapManager : MonoBehaviour
{
    public List<GameObject> maps;
    public List<Tilemap> interactables;
    // Start is called before the first frame update
    void Start()
    {
        GameObject map = maps[GameObject.Find("DataManager").GetComponent<DataManager>().day];
        map.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Tilemap GetInteractable()
    {
        return interactables[GameObject.Find("DataManager").GetComponent<DataManager>().day];
    }
}
