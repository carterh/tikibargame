using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using SuperTiled2Unity;
using UnityEngine.Events;

public class PositionManager : MonoBehaviour
{

    public UnityEvent<string, Vector3Int> e_lookingAt;
    public MapManager mapManager;
    private Tilemap tilemap;
    public PlayerControls player;
    // Start is called before the first frame update
    void Start()
    {
        player.e_movedPosition.AddListener(UpdatePlayerPosition);
        tilemap = mapManager.GetInteractable();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdatePlayerPosition(Vector2 pos, SpriteSwapper.Direction dir)
    {
        Debug.Log(pos);
        Vector3Int intPos = tilemap.WorldToCell(new Vector3(pos.x, pos.y, 0));
        if (dir == SpriteSwapper.Direction.Down)
        {
            intPos.y -= 1;
        }
        else if (dir == SpriteSwapper.Direction.Left)
        {
            intPos.x -= 1;
        }
        else if (dir == SpriteSwapper.Direction.Right)
        {
            intPos.x += 1;
        }

        SuperTile tile = tilemap.GetTile<SuperTile>(intPos);
        if (tile.GetPropertyValueAsString("ItemName") == "")
        {
            intPos.y-=1;
        }
        tile = tilemap.GetTile<SuperTile>(intPos);

        e_lookingAt.Invoke(tile.GetPropertyValueAsString("ItemName"), intPos);
    }

    public Vector3Int GetCellPos(Vector2 pos)
    {
        return tilemap.WorldToCell(new Vector3(pos.x, pos.y, 0));
    }
}
