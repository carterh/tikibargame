using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class LightingManager : MonoBehaviour
{
    public Light2D globalLight;
    public Light2D interiorLight;
    public List<Light2D> torchLights;
    public float globalLightTargetValue;
    public float globalLightCurrentValue = 0;
    public float lightSpeed;
    public float hue;
    public float value;
    // Update is called once per frame
    void Update()
    {
        globalLightCurrentValue = Vector2.MoveTowards(new Vector2(globalLightCurrentValue, 0), new Vector2(globalLightTargetValue, 0), Time.deltaTime * lightSpeed).x;
        globalLight.color = Color.HSVToRGB(hue, globalLightCurrentValue, value);
        interiorLight.intensity = globalLightCurrentValue;
        foreach (Light2D light in torchLights)
        {
            light.intensity = globalLightCurrentValue;
        }
    }
}
