using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// TODO: High/Low ordering mapping for polish. Need to keep two incrementing counters to correct the overlap order of the sprites.
public class CustomerSpawner : MonoBehaviour
{
    public List<GameObject> prefabs;
    public float rate;
    public float easyRate;
    public float normalRate;
    public float hardRate;
    private float timer = 100;
    public GameObject targetA;
    public GameObject targetB;
    public GameObject danceAreaTopLeft;
    public GameObject danceAreaBottomRight;
    public List<Slot> barSlots;
    public int count;
    private int dancingCount = 0;
    private int countDown;
    private int previousSpawn = 100;
    private DataManager dataManager;
    public float endlessIncrease;
    public float endlessStartRate;
    public WinManager winManager;
    // Start is called before the first frame update
    void Start()
    {
        dataManager = GameObject.Find("DataManager").GetComponent<DataManager>();

        count = count + (3 * dataManager.day);
        // Make day 7 easier.
        if (dataManager.day == 7)
        {
            count -= 5;
        }
        countDown = count;
        if (dataManager.endless)
        {
            rate = endlessStartRate;
        }
        else 
        {
            switch(dataManager.dificulty)
            {
                case DataManager.Dificulty.easy:
                    rate = easyRate;
                    break;
                case DataManager.Dificulty.hard:
                    rate = hardRate;
                    break;
                default:
                    rate = normalRate;
                    break;
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > rate && countDown > 0)
        {
            int rs = Random.Range(0, prefabs.Count);
            // Insure that each customer is different
            while (rs == previousSpawn)
            {
                rs = Random.Range(0, prefabs.Count);
            }
            previousSpawn = rs;
            GameObject customer = GameObject.Instantiate(prefabs[rs]);
            customer.transform.position = transform.position;
            customer.GetComponent<Customer>().SetSpawner(this);
            customer.GetComponent<Customer>().e_Dancing.AddListener(() => {
                dancingCount++;
                Debug.Log("Increasing dancing count to " + dancingCount);
            });
            customer.GetComponent<Customer>().e_patienceOut.AddListener(() => SceneManager.LoadScene("LoseScreen"));

            timer = 0;
            if (!dataManager.endless && rate >= 1)
            {
                countDown--;
            }
        }
        timer += Time.deltaTime;

        if (countDown <= 0 && dancingCount == count)
        {
            if (GameObject.Find("DataManager").GetComponent<DataManager>().day == 7)
            {
                winManager.Go();
            }
            else
            {
                GameObject.Find("DataManager").GetComponent<DataManager>().day++;
                SceneManager.LoadScene("DayStart");
            }
        }

        if (dataManager.endless)
        {
            rate -= endlessIncrease * Time.deltaTime;
        }
    }

    public Vector2 GetNextTarget(Customer customer, bool firstInQueue)
    {
        if (firstInQueue && HaveEmptySlot())
        {
            GameObject slot = GetAvailableSlot();
            slot.GetComponent<Slot>().isFilled = true;
            customer.slot = slot.GetComponent<Slot>();
            return slot.transform.position;
        }
        return targetA.transform.position;
    }

    private bool HaveEmptySlot()
    {
        foreach (Slot slot in barSlots)
        {
            if (!slot.isFilled)
                return true;
        }
        return false;
    }

    private GameObject GetAvailableSlot()
    {
        foreach (Slot slot in barSlots)
        {
            if (!slot.isFilled)
            {
                return slot.gameObject;
            }
        }
        return null;
    }

    public Vector2 GetDanceLocation()
    {
        Vector2 danceArea = new Vector2();
        danceArea.x = Random.Range(danceAreaTopLeft.transform.position.x, danceAreaBottomRight.transform.position.x);
        danceArea.y = Random.Range(danceAreaBottomRight.transform.position.y, danceAreaTopLeft.transform.position.y);
        return danceArea;
    }
}
