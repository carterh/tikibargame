using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseScreenInput : MonoBehaviour
{
    void OnPickup()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
