using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DayStartInput : MonoBehaviour
{
    bool isExposition = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnPickup()
    {
        if (isExposition)
        {
            isExposition = GameObject.Find("DayStartCanvas").GetComponent<DayStartScreen>().MoveToDay();
        }
        else
        {
            SceneManager.LoadScene("MainScene");
        }
    }
}
