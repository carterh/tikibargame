using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using SuperTiled2Unity;
using UnityEngine.Tilemaps;

public class PlayerControls : MonoBehaviour
{
    SpriteSwapper.Direction direction = SpriteSwapper.Direction.Down;
    public UnityEvent<Vector2, SpriteSwapper.Direction> e_movedPosition;
    private Vector2 movement;
    private Rigidbody2D rigidbody;
    private SpriteSwapper spriteSwapper;
    public float speed;
    private Vector2 previousPosition;
    private string lookingAt;
    private Vector3Int lookingAtPos;
    public ItemManager itemManager;
    public GameObject handsDown;
    public GameObject handsUp;
    public GameObject handsLeft;
    public GameObject handsRight;
    private GameObject holding;
    public BarSlots barSlots;
    public AudioManager audioManager;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        spriteSwapper = GetComponent<SpriteSwapper>();
        spriteSwapper.autoswapEnabled = false;
        previousPosition = rigidbody.position;

        GameObject.Find("PositionManager").GetComponent<PositionManager>().e_lookingAt.AddListener((ns, t) => {
            lookingAt = ns;
            lookingAtPos = t;
            });
    }

    // Update is called once per frame
    void Update()
    {
        rigidbody.position += movement * Time.deltaTime * speed;
        if (movement.y < 0)
        {
            direction = SpriteSwapper.Direction.Down;
        }
        else if (movement.y > 0)
        {
            direction = SpriteSwapper.Direction.Up;
        }
        else if (movement.x < 0)
        {
            direction = SpriteSwapper.Direction.Left;
        }
        else if (movement.x > 0)
        {
            direction = SpriteSwapper.Direction.Right;
        }
        spriteSwapper.SwapSprite(direction);

        if (previousPosition != rigidbody.position)
        {
            e_movedPosition.Invoke(rigidbody.position, direction);
            previousPosition = rigidbody.position;

            if (holding != null)
            {
                ApplyHoldingDirection();
            }
        }
    }

    void OnMove(InputValue value)
    {
        movement = value.Get<Vector2>();
    }

    void OnPickup(InputValue value)
    {
        if (holding == null)
        {
            if (lookingAt == "Bar")
            {
                holding = barSlots.PickupFromSlot(lookingAtPos.x);
                if (holding != null)
                {
                    audioManager.e_pickUp.Invoke();
                }
            }
            else
            {
                GameObject item = itemManager.GetItem(lookingAt);
                if (item == null)
                {
                    audioManager.e_error.Invoke();
                }
                else
                {
                    GameObject instance = GameObject.Instantiate(item, handsUp.transform);
                    holding = instance;
                    ApplyHoldingDirection();
                    audioManager.e_pickUp.Invoke();
                }
            }
        }
        else if (lookingAt == "Bar")
        {
            Debug.Log(lookingAtPos);
            if (barSlots.PlaceOnSlot(lookingAtPos.x, holding))
            {
                holding = null;
                audioManager.e_putDown.Invoke();
            }
        }
        else if (lookingAt == "Trash")
        {
            GameObject.Destroy(holding);
            holding = null;
            audioManager.e_putDown.Invoke();
        }
        else
        {
            GameObject newPrefab = holding.GetComponent<Item>().Interact(lookingAt);
            if (newPrefab != null)
            {
                GameObject newItem = GameObject.Instantiate(newPrefab);
                GameObject.Destroy(holding);
                holding = newItem;
                ApplyHoldingDirection();
                audioManager.e_pickUp.Invoke();
            }
            else
            {
                audioManager.e_error.Invoke();
            }
        }
    }

    void OnCollisionEnter(Collision hit){
        Debug.Log("Collided");
    }

    void ApplyHoldingDirection()
    {
        switch(direction)
        {
            case SpriteSwapper.Direction.Up:
                holding.transform.SetParent(handsUp.transform);
                holding.GetComponent<SpriteRenderer>().sortingOrder = 4;
                break;
            case SpriteSwapper.Direction.Left:
                holding.transform.SetParent(handsLeft.transform);
                holding.GetComponent<SpriteRenderer>().sortingOrder = 5;
                break;
            case SpriteSwapper.Direction.Right:
                holding.transform.SetParent(handsRight.transform);
                holding.GetComponent<SpriteRenderer>().sortingOrder = 5;
                break;
            default:
                holding.transform.SetParent(handsDown.transform);
                holding.GetComponent<SpriteRenderer>().sortingOrder = 5;
                break;
            
        }
        holding.transform.position = holding.transform.parent.transform.position;
    }
}
