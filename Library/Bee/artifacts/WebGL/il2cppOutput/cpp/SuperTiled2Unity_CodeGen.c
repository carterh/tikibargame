﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Vector2[] SuperTiled2Unity.CollisionObject::get_Points()
extern void CollisionObject_get_Points_mDC299097F3812479DAE12DBF1AC785F93689B734 (void);
// 0x00000002 System.Boolean SuperTiled2Unity.CollisionObject::get_IsClosed()
extern void CollisionObject_get_IsClosed_mBCD345D0D56E05C4EC74A01581CEB3E8B2F4B674 (void);
// 0x00000003 SuperTiled2Unity.CollisionShapeType SuperTiled2Unity.CollisionObject::get_CollisionShapeType()
extern void CollisionObject_get_CollisionShapeType_m4F76225AC158808859D5411B8FEB5A16AC308C6B (void);
// 0x00000004 System.Void SuperTiled2Unity.CollisionObject::MakePointsFromRectangle()
extern void CollisionObject_MakePointsFromRectangle_m79B21AC37EE53B2CF34E485D2CFD8848FFFD7FDF (void);
// 0x00000005 System.Void SuperTiled2Unity.CollisionObject::MakePoint()
extern void CollisionObject_MakePoint_m41B58C685C263D1070B6A709FA4173522A317688 (void);
// 0x00000006 System.Void SuperTiled2Unity.CollisionObject::MakePointsFromEllipse(System.Int32)
extern void CollisionObject_MakePointsFromEllipse_mA70B1853AFD309BE4337FF6F75FC989AB662DB52 (void);
// 0x00000007 System.Void SuperTiled2Unity.CollisionObject::MakePointsFromPolygon(UnityEngine.Vector2[])
extern void CollisionObject_MakePointsFromPolygon_m50FBB4A510348191E291D989168C142C58AB0F79 (void);
// 0x00000008 System.Void SuperTiled2Unity.CollisionObject::MakePointsFromPolyline(UnityEngine.Vector2[])
extern void CollisionObject_MakePointsFromPolyline_m6187F93C5E266B1A15A6C68B3A13296A7C444BB1 (void);
// 0x00000009 System.Void SuperTiled2Unity.CollisionObject::RenderPoints(SuperTiled2Unity.SuperTile,SuperTiled2Unity.GridOrientation,UnityEngine.Vector2)
extern void CollisionObject_RenderPoints_m0F5F6114893BECD3F44230F675DE29646D665C2D (void);
// 0x0000000A UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::IsometricTransform(UnityEngine.Vector2,SuperTiled2Unity.SuperTile,UnityEngine.Vector2)
extern void CollisionObject_IsometricTransform_m59113D4A7DBD20C375B1B0D596519BDB9395CEBE (void);
// 0x0000000B UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::LocalTransform(UnityEngine.Vector2,SuperTiled2Unity.SuperTile)
extern void CollisionObject_LocalTransform_m4BBC2EED1964CAC87D946F6B20C84A395BA06018 (void);
// 0x0000000C System.Void SuperTiled2Unity.CollisionObject::ApplyRotationToPoints()
extern void CollisionObject_ApplyRotationToPoints_m512B1BE5CCABAA6514BC6E76C9A261E159B6B100 (void);
// 0x0000000D System.Void SuperTiled2Unity.CollisionObject::.ctor()
extern void CollisionObject__ctor_mBC390D4F0B3F4CC9DD91895D4BC50C040DB130EA (void);
// 0x0000000E System.Void SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m8DE5A4BC5950A73655C5C2AC805A0967676BEFC4 (void);
// 0x0000000F UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::<RenderPoints>b__0(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__0_m027D21D5381DD2013D9011B4631C89AF06AC5898 (void);
// 0x00000010 UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::<RenderPoints>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__1_m6E76FA998AF975862FF19F35A85976C8E737C919 (void);
// 0x00000011 System.Void SuperTiled2Unity.CollisionObject/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_mCBE0212AE0DBE64819687EB9DD21E60F17916C9C (void);
// 0x00000012 UnityEngine.Vector3 SuperTiled2Unity.CollisionObject/<>c__DisplayClass26_0::<ApplyRotationToPoints>b__0(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass26_0_U3CApplyRotationToPointsU3Eb__0_m441B23E915236979FDC89E3044CEF47843C1F8CF (void);
// 0x00000013 System.Void SuperTiled2Unity.CollisionObject/<>c::.cctor()
extern void U3CU3Ec__cctor_m999AE66A4C40BEFFD48AB73918965F60BE4B4929 (void);
// 0x00000014 System.Void SuperTiled2Unity.CollisionObject/<>c::.ctor()
extern void U3CU3Ec__ctor_m6383F2FDA8B99E0D3A293C2E26B2B149404F8EA2 (void);
// 0x00000015 UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c::<ApplyRotationToPoints>b__26_1(UnityEngine.Vector3)
extern void U3CU3Ec_U3CApplyRotationToPointsU3Eb__26_1_mD8BB4E0ABE4DCB6ADD80368637F6E1CB2F0B7F1D (void);
// 0x00000016 System.Boolean SuperTiled2Unity.CustomProperty::get_IsEmpty()
extern void CustomProperty_get_IsEmpty_mFA36C70BB3CBC229F110FE5E4CAFFC4359767092 (void);
// 0x00000017 System.Void SuperTiled2Unity.CustomProperty::.ctor()
extern void CustomProperty__ctor_m587076D390A2AA41418C4C1057782040046FE2E4 (void);
// 0x00000018 System.Boolean SuperTiled2Unity.CustomPropertyListExtensions::TryGetProperty(System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>,System.String,SuperTiled2Unity.CustomProperty&)
extern void CustomPropertyListExtensions_TryGetProperty_m262897E51BA2D6BB72B492DA7FF7D4FEC479BB5B (void);
// 0x00000019 System.Void SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mBAB88BBA8C5224AE6CAC9C848DD1643518E5E3A1 (void);
// 0x0000001A System.Boolean SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::<TryGetProperty>b__0(SuperTiled2Unity.CustomProperty)
extern void U3CU3Ec__DisplayClass0_0_U3CTryGetPropertyU3Eb__0_mFFDDC9E830ACFEAA3E57FE8B240756505F6683BC (void);
// 0x0000001B System.String SuperTiled2Unity.CustomPropertyExtensions::GetValueAsString(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsString_m692FA0C6470995727534A812C2FD7B7D1264D577 (void);
// 0x0000001C UnityEngine.Color SuperTiled2Unity.CustomPropertyExtensions::GetValueAsColor(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsColor_mECD5B66015F0470AD850B9886361BF0EBB795422 (void);
// 0x0000001D System.Int32 SuperTiled2Unity.CustomPropertyExtensions::GetValueAsInt(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsInt_m08EBBA564426968A0AD4A00BDC2E01E9A3C98B7D (void);
// 0x0000001E System.Single SuperTiled2Unity.CustomPropertyExtensions::GetValueAsFloat(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsFloat_m7A0545C8D8D20891BC596F1D31A40FBD2B10F27C (void);
// 0x0000001F System.Boolean SuperTiled2Unity.CustomPropertyExtensions::GetValueAsBool(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsBool_mC15F3B153F093F3249890AAFC2DCB0317685F28D (void);
// 0x00000020 T SuperTiled2Unity.CustomPropertyExtensions::GetValueAsEnum(SuperTiled2Unity.CustomProperty)
// 0x00000021 System.Boolean SuperTiled2Unity.EnumerableExtensions::IsEmpty(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000022 T SuperTiled2Unity.GameObjectExtensions::GetComponentInAncestor(UnityEngine.MonoBehaviour)
// 0x00000023 T SuperTiled2Unity.GameObjectExtensions::GetComponentInAncestor(UnityEngine.GameObject)
// 0x00000024 System.Boolean SuperTiled2Unity.GameObjectExtensions::TryGetCustomPropertySafe(UnityEngine.GameObject,System.String,SuperTiled2Unity.CustomProperty&)
extern void GameObjectExtensions_TryGetCustomPropertySafe_m9869D960C47BA789F6A08A718030FA7DB4D35053 (void);
// 0x00000025 UnityEngine.Color SuperTiled2Unity.StringExtensions::ToColor(System.String)
extern void StringExtensions_ToColor_mCA880BC04D74E0EC9D926C72B478F58027A082D1 (void);
// 0x00000026 T SuperTiled2Unity.StringExtensions::ToEnum(System.String)
// 0x00000027 System.Single SuperTiled2Unity.StringExtensions::ToFloat(System.String)
extern void StringExtensions_ToFloat_m41589007A7AF2104695878FDB4D80E3AFAAFA496 (void);
// 0x00000028 System.Int32 SuperTiled2Unity.StringExtensions::ToInt(System.String)
extern void StringExtensions_ToInt_m1B2045627BF608C15A803476AA9DB54B149B1E76 (void);
// 0x00000029 System.Boolean SuperTiled2Unity.StringExtensions::ToBool(System.String)
extern void StringExtensions_ToBool_mF36AD979ADB49B4DE575A0CC84CBFFC6CA0795F0 (void);
// 0x0000002A System.Boolean SuperTiled2Unity.SuperTileExtensions::TryGetProperty(SuperTiled2Unity.SuperTile,System.String,SuperTiled2Unity.CustomProperty&)
extern void SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84 (void);
// 0x0000002B System.String SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsString(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsString_m3558F05D09E99AEC4991E35CED7734A0CEED3496 (void);
// 0x0000002C System.String SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsString(SuperTiled2Unity.SuperTile,System.String,System.String)
extern void SuperTileExtensions_GetPropertyValueAsString_m2D9D3FDA56279F63FB2FD59FEDF28429B416AF5E (void);
// 0x0000002D System.Boolean SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsBool(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsBool_m87FCCCFA1E3E7339949F88AE760A16DB62ED6F8F (void);
// 0x0000002E System.Boolean SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsBool(SuperTiled2Unity.SuperTile,System.String,System.Boolean)
extern void SuperTileExtensions_GetPropertyValueAsBool_m7F16749D7A003164C8E166412E87955874412F83 (void);
// 0x0000002F System.Int32 SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsInt(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsInt_mCBCE77AAC99985EEA4FDA1E9DCE02A229B0F3776 (void);
// 0x00000030 System.Int32 SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsInt(SuperTiled2Unity.SuperTile,System.String,System.Int32)
extern void SuperTileExtensions_GetPropertyValueAsInt_m3970CC724020E9F46C4387FA8C911F7908EA7E3C (void);
// 0x00000031 System.Single SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsFloat(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsFloat_m7AF951188326A939D105FAFB6F639187F973FA18 (void);
// 0x00000032 System.Single SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsFloat(SuperTiled2Unity.SuperTile,System.String,System.Single)
extern void SuperTileExtensions_GetPropertyValueAsFloat_m7B0D6B7A93DE789BC97C462A9B9168C60B9523DC (void);
// 0x00000033 UnityEngine.Color SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsColor(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsColor_mEC386C1FF460A9E361608047B11407206BE7735A (void);
// 0x00000034 UnityEngine.Color SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsColor(SuperTiled2Unity.SuperTile,System.String,UnityEngine.Color)
extern void SuperTileExtensions_GetPropertyValueAsColor_m612E3C0B943694EF561332143F07AA0862DB7E70 (void);
// 0x00000035 T SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsEnum(SuperTiled2Unity.SuperTile,System.String)
// 0x00000036 T SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsEnum(SuperTiled2Unity.SuperTile,System.String,T)
// 0x00000037 System.Boolean SuperTiled2Unity.FlipFlagsMask::FlippedHorizontally(SuperTiled2Unity.FlipFlags)
extern void FlipFlagsMask_FlippedHorizontally_mEEFD6BFA6D3842CB5425125A29735904B90F03A1 (void);
// 0x00000038 System.Boolean SuperTiled2Unity.FlipFlagsMask::FlippedVertically(SuperTiled2Unity.FlipFlags)
extern void FlipFlagsMask_FlippedVertically_mB596859EF886CE6219BC0B4E5697260B598269C3 (void);
// 0x00000039 System.Boolean SuperTiled2Unity.FlipFlagsMask::RotatedDiagonally(SuperTiled2Unity.FlipFlags)
extern void FlipFlagsMask_RotatedDiagonally_mECF1DF8F2B9DC31F7F2060FA362BDC8D7C47D2F5 (void);
// 0x0000003A System.Boolean SuperTiled2Unity.FlipFlagsMask::RotatedHexagonally120(SuperTiled2Unity.FlipFlags)
extern void FlipFlagsMask_RotatedHexagonally120_m47701C0290627A54236243EF6079E27F53FF6B89 (void);
// 0x0000003B UnityEngine.Tilemaps.TilemapRenderer/SortOrder SuperTiled2Unity.MapRenderConverter::Tiled2Unity(SuperTiled2Unity.MapRenderOrder)
extern void MapRenderConverter_Tiled2Unity_mA0B4704642C31A8710C7D3FD9D03A53AB78F89BE (void);
// 0x0000003C UnityEngine.Matrix4x4 SuperTiled2Unity.MatrixUtils::Rotate2d(System.Single,System.Single,System.Single,System.Single)
extern void MatrixUtils_Rotate2d_m94977337A24271488FF3F281D11DF921A20079DA (void);
// 0x0000003D UnityEngine.Vector3 SuperTiled2Unity.ObjectAlignmentToPivot::ToVector3(System.Single,System.Single,System.Single,SuperTiled2Unity.MapOrientation,SuperTiled2Unity.ObjectAlignment)
extern void ObjectAlignmentToPivot_ToVector3_m03C9825A915AFCD26166818258EE018E51A33977 (void);
// 0x0000003E System.Void SuperTiled2Unity.ReadOnlyAttribute::.ctor()
extern void ReadOnlyAttribute__ctor_m0BAA811D5FE2D4958319538FE4EAD5FBEBFF6BFD (void);
// 0x0000003F System.Void SuperTiled2Unity.SuperColliderComponent::.ctor()
extern void SuperColliderComponent__ctor_m4EB807688DC0B776A283C84A53C1DD85F4B28557 (void);
// 0x00000040 System.Void SuperTiled2Unity.SuperColliderComponent/Shape::.ctor()
extern void Shape__ctor_m4D0B7FB88E2324F5643E54E0B0EBC9240AA15D32 (void);
// 0x00000041 System.Boolean SuperTiled2Unity.SuperCustomProperties::TryGetCustomProperty(System.String,SuperTiled2Unity.CustomProperty&)
extern void SuperCustomProperties_TryGetCustomProperty_m621D9CBDF999333FB4836A496DA074ED1BFDD981 (void);
// 0x00000042 System.Void SuperTiled2Unity.SuperCustomProperties::RemoveCustomProperty(System.String)
extern void SuperCustomProperties_RemoveCustomProperty_m0FF0B5F1C8F57C7F4ED96D1EFFBB9008FA643F5A (void);
// 0x00000043 System.Void SuperTiled2Unity.SuperCustomProperties::.ctor()
extern void SuperCustomProperties__ctor_m09B5D823FCD1C41B2808B92CDF27659D9A8E3DD2 (void);
// 0x00000044 System.Void SuperTiled2Unity.SuperCustomProperties/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mABC57E0F328DE14C90D613679DBBFFB4FB78A8D8 (void);
// 0x00000045 System.Boolean SuperTiled2Unity.SuperCustomProperties/<>c__DisplayClass2_0::<RemoveCustomProperty>g__PredicateRemoveCustomProperty|0(SuperTiled2Unity.CustomProperty)
extern void U3CU3Ec__DisplayClass2_0_U3CRemoveCustomPropertyU3Eg__PredicateRemoveCustomPropertyU7C0_m25DC0B231A7E7E6E137D739178A6E283DD075AC9 (void);
// 0x00000046 System.Void SuperTiled2Unity.SuperGroupLayer::.ctor()
extern void SuperGroupLayer__ctor_m517B0645DCA484925C2AFA5D0DE8563FC4FFF4B0 (void);
// 0x00000047 System.Void SuperTiled2Unity.SuperImageLayer::.ctor()
extern void SuperImageLayer__ctor_m2548E0F500D974FCBF85B1114F29739D8FD033FD (void);
// 0x00000048 UnityEngine.Color SuperTiled2Unity.SuperLayer::CalculateColor()
extern void SuperLayer_CalculateColor_m0CDBE161CBA50DE3F521F2211DF23117CB1658B0 (void);
// 0x00000049 System.Single SuperTiled2Unity.SuperLayer::CalculateOpacity()
extern void SuperLayer_CalculateOpacity_m4343ED780CD03CCE9AB0E1AEDB474BC003419846 (void);
// 0x0000004A System.Void SuperTiled2Unity.SuperLayer::.ctor()
extern void SuperLayer__ctor_m27C9812BCE3C7EF1A912DAB7F323C8C56BFE82E9 (void);
// 0x0000004B System.Void SuperTiled2Unity.SuperMap::Start()
extern void SuperMap_Start_mCDD9A6B0489E2FBCD3CA6C0722D32D59E7EA416F (void);
// 0x0000004C UnityEngine.Vector3Int SuperTiled2Unity.SuperMap::TiledIndexToGridCell(System.Int32,System.Int32,System.Int32,System.Int32)
extern void SuperMap_TiledIndexToGridCell_mCAD36FC70FE48D50197A57C7DAF555FB09A4D2EB (void);
// 0x0000004D UnityEngine.Vector3Int SuperTiled2Unity.SuperMap::TiledCellToGridCell(System.Int32,System.Int32)
extern void SuperMap_TiledCellToGridCell_m5FEBC722CCC4F177AD34568AC83330C76C094681 (void);
// 0x0000004E System.Void SuperTiled2Unity.SuperMap::.ctor()
extern void SuperMap__ctor_mB9C726674494050FF4E16ADEF2C2B8B736379020 (void);
// 0x0000004F UnityEngine.Color SuperTiled2Unity.SuperObject::CalculateColor()
extern void SuperObject_CalculateColor_m133FE769F9023B882EBBF47BE11813528C31EAD8 (void);
// 0x00000050 System.Void SuperTiled2Unity.SuperObject::.ctor()
extern void SuperObject__ctor_m981FD9F134B7AA0EA0DAC7636FA2EEA501F8B8A1 (void);
// 0x00000051 System.Void SuperTiled2Unity.SuperObjectLayer::.ctor()
extern void SuperObjectLayer__ctor_m30B9FD7AC4E12FA244D88AF73FB53E27DF9CAEA2 (void);
// 0x00000052 UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::GetTransformMatrix(SuperTiled2Unity.FlipFlags,SuperTiled2Unity.MapOrientation)
extern void SuperTile_GetTransformMatrix_m405A1814B7D9CF76DE6D99AFF22A1E04D67FBE77 (void);
// 0x00000053 System.Void SuperTiled2Unity.SuperTile::GetTRS(SuperTiled2Unity.FlipFlags,SuperTiled2Unity.MapOrientation,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void SuperTile_GetTRS_m525BD60277E27D9F0D26C5FB7AE5514074AE7BE9 (void);
// 0x00000054 System.Void SuperTiled2Unity.SuperTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void SuperTile_GetTileData_mD082BBDC5BEFE028C2E0F10CD3BDA41076C4BDE1 (void);
// 0x00000055 System.Boolean SuperTiled2Unity.SuperTile::GetTileAnimationData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileAnimationData&)
extern void SuperTile_GetTileAnimationData_mA95A8339FF7A3B058B57177AF06DFD971E7559C4 (void);
// 0x00000056 System.Void SuperTiled2Unity.SuperTile::.ctor()
extern void SuperTile__ctor_m23DF3B1098095B93E4F1EDBE23CA3715B3C25863 (void);
// 0x00000057 System.Void SuperTiled2Unity.SuperTile::.cctor()
extern void SuperTile__cctor_mCB8B3A4F115B1C1D4852F99C7C0C49911731ABE7 (void);
// 0x00000058 System.Void SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m97CDB1D70C523C541D148070D2B82323FE8EEAA6 (void);
// 0x00000059 UnityEngine.Vector3 SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::<GetTRS>b__0(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__0_m75AC6BEE8A89E221D9AF18BADEACA6EBC800448B (void);
// 0x0000005A UnityEngine.Vector3 SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::<GetTRS>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__1_mA2D7CBC425D13D83AF72FF71A1381AFBC1C389F5 (void);
// 0x0000005B System.Void SuperTiled2Unity.SuperTile/<>c::.cctor()
extern void U3CU3Ec__cctor_m7BBB440A1BEAAA8E43FF4405E6C99B2FDC5F74CB (void);
// 0x0000005C System.Void SuperTiled2Unity.SuperTile/<>c::.ctor()
extern void U3CU3Ec__ctor_mAEBBF578A513958C40FF2051364E82C0A4C8188E (void);
// 0x0000005D System.Single SuperTiled2Unity.SuperTile/<>c::<GetTRS>b__17_2(UnityEngine.Vector3)
extern void U3CU3Ec_U3CGetTRSU3Eb__17_2_mD578367B5F4AC01E5653744DFA0E0BEBDFD38916 (void);
// 0x0000005E System.Single SuperTiled2Unity.SuperTile/<>c::<GetTRS>b__17_3(UnityEngine.Vector3)
extern void U3CU3Ec_U3CGetTRSU3Eb__17_3_m7C98132BC1339274334A35C8DFE5FA4078753870 (void);
// 0x0000005F System.Void SuperTiled2Unity.SuperTileLayer::.ctor()
extern void SuperTileLayer__ctor_m2650C41ACDBE8A29A7B026756BBF24D25D6CF2F0 (void);
// 0x00000060 System.Void SuperTiled2Unity.SuperTilesAsObjectsTilemap::.ctor()
extern void SuperTilesAsObjectsTilemap__ctor_mC9D49805ED4EF2E17780F926EEA6D27F5EC0DC5D (void);
// 0x00000061 System.Void SuperTiled2Unity.SuperWorld::.ctor()
extern void SuperWorld__ctor_mA65BF6084579CC9D390265E18E63A01FE631A94A (void);
// 0x00000062 System.Void SuperTiled2Unity.TileObjectAnimator::Update()
extern void TileObjectAnimator_Update_m6F4A7FF613C3DC5BA9F6F775825C142944299ADC (void);
// 0x00000063 System.Void SuperTiled2Unity.TileObjectAnimator::.ctor()
extern void TileObjectAnimator__ctor_mFBBDEF40740930CACC5A12FA3704E11F7E183B93 (void);
static Il2CppMethodPointer s_methodPointers[99] = 
{
	CollisionObject_get_Points_mDC299097F3812479DAE12DBF1AC785F93689B734,
	CollisionObject_get_IsClosed_mBCD345D0D56E05C4EC74A01581CEB3E8B2F4B674,
	CollisionObject_get_CollisionShapeType_m4F76225AC158808859D5411B8FEB5A16AC308C6B,
	CollisionObject_MakePointsFromRectangle_m79B21AC37EE53B2CF34E485D2CFD8848FFFD7FDF,
	CollisionObject_MakePoint_m41B58C685C263D1070B6A709FA4173522A317688,
	CollisionObject_MakePointsFromEllipse_mA70B1853AFD309BE4337FF6F75FC989AB662DB52,
	CollisionObject_MakePointsFromPolygon_m50FBB4A510348191E291D989168C142C58AB0F79,
	CollisionObject_MakePointsFromPolyline_m6187F93C5E266B1A15A6C68B3A13296A7C444BB1,
	CollisionObject_RenderPoints_m0F5F6114893BECD3F44230F675DE29646D665C2D,
	CollisionObject_IsometricTransform_m59113D4A7DBD20C375B1B0D596519BDB9395CEBE,
	CollisionObject_LocalTransform_m4BBC2EED1964CAC87D946F6B20C84A395BA06018,
	CollisionObject_ApplyRotationToPoints_m512B1BE5CCABAA6514BC6E76C9A261E159B6B100,
	CollisionObject__ctor_mBC390D4F0B3F4CC9DD91895D4BC50C040DB130EA,
	U3CU3Ec__DisplayClass23_0__ctor_m8DE5A4BC5950A73655C5C2AC805A0967676BEFC4,
	U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__0_m027D21D5381DD2013D9011B4631C89AF06AC5898,
	U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__1_m6E76FA998AF975862FF19F35A85976C8E737C919,
	U3CU3Ec__DisplayClass26_0__ctor_mCBE0212AE0DBE64819687EB9DD21E60F17916C9C,
	U3CU3Ec__DisplayClass26_0_U3CApplyRotationToPointsU3Eb__0_m441B23E915236979FDC89E3044CEF47843C1F8CF,
	U3CU3Ec__cctor_m999AE66A4C40BEFFD48AB73918965F60BE4B4929,
	U3CU3Ec__ctor_m6383F2FDA8B99E0D3A293C2E26B2B149404F8EA2,
	U3CU3Ec_U3CApplyRotationToPointsU3Eb__26_1_mD8BB4E0ABE4DCB6ADD80368637F6E1CB2F0B7F1D,
	CustomProperty_get_IsEmpty_mFA36C70BB3CBC229F110FE5E4CAFFC4359767092,
	CustomProperty__ctor_m587076D390A2AA41418C4C1057782040046FE2E4,
	CustomPropertyListExtensions_TryGetProperty_m262897E51BA2D6BB72B492DA7FF7D4FEC479BB5B,
	U3CU3Ec__DisplayClass0_0__ctor_mBAB88BBA8C5224AE6CAC9C848DD1643518E5E3A1,
	U3CU3Ec__DisplayClass0_0_U3CTryGetPropertyU3Eb__0_mFFDDC9E830ACFEAA3E57FE8B240756505F6683BC,
	CustomPropertyExtensions_GetValueAsString_m692FA0C6470995727534A812C2FD7B7D1264D577,
	CustomPropertyExtensions_GetValueAsColor_mECD5B66015F0470AD850B9886361BF0EBB795422,
	CustomPropertyExtensions_GetValueAsInt_m08EBBA564426968A0AD4A00BDC2E01E9A3C98B7D,
	CustomPropertyExtensions_GetValueAsFloat_m7A0545C8D8D20891BC596F1D31A40FBD2B10F27C,
	CustomPropertyExtensions_GetValueAsBool_mC15F3B153F093F3249890AAFC2DCB0317685F28D,
	NULL,
	NULL,
	NULL,
	NULL,
	GameObjectExtensions_TryGetCustomPropertySafe_m9869D960C47BA789F6A08A718030FA7DB4D35053,
	StringExtensions_ToColor_mCA880BC04D74E0EC9D926C72B478F58027A082D1,
	NULL,
	StringExtensions_ToFloat_m41589007A7AF2104695878FDB4D80E3AFAAFA496,
	StringExtensions_ToInt_m1B2045627BF608C15A803476AA9DB54B149B1E76,
	StringExtensions_ToBool_mF36AD979ADB49B4DE575A0CC84CBFFC6CA0795F0,
	SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84,
	SuperTileExtensions_GetPropertyValueAsString_m3558F05D09E99AEC4991E35CED7734A0CEED3496,
	SuperTileExtensions_GetPropertyValueAsString_m2D9D3FDA56279F63FB2FD59FEDF28429B416AF5E,
	SuperTileExtensions_GetPropertyValueAsBool_m87FCCCFA1E3E7339949F88AE760A16DB62ED6F8F,
	SuperTileExtensions_GetPropertyValueAsBool_m7F16749D7A003164C8E166412E87955874412F83,
	SuperTileExtensions_GetPropertyValueAsInt_mCBCE77AAC99985EEA4FDA1E9DCE02A229B0F3776,
	SuperTileExtensions_GetPropertyValueAsInt_m3970CC724020E9F46C4387FA8C911F7908EA7E3C,
	SuperTileExtensions_GetPropertyValueAsFloat_m7AF951188326A939D105FAFB6F639187F973FA18,
	SuperTileExtensions_GetPropertyValueAsFloat_m7B0D6B7A93DE789BC97C462A9B9168C60B9523DC,
	SuperTileExtensions_GetPropertyValueAsColor_mEC386C1FF460A9E361608047B11407206BE7735A,
	SuperTileExtensions_GetPropertyValueAsColor_m612E3C0B943694EF561332143F07AA0862DB7E70,
	NULL,
	NULL,
	FlipFlagsMask_FlippedHorizontally_mEEFD6BFA6D3842CB5425125A29735904B90F03A1,
	FlipFlagsMask_FlippedVertically_mB596859EF886CE6219BC0B4E5697260B598269C3,
	FlipFlagsMask_RotatedDiagonally_mECF1DF8F2B9DC31F7F2060FA362BDC8D7C47D2F5,
	FlipFlagsMask_RotatedHexagonally120_m47701C0290627A54236243EF6079E27F53FF6B89,
	MapRenderConverter_Tiled2Unity_mA0B4704642C31A8710C7D3FD9D03A53AB78F89BE,
	MatrixUtils_Rotate2d_m94977337A24271488FF3F281D11DF921A20079DA,
	ObjectAlignmentToPivot_ToVector3_m03C9825A915AFCD26166818258EE018E51A33977,
	ReadOnlyAttribute__ctor_m0BAA811D5FE2D4958319538FE4EAD5FBEBFF6BFD,
	SuperColliderComponent__ctor_m4EB807688DC0B776A283C84A53C1DD85F4B28557,
	Shape__ctor_m4D0B7FB88E2324F5643E54E0B0EBC9240AA15D32,
	SuperCustomProperties_TryGetCustomProperty_m621D9CBDF999333FB4836A496DA074ED1BFDD981,
	SuperCustomProperties_RemoveCustomProperty_m0FF0B5F1C8F57C7F4ED96D1EFFBB9008FA643F5A,
	SuperCustomProperties__ctor_m09B5D823FCD1C41B2808B92CDF27659D9A8E3DD2,
	U3CU3Ec__DisplayClass2_0__ctor_mABC57E0F328DE14C90D613679DBBFFB4FB78A8D8,
	U3CU3Ec__DisplayClass2_0_U3CRemoveCustomPropertyU3Eg__PredicateRemoveCustomPropertyU7C0_m25DC0B231A7E7E6E137D739178A6E283DD075AC9,
	SuperGroupLayer__ctor_m517B0645DCA484925C2AFA5D0DE8563FC4FFF4B0,
	SuperImageLayer__ctor_m2548E0F500D974FCBF85B1114F29739D8FD033FD,
	SuperLayer_CalculateColor_m0CDBE161CBA50DE3F521F2211DF23117CB1658B0,
	SuperLayer_CalculateOpacity_m4343ED780CD03CCE9AB0E1AEDB474BC003419846,
	SuperLayer__ctor_m27C9812BCE3C7EF1A912DAB7F323C8C56BFE82E9,
	SuperMap_Start_mCDD9A6B0489E2FBCD3CA6C0722D32D59E7EA416F,
	SuperMap_TiledIndexToGridCell_mCAD36FC70FE48D50197A57C7DAF555FB09A4D2EB,
	SuperMap_TiledCellToGridCell_m5FEBC722CCC4F177AD34568AC83330C76C094681,
	SuperMap__ctor_mB9C726674494050FF4E16ADEF2C2B8B736379020,
	SuperObject_CalculateColor_m133FE769F9023B882EBBF47BE11813528C31EAD8,
	SuperObject__ctor_m981FD9F134B7AA0EA0DAC7636FA2EEA501F8B8A1,
	SuperObjectLayer__ctor_m30B9FD7AC4E12FA244D88AF73FB53E27DF9CAEA2,
	SuperTile_GetTransformMatrix_m405A1814B7D9CF76DE6D99AFF22A1E04D67FBE77,
	SuperTile_GetTRS_m525BD60277E27D9F0D26C5FB7AE5514074AE7BE9,
	SuperTile_GetTileData_mD082BBDC5BEFE028C2E0F10CD3BDA41076C4BDE1,
	SuperTile_GetTileAnimationData_mA95A8339FF7A3B058B57177AF06DFD971E7559C4,
	SuperTile__ctor_m23DF3B1098095B93E4F1EDBE23CA3715B3C25863,
	SuperTile__cctor_mCB8B3A4F115B1C1D4852F99C7C0C49911731ABE7,
	U3CU3Ec__DisplayClass17_0__ctor_m97CDB1D70C523C541D148070D2B82323FE8EEAA6,
	U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__0_m75AC6BEE8A89E221D9AF18BADEACA6EBC800448B,
	U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__1_mA2D7CBC425D13D83AF72FF71A1381AFBC1C389F5,
	U3CU3Ec__cctor_m7BBB440A1BEAAA8E43FF4405E6C99B2FDC5F74CB,
	U3CU3Ec__ctor_mAEBBF578A513958C40FF2051364E82C0A4C8188E,
	U3CU3Ec_U3CGetTRSU3Eb__17_2_mD578367B5F4AC01E5653744DFA0E0BEBDFD38916,
	U3CU3Ec_U3CGetTRSU3Eb__17_3_m7C98132BC1339274334A35C8DFE5FA4078753870,
	SuperTileLayer__ctor_m2650C41ACDBE8A29A7B026756BBF24D25D6CF2F0,
	SuperTilesAsObjectsTilemap__ctor_mC9D49805ED4EF2E17780F926EEA6D27F5EC0DC5D,
	SuperWorld__ctor_mA65BF6084579CC9D390265E18E63A01FE631A94A,
	TileObjectAnimator_Update_m6F4A7FF613C3DC5BA9F6F775825C142944299ADC,
	TileObjectAnimator__ctor_mFBBDEF40740930CACC5A12FA3704E11F7E183B93,
};
static const int32_t s_InvokerIndices[99] = 
{
	6345,
	6245,
	6316,
	6473,
	6473,
	5073,
	5102,
	5102,
	1409,
	1256,
	2214,
	6473,
	6473,
	6473,
	4700,
	4700,
	6473,
	4710,
	11691,
	6473,
	4701,
	6245,
	6473,
	8025,
	6473,
	3570,
	10540,
	10350,
	10438,
	10632,
	10317,
	0,
	0,
	0,
	0,
	8025,
	10350,
	0,
	10632,
	10438,
	10317,
	8025,
	9111,
	8204,
	8835,
	8026,
	9015,
	8128,
	9185,
	8245,
	8923,
	8051,
	0,
	0,
	10314,
	10314,
	10314,
	10314,
	10434,
	7669,
	7175,
	6473,
	6473,
	6473,
	1685,
	5102,
	6473,
	6473,
	3570,
	6473,
	6473,
	6247,
	6402,
	6473,
	6473,
	863,
	2219,
	6473,
	6247,
	6473,
	6473,
	2136,
	451,
	1488,
	1150,
	6473,
	11691,
	6473,
	4711,
	4711,
	11691,
	6473,
	4608,
	4608,
	6473,
	6473,
	6473,
	6473,
	6473,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x06000020, { 0, 1 } },
	{ 0x06000021, { 1, 1 } },
	{ 0x06000022, { 2, 1 } },
	{ 0x06000023, { 3, 1 } },
	{ 0x06000026, { 4, 2 } },
	{ 0x06000035, { 6, 1 } },
	{ 0x06000036, { 7, 1 } },
};
extern const uint32_t g_rgctx_StringExtensions_ToEnum_TisT_tADD299B9D521CB5D511E42F6D0B998120A8C5C2F_m39821F873C115A1577AF78B05A4D8EFC92B4D914;
extern const uint32_t g_rgctx_Enumerable_Any_TisT_tFFA846203F60D450B1895DF07A0672A424471E3C_mF43AC8DAC555C4FED9A8241A746A38AA7D6DD4D9;
extern const uint32_t g_rgctx_GameObjectExtensions_GetComponentInAncestor_TisT_t844B4AA5D9B01B39BCD2A4F712620A5FFC58AB0A_m1305B30E1D1C82C227FB7D06AEFBE7E1450D056A;
extern const uint32_t g_rgctx_Component_GetComponentInParent_TisT_t89349F42DF7BA3766244DFDB4871BD633267DE20_m9FD965DD6DE3963D0BB1BD59C8BFB4281C8991E1;
extern const uint32_t g_rgctx_T_t26522A96E2A7AA0F3575D5C529DF1ADE06F35B7C;
extern const uint32_t g_rgctx_T_t26522A96E2A7AA0F3575D5C529DF1ADE06F35B7C;
extern const uint32_t g_rgctx_SuperTileExtensions_GetPropertyValueAsEnum_TisT_t8CCD45EFC66BE67C0D4719BE3CEB2AFADDCFF0D8_mCA7A3EA3F44F4BE3350D89B95B5F0CD1FE8592EE;
extern const uint32_t g_rgctx_CustomPropertyExtensions_GetValueAsEnum_TisT_t3B5BB856DCB14EDFDEFA738B1E1461CD3798D8C1_m72E74148DED24D8B26BF9E2882959D4B9AC55E59;
static const Il2CppRGCTXDefinition s_rgctxValues[8] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_StringExtensions_ToEnum_TisT_tADD299B9D521CB5D511E42F6D0B998120A8C5C2F_m39821F873C115A1577AF78B05A4D8EFC92B4D914 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerable_Any_TisT_tFFA846203F60D450B1895DF07A0672A424471E3C_mF43AC8DAC555C4FED9A8241A746A38AA7D6DD4D9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObjectExtensions_GetComponentInAncestor_TisT_t844B4AA5D9B01B39BCD2A4F712620A5FFC58AB0A_m1305B30E1D1C82C227FB7D06AEFBE7E1450D056A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Component_GetComponentInParent_TisT_t89349F42DF7BA3766244DFDB4871BD633267DE20_m9FD965DD6DE3963D0BB1BD59C8BFB4281C8991E1 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t26522A96E2A7AA0F3575D5C529DF1ADE06F35B7C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t26522A96E2A7AA0F3575D5C529DF1ADE06F35B7C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SuperTileExtensions_GetPropertyValueAsEnum_TisT_t8CCD45EFC66BE67C0D4719BE3CEB2AFADDCFF0D8_mCA7A3EA3F44F4BE3350D89B95B5F0CD1FE8592EE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_CustomPropertyExtensions_GetValueAsEnum_TisT_t3B5BB856DCB14EDFDEFA738B1E1461CD3798D8C1_m72E74148DED24D8B26BF9E2882959D4B9AC55E59 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SuperTiled2Unity_CodeGenModule;
const Il2CppCodeGenModule g_SuperTiled2Unity_CodeGenModule = 
{
	"SuperTiled2Unity.dll",
	99,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	8,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
