﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t9FA6D82CAFC18769F7515BB51D1C56DAE09381C3;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_tE1603CE612C16451D1E56FF4D4859D4FE4087C28;
// System.Func`2<UnityEngine.Vector2,UnityEngine.Vector2>
struct Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA;
// System.Func`2<UnityEngine.Vector2,UnityEngine.Vector3>
struct Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768;
// System.Func`2<UnityEngine.Vector3,System.Single>
struct Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC;
// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2>
struct Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B;
// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t352FDDEA001ABE8E1D67849D2E2F3D1D75B03D41;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t6C47A8FE62321E6AD75C312B8549AFD2B13F0591;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t5C1E5CDFEA99062D152E83B174072FEDB9763788;
// System.Collections.Generic.List`1<SuperTiled2Unity.CollisionObject>
struct List_1_tD0C437F57D6FF6931FC5080DADDD0F9557D71D02;
// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>
struct List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<SuperTiled2Unity.SuperColliderComponent/Shape>
struct List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17;
// System.Predicate`1<SuperTiled2Unity.CustomProperty>
struct Predicate_1_tE3859600DB095616566FA8539A8605019C819898;
// System.Predicate`1<System.Object>
struct Predicate_1_t8342C85FF4E41CD1F7024AC0CDC3E5312A32CB12;
// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer>
struct UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// SuperTiled2Unity.CustomProperty[]
struct CustomPropertyU5BU5D_t2CF95CE738AB8D7C92E9E040911B3C99EF0499CF;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// SuperTiled2Unity.SuperLayer[]
struct SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52;
// UnityEngine.Tilemaps.TilemapRenderer[]
struct TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// SuperTiled2Unity.SuperColliderComponent/Shape[]
struct ShapeU5BU5D_t57E1CA1A2F7427123379D595C484C9A786DE4D61;
// System.Globalization.Calendar
struct Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B;
// SuperTiled2Unity.CollisionObject
struct CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A;
// System.Globalization.CompareInfo
struct CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// System.Globalization.CultureData
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D;
// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0;
// SuperTiled2Unity.CustomProperty
struct CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.IFormatProvider
struct IFormatProvider_tC202922D43BFF3525109ABF3FB79625F5646AB52;
// UnityEngine.Tilemaps.ITilemap
struct ITilemap_tCD8B9C2D6A80DB1DFE9C934D91EACE6B8A018164;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t5E0CB5A6CDA6E24CBD4FF26DE3B0C29D8BB54BF0;
// SuperTiled2Unity.ReadOnlyAttribute
struct ReadOnlyAttribute_tC7D6593B643C74C635E5BEE10D4BF3383D6701FE;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B;
// System.String
struct String_t;
// SuperTiled2Unity.SuperColliderComponent
struct SuperColliderComponent_tC658CE43E321DE9458EF2777905A81C57426E419;
// SuperTiled2Unity.SuperCustomProperties
struct SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE;
// SuperTiled2Unity.SuperGroupLayer
struct SuperGroupLayer_tC73291F06FA4A186FAB5BCBD8FE92853B5DE2AB1;
// SuperTiled2Unity.SuperImageLayer
struct SuperImageLayer_t7023C8E04FA266112B5F0F2321FB2051A4791EDB;
// SuperTiled2Unity.SuperLayer
struct SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76;
// SuperTiled2Unity.SuperMap
struct SuperMap_tBBCDA02EE1A6B1D4E3B0B7D85C4D6A129280177D;
// SuperTiled2Unity.SuperObject
struct SuperObject_t69DFDC213711BB2C894AB42B71BFF3238035C06E;
// SuperTiled2Unity.SuperObjectLayer
struct SuperObjectLayer_tE988ACCE14F2B9F055CFE9E0256B2597C0DCC995;
// SuperTiled2Unity.SuperTile
struct SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898;
// SuperTiled2Unity.SuperTileLayer
struct SuperTileLayer_t100FD3159D41613E8B5DE0EF08F08629B6FC1B69;
// SuperTiled2Unity.SuperTilesAsObjectsTilemap
struct SuperTilesAsObjectsTilemap_t541B0111712AA3121005663AF25E0440918656B4;
// SuperTiled2Unity.SuperWorld
struct SuperWorld_t7FDD3E6EC0C0633E0F9A88892A9C5B500E5DB97B;
// System.Globalization.TextInfo
struct TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4;
// UnityEngine.Tilemaps.TileBase
struct TileBase_t07019BD771D35E8EA68118157D6EEE4C770CF0F9;
// SuperTiled2Unity.TileObjectAnimator
struct TileObjectAnimator_t136148EED3B0B64B585CB1A265930B96EF0E821F;
// UnityEngine.Tilemaps.Tilemap
struct Tilemap_t18C4166D0AC702D5BFC0C411FA73C4B61D9D1751;
// UnityEngine.Tilemaps.TilemapRenderer
struct TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// SuperTiled2Unity.CollisionObject/<>c
struct U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77;
// SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18;
// SuperTiled2Unity.CollisionObject/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB;
// SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA;
// SuperTiled2Unity.SuperColliderComponent/Shape
struct Shape_tBA148987BC8B013C676DCCCFD4F112E273FBAF71;
// SuperTiled2Unity.SuperCustomProperties/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452;
// SuperTiled2Unity.SuperTile/<>c
struct U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA;
// SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E;

IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Predicate_1_tE3859600DB095616566FA8539A8605019C819898_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0B0FEB3147CE20EB2C90076367F895C59BCD14B3;
IL2CPP_EXTERN_C String_t* _stringLiteral14612D0AE4B7E7FCE6B63EA53EC07A4E37A2C80E;
IL2CPP_EXTERN_C String_t* _stringLiteral6CAEFDEE17C0117C16963C7A094BBDC99E09B94D;
IL2CPP_EXTERN_C String_t* _stringLiteral77D38C0623F92B292B925F6E72CF5CF99A20D4EB;
IL2CPP_EXTERN_C String_t* _stringLiteral9DEC6BA217C79567CA5C709BA89D4C7622E5FEEC;
IL2CPP_EXTERN_C String_t* _stringLiteralA88B7770DB03C11888305F2EC564F231840512FD;
IL2CPP_EXTERN_C String_t* _stringLiteralB7C45DD316C68ABF3429C20058C2981C652192F2;
IL2CPP_EXTERN_C String_t* _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3;
IL2CPP_EXTERN_C String_t* _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisTilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB_m3CFA932712E5F15C704FAA4A95AB809344841ED8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mA5D7A04386B3B335680CF888F4516918A1100459_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D6DCFEC603609B8CEFE5F7186E6CB0903999035_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisSuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE_m62AF091848A5ACFB37052C7AF9F74C9484B0EE32_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Find_mE71D47CAE16DFDCE85DAEA6732CCF78DCB0AEEE5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAll_mFE2C6425D80C8A9E05C9B8ADB56C91773C9EC1C5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m65D107AE0F3BB61F0C374671D35DBB2181FF6F95_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CApplyRotationToPointsU3Eb__26_1_mD8BB4E0ABE4DCB6ADD80368637F6E1CB2F0B7F1D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CGetTRSU3Eb__17_2_mD578367B5F4AC01E5653744DFA0E0BEBDFD38916_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CGetTRSU3Eb__17_3_m7C98132BC1339274334A35C8DFE5FA4078753870_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass0_0_U3CTryGetPropertyU3Eb__0_mFFDDC9E830ACFEAA3E57FE8B240756505F6683BC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__0_m75AC6BEE8A89E221D9AF18BADEACA6EBC800448B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__1_mA2D7CBC425D13D83AF72FF71A1381AFBC1C389F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__0_m027D21D5381DD2013D9011B4631C89AF06AC5898_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__1_m6E76FA998AF975862FF19F35A85976C8E737C919_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass26_0_U3CApplyRotationToPointsU3Eb__0_m441B23E915236979FDC89E3044CEF47843C1F8CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass2_0_U3CRemoveCustomPropertyU3Eg__PredicateRemoveCustomPropertyU7C0_m25DC0B231A7E7E6E137D739178A6E283DD075AC9_RuntimeMethod_var;
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_com;
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_pinvoke;
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com;
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
struct SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52;
struct TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53;
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tAB15E17F68651F0CB56524A2AC070DD209DB5034 
{
};

// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>
struct List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	CustomPropertyU5BU5D_t2CF95CE738AB8D7C92E9E040911B3C99EF0499CF* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	CustomPropertyU5BU5D_t2CF95CE738AB8D7C92E9E040911B3C99EF0499CF* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<SuperTiled2Unity.SuperColliderComponent/Shape>
struct List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ShapeU5BU5D_t57E1CA1A2F7427123379D595C484C9A786DE4D61* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ShapeU5BU5D_t57E1CA1A2F7427123379D595C484C9A786DE4D61* ___s_emptyArray_5;
};
struct Il2CppArrayBounds;

// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA  : public RuntimeObject
{
};

// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0  : public RuntimeObject
{
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D* ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;
};

struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_StaticFields
{
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject* ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_DefaultThreadCurrentUICulture_34;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_DefaultThreadCurrentCulture_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t9FA6D82CAFC18769F7515BB51D1C56DAE09381C3* ___shared_by_number_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_tE1603CE612C16451D1E56FF4D4859D4FE4087C28* ___shared_by_name_37;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::s_UserPreferredCultureInfoInAppX
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_UserPreferredCultureInfoInAppX_38;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_39;
};
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// SuperTiled2Unity.CustomProperty
struct CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B  : public RuntimeObject
{
	// System.String SuperTiled2Unity.CustomProperty::m_Name
	String_t* ___m_Name_0;
	// System.String SuperTiled2Unity.CustomProperty::m_Type
	String_t* ___m_Type_1;
	// System.String SuperTiled2Unity.CustomProperty::m_Value
	String_t* ___m_Value_2;
};

// SuperTiled2Unity.CustomPropertyExtensions
struct CustomPropertyExtensions_t0F9D78DC8CD7355D4228A822C9D2936B32F1E1EB  : public RuntimeObject
{
};

// SuperTiled2Unity.CustomPropertyListExtensions
struct CustomPropertyListExtensions_tF5BCFBD7539FD5A7BD2BB9B4A39932320D87A5E2  : public RuntimeObject
{
};

// SuperTiled2Unity.EnumerableExtensions
struct EnumerableExtensions_tB78925B8F3F2FAF643D90D0FEEBA777582790677  : public RuntimeObject
{
};

// SuperTiled2Unity.FlipFlagsMask
struct FlipFlagsMask_t78C339BC7BD342C33316E940F9EBC8D55EF7463A  : public RuntimeObject
{
};

// SuperTiled2Unity.GameObjectExtensions
struct GameObjectExtensions_tD17F7902FE26ECDCE640C7FBC39D498AB2BBB984  : public RuntimeObject
{
};

// SuperTiled2Unity.MapRenderConverter
struct MapRenderConverter_tB5FB0B2504DBB94A7C22EFF7325A488B4AA482EB  : public RuntimeObject
{
};

// SuperTiled2Unity.MatrixUtils
struct MatrixUtils_t8F7F042C4896A4F063B4A0329752F87708728352  : public RuntimeObject
{
};

// SuperTiled2Unity.ObjectAlignmentToPivot
struct ObjectAlignmentToPivot_tF94DF70CDEE89D1D0C179EDFCF19A6792D7A5501  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// SuperTiled2Unity.StringExtensions
struct StringExtensions_t0D1FC21F57E754B6296A74DBC885BCD6F63CFD52  : public RuntimeObject
{
};

// SuperTiled2Unity.SuperTileExtensions
struct SuperTileExtensions_t4317708EDBFD8CD7E77A1678062E1300C8D19DFD  : public RuntimeObject
{
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// SuperTiled2Unity.CollisionObject/<>c
struct U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77  : public RuntimeObject
{
};

struct U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_StaticFields
{
	// SuperTiled2Unity.CollisionObject/<>c SuperTiled2Unity.CollisionObject/<>c::<>9
	U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77* ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2> SuperTiled2Unity.CollisionObject/<>c::<>9__26_1
	Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* ___U3CU3E9__26_1_1;
};

// SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18  : public RuntimeObject
{
	// SuperTiled2Unity.CollisionObject SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::<>4__this
	CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* ___U3CU3E4__this_0;
	// SuperTiled2Unity.SuperTile SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::tile
	SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile_1;
};

// SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA  : public RuntimeObject
{
	// System.String SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::propertyName
	String_t* ___propertyName_0;
};

// SuperTiled2Unity.SuperColliderComponent/Shape
struct Shape_tBA148987BC8B013C676DCCCFD4F112E273FBAF71  : public RuntimeObject
{
	// UnityEngine.Vector2[] SuperTiled2Unity.SuperColliderComponent/Shape::m_Points
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_Points_0;
};

// SuperTiled2Unity.SuperCustomProperties/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452  : public RuntimeObject
{
	// System.String SuperTiled2Unity.SuperCustomProperties/<>c__DisplayClass2_0::name
	String_t* ___name_0;
};

// SuperTiled2Unity.SuperTile/<>c
struct U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA  : public RuntimeObject
{
};

struct U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields
{
	// SuperTiled2Unity.SuperTile/<>c SuperTiled2Unity.SuperTile/<>c::<>9
	U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA* ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Vector3,System.Single> SuperTiled2Unity.SuperTile/<>c::<>9__17_2
	Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* ___U3CU3E9__17_2_1;
	// System.Func`2<UnityEngine.Vector3,System.Single> SuperTiled2Unity.SuperTile/<>c::<>9__17_3
	Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* ___U3CU3E9__17_3_2;
};

// Unity.Collections.NativeArray`1<UnityEngine.Vector3Int>
struct NativeArray_1_t245D7224A42D1A32B87C64E49B7B434585EC91EF 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.PropertyAttribute
struct PropertyAttribute_t5E0CB5A6CDA6E24CBD4FF26DE3B0C29D8BB54BF0  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Tilemaps.TileAnimationData
struct TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149 
{
	// UnityEngine.Sprite[] UnityEngine.Tilemaps.TileAnimationData::m_AnimatedSprites
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___m_AnimatedSprites_0;
	// System.Single UnityEngine.Tilemaps.TileAnimationData::m_AnimationSpeed
	float ___m_AnimationSpeed_1;
	// System.Single UnityEngine.Tilemaps.TileAnimationData::m_AnimationStartTime
	float ___m_AnimationStartTime_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.Tilemaps.TileAnimationData
struct TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149_marshaled_pinvoke
{
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___m_AnimatedSprites_0;
	float ___m_AnimationSpeed_1;
	float ___m_AnimationStartTime_2;
};
// Native definition for COM marshalling of UnityEngine.Tilemaps.TileAnimationData
struct TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149_marshaled_com
{
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___m_AnimatedSprites_0;
	float ___m_AnimationSpeed_1;
	float ___m_AnimationStartTime_2;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3Int
struct Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 
{
	// System.Int32 UnityEngine.Vector3Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector3Int::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.Vector3Int::m_Z
	int32_t ___m_Z_2;
};

struct Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376_StaticFields
{
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Zero
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Zero_3;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_One
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_One_4;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Up
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Up_5;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Down
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Down_6;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Left
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Left_7;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Right
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Right_8;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Forward
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Forward_9;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Back
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Back_10;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// SuperTiled2Unity.CollisionObject
struct CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A  : public RuntimeObject
{
	// System.Int32 SuperTiled2Unity.CollisionObject::m_ObjectId
	int32_t ___m_ObjectId_0;
	// System.String SuperTiled2Unity.CollisionObject::m_ObjectName
	String_t* ___m_ObjectName_1;
	// System.String SuperTiled2Unity.CollisionObject::m_ObjectType
	String_t* ___m_ObjectType_2;
	// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::m_Position
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Position_3;
	// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::m_Size
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Size_4;
	// System.Single SuperTiled2Unity.CollisionObject::m_Rotation
	float ___m_Rotation_5;
	// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty> SuperTiled2Unity.CollisionObject::m_CustomProperties
	List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* ___m_CustomProperties_6;
	// System.String SuperTiled2Unity.CollisionObject::m_PhysicsLayer
	String_t* ___m_PhysicsLayer_7;
	// System.Boolean SuperTiled2Unity.CollisionObject::m_IsTrigger
	bool ___m_IsTrigger_8;
	// UnityEngine.Vector2[] SuperTiled2Unity.CollisionObject::m_Points
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_Points_9;
	// System.Boolean SuperTiled2Unity.CollisionObject::m_IsClosed
	bool ___m_IsClosed_10;
	// SuperTiled2Unity.CollisionShapeType SuperTiled2Unity.CollisionObject::m_CollisionShapeType
	int32_t ___m_CollisionShapeType_11;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// UnityEngine.Tilemaps.ITilemap
struct ITilemap_tCD8B9C2D6A80DB1DFE9C934D91EACE6B8A018164  : public RuntimeObject
{
	// UnityEngine.Tilemaps.Tilemap UnityEngine.Tilemaps.ITilemap::m_Tilemap
	Tilemap_t18C4166D0AC702D5BFC0C411FA73C4B61D9D1751* ___m_Tilemap_1;
	// System.Boolean UnityEngine.Tilemaps.ITilemap::m_AddToList
	bool ___m_AddToList_2;
	// System.Int32 UnityEngine.Tilemaps.ITilemap::m_RefreshCount
	int32_t ___m_RefreshCount_3;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector3Int> UnityEngine.Tilemaps.ITilemap::m_RefreshPos
	NativeArray_1_t245D7224A42D1A32B87C64E49B7B434585EC91EF ___m_RefreshPos_4;
};

struct ITilemap_tCD8B9C2D6A80DB1DFE9C934D91EACE6B8A018164_StaticFields
{
	// UnityEngine.Tilemaps.ITilemap UnityEngine.Tilemaps.ITilemap::s_Instance
	ITilemap_tCD8B9C2D6A80DB1DFE9C934D91EACE6B8A018164* ___s_Instance_0;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// SuperTiled2Unity.ReadOnlyAttribute
struct ReadOnlyAttribute_tC7D6593B643C74C635E5BEE10D4BF3383D6701FE  : public PropertyAttribute_t5E0CB5A6CDA6E24CBD4FF26DE3B0C29D8BB54BF0
{
};

// UnityEngine.Tilemaps.TileData
struct TileData_tFB814629D010ABD175127C0BE96FD96EA606E00F 
{
	// System.Int32 UnityEngine.Tilemaps.TileData::m_Sprite
	int32_t ___m_Sprite_0;
	// UnityEngine.Color UnityEngine.Tilemaps.TileData::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_1;
	// UnityEngine.Matrix4x4 UnityEngine.Tilemaps.TileData::m_Transform
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___m_Transform_2;
	// System.Int32 UnityEngine.Tilemaps.TileData::m_GameObject
	int32_t ___m_GameObject_3;
	// UnityEngine.Tilemaps.TileFlags UnityEngine.Tilemaps.TileData::m_Flags
	int32_t ___m_Flags_4;
	// UnityEngine.Tilemaps.Tile/ColliderType UnityEngine.Tilemaps.TileData::m_ColliderType
	int32_t ___m_ColliderType_5;
};

struct TileData_tFB814629D010ABD175127C0BE96FD96EA606E00F_StaticFields
{
	// UnityEngine.Tilemaps.TileData UnityEngine.Tilemaps.TileData::Default
	TileData_tFB814629D010ABD175127C0BE96FD96EA606E00F ___Default_6;
};

// SuperTiled2Unity.CollisionObject/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB  : public RuntimeObject
{
	// UnityEngine.Matrix4x4 SuperTiled2Unity.CollisionObject/<>c__DisplayClass26_0::rotate
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___rotate_0;
};

// SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E  : public RuntimeObject
{
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::matScale
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___matScale_0;
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::matRotate
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___matRotate_1;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.Func`2<UnityEngine.Vector2,UnityEngine.Vector2>
struct Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA  : public MulticastDelegate_t
{
};

// System.Func`2<UnityEngine.Vector2,UnityEngine.Vector3>
struct Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768  : public MulticastDelegate_t
{
};

// System.Func`2<UnityEngine.Vector3,System.Single>
struct Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC  : public MulticastDelegate_t
{
};

// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2>
struct Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B  : public MulticastDelegate_t
{
};

// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4  : public MulticastDelegate_t
{
};

// System.Predicate`1<SuperTiled2Unity.CustomProperty>
struct Predicate_1_tE3859600DB095616566FA8539A8605019C819898  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Tilemaps.TileBase
struct TileBase_t07019BD771D35E8EA68118157D6EEE4C770CF0F9  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
	// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer> UnityEngine.SpriteRenderer::m_SpriteChangeEvent
	UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E* ___m_SpriteChangeEvent_4;
};

// SuperTiled2Unity.SuperTile
struct SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898  : public TileBase_t07019BD771D35E8EA68118157D6EEE4C770CF0F9
{
	// System.Int32 SuperTiled2Unity.SuperTile::m_TileId
	int32_t ___m_TileId_9;
	// UnityEngine.Sprite SuperTiled2Unity.SuperTile::m_Sprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite_10;
	// UnityEngine.Sprite[] SuperTiled2Unity.SuperTile::m_AnimationSprites
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___m_AnimationSprites_11;
	// System.String SuperTiled2Unity.SuperTile::m_Type
	String_t* ___m_Type_12;
	// System.Single SuperTiled2Unity.SuperTile::m_Width
	float ___m_Width_13;
	// System.Single SuperTiled2Unity.SuperTile::m_Height
	float ___m_Height_14;
	// System.Single SuperTiled2Unity.SuperTile::m_TileOffsetX
	float ___m_TileOffsetX_15;
	// System.Single SuperTiled2Unity.SuperTile::m_TileOffsetY
	float ___m_TileOffsetY_16;
	// SuperTiled2Unity.ObjectAlignment SuperTiled2Unity.SuperTile::m_ObjectAlignment
	int32_t ___m_ObjectAlignment_17;
	// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty> SuperTiled2Unity.SuperTile::m_CustomProperties
	List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* ___m_CustomProperties_18;
	// System.Collections.Generic.List`1<SuperTiled2Unity.CollisionObject> SuperTiled2Unity.SuperTile::m_CollisionObjects
	List_1_tD0C437F57D6FF6931FC5080DADDD0F9557D71D02* ___m_CollisionObjects_19;
};

struct SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields
{
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::HorizontalFlipMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___HorizontalFlipMatrix_4;
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::VerticalFlipMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___VerticalFlipMatrix_5;
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::DiagonalFlipMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___DiagonalFlipMatrix_6;
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::Rotate60Matrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___Rotate60Matrix_7;
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::Rotate120Matrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___Rotate120Matrix_8;
};

// UnityEngine.Tilemaps.TilemapRenderer
struct TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};

// SuperTiled2Unity.SuperColliderComponent
struct SuperColliderComponent_tC658CE43E321DE9458EF2777905A81C57426E419  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<SuperTiled2Unity.SuperColliderComponent/Shape> SuperTiled2Unity.SuperColliderComponent::m_PolygonShapes
	List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17* ___m_PolygonShapes_4;
	// System.Collections.Generic.List`1<SuperTiled2Unity.SuperColliderComponent/Shape> SuperTiled2Unity.SuperColliderComponent::m_OutlineShapes
	List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17* ___m_OutlineShapes_5;
};

// SuperTiled2Unity.SuperCustomProperties
struct SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty> SuperTiled2Unity.SuperCustomProperties::m_Properties
	List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* ___m_Properties_4;
};

// SuperTiled2Unity.SuperLayer
struct SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String SuperTiled2Unity.SuperLayer::m_TiledName
	String_t* ___m_TiledName_4;
	// System.Single SuperTiled2Unity.SuperLayer::m_OffsetX
	float ___m_OffsetX_5;
	// System.Single SuperTiled2Unity.SuperLayer::m_OffsetY
	float ___m_OffsetY_6;
	// System.Single SuperTiled2Unity.SuperLayer::m_ParallaxX
	float ___m_ParallaxX_7;
	// System.Single SuperTiled2Unity.SuperLayer::m_ParallaxY
	float ___m_ParallaxY_8;
	// System.Single SuperTiled2Unity.SuperLayer::m_Opacity
	float ___m_Opacity_9;
	// UnityEngine.Color SuperTiled2Unity.SuperLayer::m_TintColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_TintColor_10;
	// System.Boolean SuperTiled2Unity.SuperLayer::m_Visible
	bool ___m_Visible_11;
};

// SuperTiled2Unity.SuperMap
struct SuperMap_tBBCDA02EE1A6B1D4E3B0B7D85C4D6A129280177D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String SuperTiled2Unity.SuperMap::m_Version
	String_t* ___m_Version_4;
	// System.String SuperTiled2Unity.SuperMap::m_TiledVersion
	String_t* ___m_TiledVersion_5;
	// SuperTiled2Unity.MapOrientation SuperTiled2Unity.SuperMap::m_Orientation
	int32_t ___m_Orientation_6;
	// SuperTiled2Unity.MapRenderOrder SuperTiled2Unity.SuperMap::m_RenderOrder
	int32_t ___m_RenderOrder_7;
	// System.Int32 SuperTiled2Unity.SuperMap::m_Width
	int32_t ___m_Width_8;
	// System.Int32 SuperTiled2Unity.SuperMap::m_Height
	int32_t ___m_Height_9;
	// System.Int32 SuperTiled2Unity.SuperMap::m_TileWidth
	int32_t ___m_TileWidth_10;
	// System.Int32 SuperTiled2Unity.SuperMap::m_TileHeight
	int32_t ___m_TileHeight_11;
	// System.Int32 SuperTiled2Unity.SuperMap::m_HexSideLength
	int32_t ___m_HexSideLength_12;
	// SuperTiled2Unity.StaggerAxis SuperTiled2Unity.SuperMap::m_StaggerAxis
	int32_t ___m_StaggerAxis_13;
	// SuperTiled2Unity.StaggerIndex SuperTiled2Unity.SuperMap::m_StaggerIndex
	int32_t ___m_StaggerIndex_14;
	// System.Boolean SuperTiled2Unity.SuperMap::m_Infinite
	bool ___m_Infinite_15;
	// UnityEngine.Color SuperTiled2Unity.SuperMap::m_BackgroundColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_BackgroundColor_16;
	// System.Int32 SuperTiled2Unity.SuperMap::m_NextObjectId
	int32_t ___m_NextObjectId_17;
};

// SuperTiled2Unity.SuperObject
struct SuperObject_t69DFDC213711BB2C894AB42B71BFF3238035C06E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 SuperTiled2Unity.SuperObject::m_Id
	int32_t ___m_Id_4;
	// System.String SuperTiled2Unity.SuperObject::m_TiledName
	String_t* ___m_TiledName_5;
	// System.String SuperTiled2Unity.SuperObject::m_Type
	String_t* ___m_Type_6;
	// System.Single SuperTiled2Unity.SuperObject::m_X
	float ___m_X_7;
	// System.Single SuperTiled2Unity.SuperObject::m_Y
	float ___m_Y_8;
	// System.Single SuperTiled2Unity.SuperObject::m_Width
	float ___m_Width_9;
	// System.Single SuperTiled2Unity.SuperObject::m_Height
	float ___m_Height_10;
	// System.Single SuperTiled2Unity.SuperObject::m_Rotation
	float ___m_Rotation_11;
	// System.UInt32 SuperTiled2Unity.SuperObject::m_TileId
	uint32_t ___m_TileId_12;
	// SuperTiled2Unity.SuperTile SuperTiled2Unity.SuperObject::m_SuperTile
	SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___m_SuperTile_13;
	// System.Boolean SuperTiled2Unity.SuperObject::m_Visible
	bool ___m_Visible_14;
	// System.String SuperTiled2Unity.SuperObject::m_Template
	String_t* ___m_Template_15;
};

// SuperTiled2Unity.SuperTilesAsObjectsTilemap
struct SuperTilesAsObjectsTilemap_t541B0111712AA3121005663AF25E0440918656B4  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// SuperTiled2Unity.SuperWorld
struct SuperWorld_t7FDD3E6EC0C0633E0F9A88892A9C5B500E5DB97B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// SuperTiled2Unity.TileObjectAnimator
struct TileObjectAnimator_t136148EED3B0B64B585CB1A265930B96EF0E821F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single SuperTiled2Unity.TileObjectAnimator::m_AnimationFramerate
	float ___m_AnimationFramerate_4;
	// UnityEngine.Sprite[] SuperTiled2Unity.TileObjectAnimator::m_AnimationSprites
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___m_AnimationSprites_5;
	// System.Single SuperTiled2Unity.TileObjectAnimator::m_Timer
	float ___m_Timer_6;
	// System.Int32 SuperTiled2Unity.TileObjectAnimator::m_AnimationIndex
	int32_t ___m_AnimationIndex_7;
};

// SuperTiled2Unity.SuperGroupLayer
struct SuperGroupLayer_tC73291F06FA4A186FAB5BCBD8FE92853B5DE2AB1  : public SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76
{
};

// SuperTiled2Unity.SuperImageLayer
struct SuperImageLayer_t7023C8E04FA266112B5F0F2321FB2051A4791EDB  : public SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76
{
	// System.String SuperTiled2Unity.SuperImageLayer::m_ImageFilename
	String_t* ___m_ImageFilename_12;
};

// SuperTiled2Unity.SuperObjectLayer
struct SuperObjectLayer_tE988ACCE14F2B9F055CFE9E0256B2597C0DCC995  : public SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76
{
	// UnityEngine.Color SuperTiled2Unity.SuperObjectLayer::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_12;
};

// SuperTiled2Unity.SuperTileLayer
struct SuperTileLayer_t100FD3159D41613E8B5DE0EF08F08629B6FC1B69  : public SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76
{
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 m_Items[1];

	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// SuperTiled2Unity.SuperLayer[]
struct SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52  : public RuntimeArray
{
	ALIGN_FIELD (8) SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* m_Items[1];

	inline SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Tilemaps.TilemapRenderer[]
struct TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53  : public RuntimeArray
{
	ALIGN_FIELD (8) TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB* m_Items[1];

	inline TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 m_Items[1];

	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B  : public RuntimeArray
{
	ALIGN_FIELD (8) Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* m_Items[1];

	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Func`2<UnityEngine.Vector2,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mAB215938138B4DAC89DA24CC6B00066F2942477C_gshared (Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector2,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C_gshared (RuntimeObject* ___source0, Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA* ___selector1, const RuntimeMethod* method) ;
// TSource[] System.Linq.Enumerable::ToArray<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB_gshared (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Func`2<UnityEngine.Vector2,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m5E7087CFE7EE090E6F0CA2843146A70D4CAAA49E_gshared (Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector2,UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mA5D7A04386B3B335680CF888F4516918A1100459_gshared (RuntimeObject* ___source0, Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768* ___selector1, const RuntimeMethod* method) ;
// System.Void System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mB1CEAB608E19C24C9324214823684B3D4195DD87_gshared (Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector3,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D6DCFEC603609B8CEFE5F7186E6CB0903999035_gshared (RuntimeObject* ___source0, Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* ___selector1, const RuntimeMethod* method) ;
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Predicate_1__ctor_m3E007299121A15DF80F4A210FF8C20E5DF688F20_gshared (Predicate_1_t8342C85FF4E41CD1F7024AC0CDC3E5312A32CB12* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* List_1_Find_m5E78A210541B0D844FE27B94F509313623BE33D3_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, Predicate_1_t8342C85FF4E41CD1F7024AC0CDC3E5312A32CB12* ___match0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t List_1_RemoveAll_m1A8DE2A7640CC473609F3ADAC38FDB960520636D_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, Predicate_1_t8342C85FF4E41CD1F7024AC0CDC3E5312A32CB12* ___match0, const RuntimeMethod* method) ;
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* GameObject_GetComponentsInParent_TisRuntimeObject_mDEBA0826429C4C398C9CEDF9F5520707A897AE76_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Component_GetComponentsInChildren_TisRuntimeObject_m1F5B6FC0689B07D4FAAC0C605D9B2933A9B32543_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m3B1BCBC0885F3E93CDC21C75185F09A25FE0CC17_gshared (Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector3,UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5_gshared (RuntimeObject* ___source0, Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4* ___selector1, const RuntimeMethod* method) ;
// TSource[] System.Linq.Enumerable::ToArray<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75_gshared (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Func`2<UnityEngine.Vector3,System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m8BACCCB996FEF1B06E74F85966B4231A9A24DAF0_gshared (Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector3,System.Single>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B_gshared (RuntimeObject* ___source0, Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* ___selector1, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;

// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) ;
// System.Void SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass23_0__ctor_m8DE5A4BC5950A73655C5C2AC805A0967676BEFC4 (U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::IsometricTransform(UnityEngine.Vector2,SuperTiled2Unity.SuperTile,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 CollisionObject_IsometricTransform_m59113D4A7DBD20C375B1B0D596519BDB9395CEBE (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pt0, SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___gridSize2, const RuntimeMethod* method) ;
// System.Void SuperTiled2Unity.CollisionObject::ApplyRotationToPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject_ApplyRotationToPoints_m512B1BE5CCABAA6514BC6E76C9A261E159B6B100 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, const RuntimeMethod* method) ;
// System.Void System.Func`2<UnityEngine.Vector2,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mAB215938138B4DAC89DA24CC6B00066F2942477C (Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mAB215938138B4DAC89DA24CC6B00066F2942477C_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector2,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C (RuntimeObject* ___source0, Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA*, const RuntimeMethod*))Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C_gshared)(___source0, ___selector1, method);
}
// TSource[] System.Linq.Enumerable::ToArray<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<TSource>)
inline Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB_gshared)(___source0, method);
}
// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::LocalTransform(UnityEngine.Vector2,SuperTiled2Unity.SuperTile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 CollisionObject_LocalTransform_m4BBC2EED1964CAC87D946F6B20C84A395BA06018 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pt0, SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile1, const RuntimeMethod* method) ;
// System.Void SuperTiled2Unity.CollisionObject/<>c__DisplayClass26_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass26_0__ctor_mCBE0212AE0DBE64819687EB9DD21E60F17916C9C (U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB* __this, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 SuperTiled2Unity.MatrixUtils::Rotate2d(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 MatrixUtils_Rotate2d_m94977337A24271488FF3F281D11DF921A20079DA (float ___m000, float ___m011, float ___m102, float ___m113, const RuntimeMethod* method) ;
// System.Void System.Func`2<UnityEngine.Vector2,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5E7087CFE7EE090E6F0CA2843146A70D4CAAA49E (Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m5E7087CFE7EE090E6F0CA2843146A70D4CAAA49E_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector2,UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mA5D7A04386B3B335680CF888F4516918A1100459 (RuntimeObject* ___source0, Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768*, const RuntimeMethod*))Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mA5D7A04386B3B335680CF888F4516918A1100459_gshared)(___source0, ___selector1, method);
}
// System.Void System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mB1CEAB608E19C24C9324214823684B3D4195DD87 (Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mB1CEAB608E19C24C9324214823684B3D4195DD87_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector3,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D6DCFEC603609B8CEFE5F7186E6CB0903999035 (RuntimeObject* ___source0, Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B*, const RuntimeMethod*))Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D6DCFEC603609B8CEFE5F7186E6CB0903999035_gshared)(___source0, ___selector1, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___v0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___point0, const RuntimeMethod* method) ;
// System.Void SuperTiled2Unity.CollisionObject/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6383F2FDA8B99E0D3A293C2E26B2B149404F8EA2 (U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Implicit_m8F73B300CB4E6F9B4EB5FB6130363D76CEAA230B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___v0, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A (String_t* ___value0, const RuntimeMethod* method) ;
// System.Void SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_0__ctor_mBAB88BBA8C5224AE6CAC9C848DD1643518E5E3A1 (U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA* __this, const RuntimeMethod* method) ;
// System.Void System.Predicate`1<SuperTiled2Unity.CustomProperty>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m84717899DF8ECB94CB79C4D3E190FBA288570664 (Predicate_1_tE3859600DB095616566FA8539A8605019C819898* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_tE3859600DB095616566FA8539A8605019C819898*, RuntimeObject*, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m3E007299121A15DF80F4A210FF8C20E5DF688F20_gshared)(__this, ___object0, ___method1, method);
}
// T System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>::Find(System.Predicate`1<T>)
inline CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* List_1_Find_mE71D47CAE16DFDCE85DAEA6732CCF78DCB0AEEE5 (List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* __this, Predicate_1_tE3859600DB095616566FA8539A8605019C819898* ___match0, const RuntimeMethod* method)
{
	return ((  CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* (*) (List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D*, Predicate_1_tE3859600DB095616566FA8539A8605019C819898*, const RuntimeMethod*))List_1_Find_m5E78A210541B0D844FE27B94F509313623BE33D3_gshared)(__this, ___match0, method);
}
// System.Boolean System.String::Equals(System.String,System.String,System.StringComparison)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_m80124ECC809968E69F952E2A49EBC03F81A23E43 (String_t* ___a0, String_t* ___b1, int32_t ___comparisonType2, const RuntimeMethod* method) ;
// UnityEngine.Color SuperTiled2Unity.StringExtensions::ToColor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F StringExtensions_ToColor_mCA880BC04D74E0EC9D926C72B478F58027A082D1 (String_t* ___htmlString0, const RuntimeMethod* method) ;
// System.Int32 SuperTiled2Unity.StringExtensions::ToInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StringExtensions_ToInt_m1B2045627BF608C15A803476AA9DB54B149B1E76 (String_t* ___str0, const RuntimeMethod* method) ;
// System.Single SuperTiled2Unity.StringExtensions::ToFloat(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float StringExtensions_ToFloat_m41589007A7AF2104695878FDB4D80E3AFAAFA496 (String_t* ___str0, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.StringExtensions::ToBool(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StringExtensions_ToBool_mF36AD979ADB49B4DE575A0CC84CBFFC6CA0795F0 (String_t* ___str0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<SuperTiled2Unity.SuperCustomProperties>()
inline SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* GameObject_GetComponent_TisSuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE_m62AF091848A5ACFB37052C7AF9F74C9484B0EE32 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Boolean SuperTiled2Unity.SuperCustomProperties::TryGetCustomProperty(System.String,SuperTiled2Unity.CustomProperty&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SuperCustomProperties_TryGetCustomProperty_m621D9CBDF999333FB4836A496DA074ED1BFDD981 (SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* __this, String_t* ___name0, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** ___property1, const RuntimeMethod* method) ;
// System.Boolean System.String::StartsWith(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_StartsWith_mF75DBA1EB709811E711B44E26FF919C88A8E65C0 (String_t* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.String System.String::Remove(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Remove_m4D7A58E2124F8D0D8AE3EEDE74B6AD6A863ABA68 (String_t* __this, int32_t ___startIndex0, int32_t ___count1, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE (String_t* __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method) ;
// System.Byte System.Byte::Parse(System.String,System.Globalization.NumberStyles)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t Byte_Parse_m1D565A1D1F7DF553B5F6CF87AC883B9BE0A6A2D3 (String_t* ___s0, int32_t ___style1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color32_op_Implicit_m203A634DBB77053C9400C68065CA29529103D172_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___c0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogErrorFormat(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogErrorFormat_mA33C95EF832A60D72A7EE26074E13A86BE7E30C6 (String_t* ___format0, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___args1, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_magenta()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_magenta_mF552F660CB0E42F18558AD59D516EBAC923F57E2_inline (const RuntimeMethod* method) ;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* CultureInfo_get_InvariantCulture_m78DAB8CBE8766445310782B6E61FB7A9983AD425 (const RuntimeMethod* method) ;
// System.Boolean System.Single::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_TryParse_m5920BF3A60EE1FFF0CC117021B4BF2A8114D1AE5 (String_t* ___s0, int32_t ___style1, RuntimeObject* ___provider2, float* ___result3, const RuntimeMethod* method) ;
// System.Boolean System.Int32::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int32_TryParse_m3CB3A8252B2254BF929D207AFA9F2CD4DA3E3F79 (String_t* ___s0, int32_t ___style1, RuntimeObject* ___provider2, int32_t* ___result3, const RuntimeMethod* method) ;
// System.Boolean System.String::Equals(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_mCD5F35DEDCAFE51ACD4E033726FC2EF8DF7E9B4D (String_t* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Boolean System.String::Equals(System.String,System.StringComparison)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_m7BDFC0B951005B9DC2BAED464AFE68FF7E9ACE5A (String_t* __this, String_t* ___value0, int32_t ___comparisonType1, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.CustomPropertyListExtensions::TryGetProperty(System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>,System.String,SuperTiled2Unity.CustomProperty&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CustomPropertyListExtensions_TryGetProperty_m262897E51BA2D6BB72B492DA7FF7D4FEC479BB5B (List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* ___list0, String_t* ___propertyName1, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** ___property2, const RuntimeMethod* method) ;
// System.String SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsString(SuperTiled2Unity.SuperTile,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SuperTileExtensions_GetPropertyValueAsString_m2D9D3FDA56279F63FB2FD59FEDF28429B416AF5E (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, String_t* ___defaultValue2, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.SuperTileExtensions::TryGetProperty(SuperTiled2Unity.SuperTile,System.String,SuperTiled2Unity.CustomProperty&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** ___property2, const RuntimeMethod* method) ;
// System.String SuperTiled2Unity.CustomPropertyExtensions::GetValueAsString(SuperTiled2Unity.CustomProperty)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CustomPropertyExtensions_GetValueAsString_m692FA0C6470995727534A812C2FD7B7D1264D577_inline (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsBool(SuperTiled2Unity.SuperTile,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SuperTileExtensions_GetPropertyValueAsBool_m7F16749D7A003164C8E166412E87955874412F83 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, bool ___defaultValue2, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.CustomPropertyExtensions::GetValueAsBool(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CustomPropertyExtensions_GetValueAsBool_mC15F3B153F093F3249890AAFC2DCB0317685F28D (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) ;
// System.Int32 SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsInt(SuperTiled2Unity.SuperTile,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SuperTileExtensions_GetPropertyValueAsInt_m3970CC724020E9F46C4387FA8C911F7908EA7E3C (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, int32_t ___defaultValue2, const RuntimeMethod* method) ;
// System.Int32 SuperTiled2Unity.CustomPropertyExtensions::GetValueAsInt(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CustomPropertyExtensions_GetValueAsInt_m08EBBA564426968A0AD4A00BDC2E01E9A3C98B7D (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) ;
// System.Single SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsFloat(SuperTiled2Unity.SuperTile,System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SuperTileExtensions_GetPropertyValueAsFloat_m7B0D6B7A93DE789BC97C462A9B9168C60B9523DC (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, float ___defaultValue2, const RuntimeMethod* method) ;
// System.Single SuperTiled2Unity.CustomPropertyExtensions::GetValueAsFloat(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CustomPropertyExtensions_GetValueAsFloat_m7A0545C8D8D20891BC596F1D31A40FBD2B10F27C (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_clear_m8B58EA88C92F7DD2C66F0EC1BCC8AC697D631298_inline (const RuntimeMethod* method) ;
// UnityEngine.Color SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsColor(SuperTiled2Unity.SuperTile,System.String,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F SuperTileExtensions_GetPropertyValueAsColor_m612E3C0B943694EF561332143F07AA0862DB7E70 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___defaultValue2, const RuntimeMethod* method) ;
// UnityEngine.Color SuperTiled2Unity.CustomPropertyExtensions::GetValueAsColor(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F CustomPropertyExtensions_GetValueAsColor_mECD5B66015F0470AD850B9886361BF0EBB795422 (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_get_identity_m94A09872C449C26863FF10D0FDF87842D91BECD6_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Void UnityEngine.PropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAttribute__ctor_m19247686E165101F140615C7306DC2DA3953D97D (PropertyAttribute_t5E0CB5A6CDA6E24CBD4FF26DE3B0C29D8BB54BF0* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<SuperTiled2Unity.SuperColliderComponent/Shape>::.ctor()
inline void List_1__ctor_m65D107AE0F3BB61F0C374671D35DBB2181FF6F95 (List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void SuperTiled2Unity.SuperCustomProperties/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mABC57E0F328DE14C90D613679DBBFFB4FB78A8D8 (U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452* __this, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>::RemoveAll(System.Predicate`1<T>)
inline int32_t List_1_RemoveAll_mFE2C6425D80C8A9E05C9B8ADB56C91773C9EC1C5 (List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* __this, Predicate_1_tE3859600DB095616566FA8539A8605019C819898* ___match0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D*, Predicate_1_tE3859600DB095616566FA8539A8605019C819898*, const RuntimeMethod*))List_1_RemoveAll_m1A8DE2A7640CC473609F3ADAC38FDB960520636D_gshared)(__this, ___match0, method);
}
// System.Void SuperTiled2Unity.SuperLayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperLayer__ctor_m27C9812BCE3C7EF1A912DAB7F323C8C56BFE82E9 (SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* __this, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline (const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// T[] UnityEngine.GameObject::GetComponentsInParent<SuperTiled2Unity.SuperLayer>()
inline SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponentsInParent_TisRuntimeObject_mDEBA0826429C4C398C9CEDF9F5520707A897AE76_gshared)(__this, method);
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,UnityEngine.Color)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_op_Multiply_mF17D278EB0ABC9AEB32E829D5CA98784E0D6B66F_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___a0, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___b1, const RuntimeMethod* method) ;
// T[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Tilemaps.TilemapRenderer>()
inline TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53* Component_GetComponentsInChildren_TisTilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB_m3CFA932712E5F15C704FAA4A95AB809344841ED8 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m1F5B6FC0689B07D4FAAC0C605D9B2933A9B32543_gshared)(__this, method);
}
// System.Void UnityEngine.Tilemaps.TilemapRenderer::set_detectChunkCullingBounds(UnityEngine.Tilemaps.TilemapRenderer/DetectChunkCullingBounds)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TilemapRenderer_set_detectChunkCullingBounds_m7EECA4EC0AE104C34E7B8BC240E6CBA6B8234153 (TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB* __this, int32_t ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector3Int SuperTiled2Unity.SuperMap::TiledCellToGridCell(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 SuperMap_TiledCellToGridCell_m5FEBC722CCC4F177AD34568AC83330C76C094681 (SuperMap_tBBCDA02EE1A6B1D4E3B0B7D85C4D6A129280177D* __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Vector3Int::get_y()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector3Int_get_y_m42F43000F85D356557CAF03442273E7AA08F7F72_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3Int::set_y(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int_set_y_mA856F32D1BF187BD4091DDF3C6872FD01F7D3377_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3Int::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Vector3Int::get_x()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector3Int_get_x_m21C268D2AA4C03CE35AA49DF6155347C9748054C_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3Int::set_x(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int_set_x_m8745C5976D035EBBAC6F6191B5838D58631D8685_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Sprite_get_pixelsPerUnit_m5A5984BC298062DF4CD2CB3E8534443FFCF31826 (Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* __this, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Translate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_Translate_m95D44EDD1A9856DD11C639692E47B7A35EF745E2 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___vector0, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.FlipFlagsMask::FlippedHorizontally(SuperTiled2Unity.FlipFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlipFlagsMask_FlippedHorizontally_mEEFD6BFA6D3842CB5425125A29735904B90F03A1 (int32_t ___flags0, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.FlipFlagsMask::FlippedVertically(SuperTiled2Unity.FlipFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlipFlagsMask_FlippedVertically_mB596859EF886CE6219BC0B4E5697260B598269C3 (int32_t ___flags0, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.FlipFlagsMask::RotatedDiagonally(SuperTiled2Unity.FlipFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlipFlagsMask_RotatedDiagonally_mECF1DF8F2B9DC31F7F2060FA362BDC8D7C47D2F5 (int32_t ___flags0, const RuntimeMethod* method) ;
// System.Boolean SuperTiled2Unity.FlipFlagsMask::RotatedHexagonally120(SuperTiled2Unity.FlipFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlipFlagsMask_RotatedHexagonally120_m47701C0290627A54236243EF6079E27F53FF6B89 (int32_t ___flags0, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_UnaryNegation_m47556D28F72B018AC4D5160710C83A805F10A783_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0 (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___lhs0, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___rhs1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_one_mE6A2D5C6578E94268024613B596BF09F990B1260_inline (const RuntimeMethod* method) ;
// System.Void SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass17_0__ctor_m97CDB1D70C523C541D148070D2B82323FE8EEAA6 (U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* __this, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Scale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_Scale_m389397AD581D1BB1A5D39B47021DD685A1EAA9AB (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___vector0, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_m66E346161C9778DF8486DB4FE823D8F81A54AF1D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___euler0, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Rotate(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_Rotate_mE2C31B51EEC282F2969B9C2BE24BD73E312807E8 (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___q0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m5F87930F9B0828E5652E2D9D01ED907C01122C86_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, const RuntimeMethod* method) ;
// System.Void System.Func`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m3B1BCBC0885F3E93CDC21C75185F09A25FE0CC17 (Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m3B1BCBC0885F3E93CDC21C75185F09A25FE0CC17_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector3,UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5 (RuntimeObject* ___source0, Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4*, const RuntimeMethod*))Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5_gshared)(___source0, ___selector1, method);
}
// TSource[] System.Linq.Enumerable::ToArray<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<TSource>)
inline Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75_gshared)(___source0, method);
}
// System.Void System.Func`2<UnityEngine.Vector3,System.Single>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m8BACCCB996FEF1B06E74F85966B4231A9A24DAF0 (Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m8BACCCB996FEF1B06E74F85966B4231A9A24DAF0_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<UnityEngine.Vector3,System.Single>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B (RuntimeObject* ___source0, Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC*, const RuntimeMethod*))Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B_gshared)(___source0, ___selector1, method);
}
// System.Single System.Linq.Enumerable::Min(System.Collections.Generic.IEnumerable`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Enumerable_Min_mAD5CE1B44AFFA09AEDCC1F32CE0C00ADAA445D3F (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Tilemaps.TileData::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TileData_set_sprite_m3566544847F9C9C27EDB154324B6FBDB446EFE94 (TileData_tFB814629D010ABD175127C0BE96FD96EA606E00F* __this, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Tilemaps.TileAnimationData::set_animatedSprites(UnityEngine.Sprite[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TileAnimationData_set_animatedSprites_m315FE8DAB5071E1FA594AEA74B1B66BBF6A5C3E1 (TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149* __this, SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Tilemaps.TileAnimationData::set_animationSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TileAnimationData_set_animationSpeed_mE1DB382A9D7F0385D70248A93B998405890D4611 (TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149* __this, float ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Tilemaps.TileAnimationData::set_animationStartTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TileAnimationData_set_animationStartTime_mBC2F61289403253C6B43C12576A98654B94A9B40 (TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149* __this, float ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Tilemaps.TileBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TileBase__ctor_mBFD0A0ACF9DB1F08783B9F3F35D4E61C9205D4A2 (TileBase_t07019BD771D35E8EA68118157D6EEE4C770CF0F9* __this, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Void SuperTiled2Unity.SuperTile/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mAEBBF578A513958C40FF2051364E82C0A4C8188E (U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D (const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_m7B176E33955108C60CAE21DFC153A0FAC674CB53 (SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* __this, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Internal_FromEulerRad_m2842B9FFB31CDC0F80B7C2172E22831D11D91E93 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___euler0, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2[] SuperTiled2Unity.CollisionObject::get_Points()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* CollisionObject_get_Points_mDC299097F3812479DAE12DBF1AC785F93689B734 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, const RuntimeMethod* method) 
{
	{
		// public Vector2[] Points { get { return m_Points; } }
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_0 = __this->___m_Points_9;
		return L_0;
	}
}
// System.Boolean SuperTiled2Unity.CollisionObject::get_IsClosed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CollisionObject_get_IsClosed_mBCD345D0D56E05C4EC74A01581CEB3E8B2F4B674 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, const RuntimeMethod* method) 
{
	{
		// public bool IsClosed { get { return m_IsClosed; } }
		bool L_0 = __this->___m_IsClosed_10;
		return L_0;
	}
}
// SuperTiled2Unity.CollisionShapeType SuperTiled2Unity.CollisionObject::get_CollisionShapeType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CollisionObject_get_CollisionShapeType_m4F76225AC158808859D5411B8FEB5A16AC308C6B (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, const RuntimeMethod* method) 
{
	{
		// public CollisionShapeType CollisionShapeType { get { return m_CollisionShapeType; } }
		int32_t L_0 = __this->___m_CollisionShapeType_11;
		return L_0;
	}
}
// System.Void SuperTiled2Unity.CollisionObject::MakePointsFromRectangle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject_MakePointsFromRectangle_m79B21AC37EE53B2CF34E485D2CFD8848FFFD7FDF (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_CollisionShapeType = CollisionShapeType.Rectangle;
		__this->___m_CollisionShapeType_11 = 0;
		// m_IsClosed = true;
		__this->___m_IsClosed_10 = (bool)1;
		// m_Points = new Vector2[4];
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_0 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->___m_Points_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Points_9), (void*)L_0);
		// m_Points[0] = Vector2.zero;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_1 = __this->___m_Points_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		L_2 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_2);
		// m_Points[1] = new Vector2(0, m_Size.y);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_3 = __this->___m_Points_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_4 = (&__this->___m_Size_4);
		float L_5 = L_4->___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), (0.0f), L_5, /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_6);
		// m_Points[2] = new Vector2(m_Size.x, m_Size.y);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_7 = __this->___m_Points_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_8 = (&__this->___m_Size_4);
		float L_9 = L_8->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_10 = (&__this->___m_Size_4);
		float L_11 = L_10->___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_12), L_9, L_11, /*hidden argument*/NULL);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_12);
		// m_Points[3] = new Vector2(m_Size.x, 0);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_13 = __this->___m_Points_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_14 = (&__this->___m_Size_4);
		float L_15 = L_14->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_16), L_15, (0.0f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_16);
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.CollisionObject::MakePoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject_MakePoint_m41B58C685C263D1070B6A709FA4173522A317688 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_CollisionShapeType = CollisionShapeType.Point;
		__this->___m_CollisionShapeType_11 = 4;
		// m_IsClosed = false;
		__this->___m_IsClosed_10 = (bool)0;
		// m_Points = new Vector2[1];
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_0 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)1);
		__this->___m_Points_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Points_9), (void*)L_0);
		// m_Points[0] = m_Position;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_1 = __this->___m_Points_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = __this->___m_Position_3;
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_2);
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.CollisionObject::MakePointsFromEllipse(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject_MakePointsFromEllipse_mA70B1853AFD309BE4337FF6F75FC989AB662DB52 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, int32_t ___numEdges0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	{
		// m_CollisionShapeType = CollisionShapeType.Ellipse;
		__this->___m_CollisionShapeType_11 = 1;
		// m_IsClosed = true;
		__this->___m_IsClosed_10 = (bool)1;
		// float theta = ((float)Math.PI * 2.0f) / numEdges;
		int32_t L_0 = ___numEdges0;
		V_0 = ((float)((6.28318548f)/((float)L_0)));
		// float half_x = m_Size.x * 0.5f;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_1 = (&__this->___m_Size_4);
		float L_2 = L_1->___x_0;
		V_1 = ((float)il2cpp_codegen_multiply(L_2, (0.5f)));
		// float half_y = m_Size.y * 0.5f;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_3 = (&__this->___m_Size_4);
		float L_4 = L_3->___y_1;
		V_2 = ((float)il2cpp_codegen_multiply(L_4, (0.5f)));
		// m_Points = new Vector2[numEdges];
		int32_t L_5 = ___numEdges0;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_6 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_5);
		__this->___m_Points_9 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Points_9), (void*)L_6);
		// for (int i = 0; i < numEdges; i++)
		V_3 = 0;
		goto IL_008b;
	}

IL_004b:
	{
		// m_Points[i].x = half_x + half_x * Mathf.Cos(theta * i);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_7 = __this->___m_Points_9;
		int32_t L_8 = V_3;
		float L_9 = V_1;
		float L_10 = V_1;
		float L_11 = V_0;
		int32_t L_12 = V_3;
		float L_13;
		L_13 = cosf(((float)il2cpp_codegen_multiply(L_11, ((float)L_12))));
		((L_7)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_8)))->___x_0 = ((float)il2cpp_codegen_add(L_9, ((float)il2cpp_codegen_multiply(L_10, L_13))));
		// m_Points[i].y = half_y + half_y * Mathf.Sin(theta * i);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_14 = __this->___m_Points_9;
		int32_t L_15 = V_3;
		float L_16 = V_2;
		float L_17 = V_2;
		float L_18 = V_0;
		int32_t L_19 = V_3;
		float L_20;
		L_20 = sinf(((float)il2cpp_codegen_multiply(L_18, ((float)L_19))));
		((L_14)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_15)))->___y_1 = ((float)il2cpp_codegen_add(L_16, ((float)il2cpp_codegen_multiply(L_17, L_20))));
		// for (int i = 0; i < numEdges; i++)
		int32_t L_21 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_21, 1));
	}

IL_008b:
	{
		// for (int i = 0; i < numEdges; i++)
		int32_t L_22 = V_3;
		int32_t L_23 = ___numEdges0;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_004b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.CollisionObject::MakePointsFromPolygon(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject_MakePointsFromPolygon_m50FBB4A510348191E291D989168C142C58AB0F79 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___points0, const RuntimeMethod* method) 
{
	{
		// m_CollisionShapeType = CollisionShapeType.Polygon;
		__this->___m_CollisionShapeType_11 = 2;
		// m_IsClosed = true;
		__this->___m_IsClosed_10 = (bool)1;
		// m_Points = points;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_0 = ___points0;
		__this->___m_Points_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Points_9), (void*)L_0);
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.CollisionObject::MakePointsFromPolyline(UnityEngine.Vector2[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject_MakePointsFromPolyline_m6187F93C5E266B1A15A6C68B3A13296A7C444BB1 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___points0, const RuntimeMethod* method) 
{
	{
		// m_CollisionShapeType = CollisionShapeType.Polyline;
		__this->___m_CollisionShapeType_11 = 3;
		// m_IsClosed = false;
		__this->___m_IsClosed_10 = (bool)0;
		// m_Points = points;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_0 = ___points0;
		__this->___m_Points_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Points_9), (void*)L_0);
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.CollisionObject::RenderPoints(SuperTiled2Unity.SuperTile,SuperTiled2Unity.GridOrientation,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject_RenderPoints_m0F5F6114893BECD3F44230F675DE29646D665C2D (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, int32_t ___orientation1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___gridSize2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__0_m027D21D5381DD2013D9011B4631C89AF06AC5898_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__1_m6E76FA998AF975862FF19F35A85976C8E737C919_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* V_0 = NULL;
	int32_t V_1 = 0;
	{
		U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* L_0 = (U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass23_0__ctor_m8DE5A4BC5950A73655C5C2AC805A0967676BEFC4(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* L_1 = V_0;
		L_1->___U3CU3E4__this_0 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_0), (void*)__this);
		U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* L_2 = V_0;
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_3 = ___tile0;
		L_2->___tile_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_2->___tile_1), (void*)L_3);
		// if (orientation == GridOrientation.Isometric)
		int32_t L_4 = ___orientation1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_009f;
		}
	}
	{
		// m_Position = IsometricTransform(m_Position, tile, gridSize);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = __this->___m_Position_3;
		U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* L_6 = V_0;
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_7 = L_6->___tile_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8 = ___gridSize2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9;
		L_9 = CollisionObject_IsometricTransform_m59113D4A7DBD20C375B1B0D596519BDB9395CEBE(__this, L_5, L_7, L_8, NULL);
		__this->___m_Position_3 = L_9;
		// m_Position.x += gridSize.x * 0.5f;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_10 = (&__this->___m_Position_3);
		float* L_11 = (&L_10->___x_0);
		float* L_12 = L_11;
		float L_13 = *((float*)L_12);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_14 = ___gridSize2;
		float L_15 = L_14.___x_0;
		*((float*)L_12) = (float)((float)il2cpp_codegen_add(L_13, ((float)il2cpp_codegen_multiply(L_15, (0.5f)))));
		// for (int i = 0; i < m_Points.Length; i++)
		V_1 = 0;
		goto IL_007c;
	}

IL_0053:
	{
		// m_Points[i] = IsometricTransform(m_Points[i], tile, gridSize);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_16 = __this->___m_Points_9;
		int32_t L_17 = V_1;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_18 = __this->___m_Points_9;
		int32_t L_19 = V_1;
		int32_t L_20 = L_19;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_21 = (L_18)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_20));
		U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* L_22 = V_0;
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_23 = L_22->___tile_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_24 = ___gridSize2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_25;
		L_25 = CollisionObject_IsometricTransform_m59113D4A7DBD20C375B1B0D596519BDB9395CEBE(__this, L_21, L_23, L_24, NULL);
		(L_16)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_17), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_25);
		// for (int i = 0; i < m_Points.Length; i++)
		int32_t L_26 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_26, 1));
	}

IL_007c:
	{
		// for (int i = 0; i < m_Points.Length; i++)
		int32_t L_27 = V_1;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_28 = __this->___m_Points_9;
		if ((((int32_t)L_27) < ((int32_t)((int32_t)(((RuntimeArray*)L_28)->max_length)))))
		{
			goto IL_0053;
		}
	}
	{
		// if (m_CollisionShapeType == CollisionShapeType.Ellipse || m_CollisionShapeType == CollisionShapeType.Rectangle)
		int32_t L_29 = __this->___m_CollisionShapeType_11;
		if ((((int32_t)L_29) == ((int32_t)1)))
		{
			goto IL_0098;
		}
	}
	{
		int32_t L_30 = __this->___m_CollisionShapeType_11;
		if (L_30)
		{
			goto IL_009f;
		}
	}

IL_0098:
	{
		// m_CollisionShapeType = CollisionShapeType.Polygon;
		__this->___m_CollisionShapeType_11 = 2;
	}

IL_009f:
	{
		// ApplyRotationToPoints();
		CollisionObject_ApplyRotationToPoints_m512B1BE5CCABAA6514BC6E76C9A261E159B6B100(__this, NULL);
		// m_Points = m_Points.Select(p => p + m_Position).ToArray();
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_31 = __this->___m_Points_9;
		U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* L_32 = V_0;
		Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA* L_33 = (Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA*)il2cpp_codegen_object_new(Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA_il2cpp_TypeInfo_var);
		Func_2__ctor_mAB215938138B4DAC89DA24CC6B00066F2942477C(L_33, L_32, (intptr_t)((void*)U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__0_m027D21D5381DD2013D9011B4631C89AF06AC5898_RuntimeMethod_var), NULL);
		RuntimeObject* L_34;
		L_34 = Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C((RuntimeObject*)L_31, L_33, Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C_RuntimeMethod_var);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_35;
		L_35 = Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB(L_34, Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB_RuntimeMethod_var);
		__this->___m_Points_9 = L_35;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Points_9), (void*)L_35);
		// m_Points = m_Points.Select(p => LocalTransform(p, tile)).ToArray();
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_36 = __this->___m_Points_9;
		U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* L_37 = V_0;
		Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA* L_38 = (Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA*)il2cpp_codegen_object_new(Func_2_t33ED521BE3A7E943FA8D764514952EDF1AF1C0FA_il2cpp_TypeInfo_var);
		Func_2__ctor_mAB215938138B4DAC89DA24CC6B00066F2942477C(L_38, L_37, (intptr_t)((void*)U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__1_m6E76FA998AF975862FF19F35A85976C8E737C919_RuntimeMethod_var), NULL);
		RuntimeObject* L_39;
		L_39 = Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C((RuntimeObject*)L_36, L_38, Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D998D99BF39D2C5F31B71A68EC23B811C4F474C_RuntimeMethod_var);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_40;
		L_40 = Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB(L_39, Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB_RuntimeMethod_var);
		__this->___m_Points_9 = L_40;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Points_9), (void*)L_40);
		// m_Position = LocalTransform(m_Position, tile);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_41 = __this->___m_Position_3;
		U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* L_42 = V_0;
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_43 = L_42->___tile_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_44;
		L_44 = CollisionObject_LocalTransform_m4BBC2EED1964CAC87D946F6B20C84A395BA06018(__this, L_41, L_43, NULL);
		__this->___m_Position_3 = L_44;
		// }
		return;
	}
}
// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::IsometricTransform(UnityEngine.Vector2,SuperTiled2Unity.SuperTile,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 CollisionObject_IsometricTransform_m59113D4A7DBD20C375B1B0D596519BDB9395CEBE (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pt0, SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___gridSize2, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// float cx = pt.x / gridSize.y;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___pt0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___gridSize2;
		float L_3 = L_2.___y_1;
		// float cy = pt.y / gridSize.y;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___pt0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___gridSize2;
		float L_7 = L_6.___y_1;
		V_0 = ((float)(L_5/L_7));
		// float x = (cx - cy) * gridSize.x * 0.5f;
		float L_8 = ((float)(L_1/L_3));
		float L_9 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10 = ___gridSize2;
		float L_11 = L_10.___x_0;
		V_1 = ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_8, L_9)), L_11)), (0.5f)));
		// float y = (cx + cy) * gridSize.y * 0.5f;
		float L_12 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_13 = ___gridSize2;
		float L_14 = L_13.___y_1;
		V_2 = ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_add(L_8, L_12)), L_14)), (0.5f)));
		// y += (tile.m_Height - gridSize.y) * 0.5f;
		float L_15 = V_2;
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_16 = ___tile1;
		float L_17 = L_16->___m_Height_14;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_18 = ___gridSize2;
		float L_19 = L_18.___y_1;
		V_2 = ((float)il2cpp_codegen_add(L_15, ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_17, L_19)), (0.5f)))));
		// return new Vector2(x, y);
		float L_20 = V_1;
		float L_21 = V_2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_22;
		memset((&L_22), 0, sizeof(L_22));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_22), L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::LocalTransform(UnityEngine.Vector2,SuperTiled2Unity.SuperTile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 CollisionObject_LocalTransform_m4BBC2EED1964CAC87D946F6B20C84A395BA06018 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pt0, SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile1, const RuntimeMethod* method) 
{
	{
		// return new Vector2(pt.x, tile.m_Height - pt.y);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___pt0;
		float L_1 = L_0.___x_0;
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_2 = ___tile1;
		float L_3 = L_2->___m_Height_14;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___pt0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), L_1, ((float)il2cpp_codegen_subtract(L_3, L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void SuperTiled2Unity.CollisionObject::ApplyRotationToPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject_ApplyRotationToPoints_m512B1BE5CCABAA6514BC6E76C9A261E159B6B100 (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mA5D7A04386B3B335680CF888F4516918A1100459_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D6DCFEC603609B8CEFE5F7186E6CB0903999035_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CApplyRotationToPointsU3Eb__26_1_mD8BB4E0ABE4DCB6ADD80368637F6E1CB2F0B7F1D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass26_0_U3CApplyRotationToPointsU3Eb__0_m441B23E915236979FDC89E3044CEF47843C1F8CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB* V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* G_B3_0 = NULL;
	RuntimeObject* G_B3_1 = NULL;
	CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* G_B3_2 = NULL;
	Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* G_B2_0 = NULL;
	RuntimeObject* G_B2_1 = NULL;
	CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* G_B2_2 = NULL;
	{
		// if (m_Rotation != 0)
		float L_0 = __this->___m_Rotation_5;
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0082;
		}
	}
	{
		U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB* L_1 = (U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass26_0__ctor_mCBE0212AE0DBE64819687EB9DD21E60F17916C9C(L_1, NULL);
		V_0 = L_1;
		// var rads = m_Rotation * Mathf.Deg2Rad;
		float L_2 = __this->___m_Rotation_5;
		// var cos = Mathf.Cos(rads);
		float L_3 = ((float)il2cpp_codegen_multiply(L_2, (0.0174532924f)));
		float L_4;
		L_4 = cosf(L_3);
		V_1 = L_4;
		// var sin = Mathf.Sin(rads);
		float L_5;
		L_5 = sinf(L_3);
		V_2 = L_5;
		// var rotate = MatrixUtils.Rotate2d(cos, -sin, sin, cos);
		U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB* L_6 = V_0;
		float L_7 = V_1;
		float L_8 = V_2;
		float L_9 = V_2;
		float L_10 = V_1;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_11;
		L_11 = MatrixUtils_Rotate2d_m94977337A24271488FF3F281D11DF921A20079DA(L_7, ((-L_8)), L_9, L_10, NULL);
		L_6->___rotate_0 = L_11;
		// m_Points = m_Points.Select(p => rotate.MultiplyPoint(p)).Select(v3 => (Vector2)v3).ToArray();
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_12 = __this->___m_Points_9;
		U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB* L_13 = V_0;
		Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768* L_14 = (Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768*)il2cpp_codegen_object_new(Func_2_tECC97CAACCC7CB873184FEF49E8343711EEF4768_il2cpp_TypeInfo_var);
		Func_2__ctor_m5E7087CFE7EE090E6F0CA2843146A70D4CAAA49E(L_14, L_13, (intptr_t)((void*)U3CU3Ec__DisplayClass26_0_U3CApplyRotationToPointsU3Eb__0_m441B23E915236979FDC89E3044CEF47843C1F8CF_RuntimeMethod_var), NULL);
		RuntimeObject* L_15;
		L_15 = Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mA5D7A04386B3B335680CF888F4516918A1100459((RuntimeObject*)L_12, L_14, Enumerable_Select_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mA5D7A04386B3B335680CF888F4516918A1100459_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var);
		Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* L_16 = ((U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var))->___U3CU3E9__26_1_1;
		Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* L_17 = L_16;
		G_B2_0 = L_17;
		G_B2_1 = L_15;
		G_B2_2 = __this;
		if (L_17)
		{
			G_B3_0 = L_17;
			G_B3_1 = L_15;
			G_B3_2 = __this;
			goto IL_0073;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var);
		U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77* L_18 = ((U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* L_19 = (Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B*)il2cpp_codegen_object_new(Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B_il2cpp_TypeInfo_var);
		Func_2__ctor_mB1CEAB608E19C24C9324214823684B3D4195DD87(L_19, L_18, (intptr_t)((void*)U3CU3Ec_U3CApplyRotationToPointsU3Eb__26_1_mD8BB4E0ABE4DCB6ADD80368637F6E1CB2F0B7F1D_RuntimeMethod_var), NULL);
		Func_2_t8B61BDCC004CDB8C358F4770244C07AB77865F4B* L_20 = L_19;
		((U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var))->___U3CU3E9__26_1_1 = L_20;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var))->___U3CU3E9__26_1_1), (void*)L_20);
		G_B3_0 = L_20;
		G_B3_1 = G_B2_1;
		G_B3_2 = G_B2_2;
	}

IL_0073:
	{
		RuntimeObject* L_21;
		L_21 = Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D6DCFEC603609B8CEFE5F7186E6CB0903999035(G_B3_1, G_B3_0, Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m3D6DCFEC603609B8CEFE5F7186E6CB0903999035_RuntimeMethod_var);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_22;
		L_22 = Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB(L_21, Enumerable_ToArray_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_mA5462E77C61EC0112ECC377121A34752702358BB_RuntimeMethod_var);
		G_B3_2->___m_Points_9 = L_22;
		Il2CppCodeGenWriteBarrier((void**)(&G_B3_2->___m_Points_9), (void*)L_22);
	}

IL_0082:
	{
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.CollisionObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionObject__ctor_mBC390D4F0B3F4CC9DD91895D4BC50C040DB130EA (CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass23_0__ctor_m8DE5A4BC5950A73655C5C2AC805A0967676BEFC4 (U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::<RenderPoints>b__0(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__0_m027D21D5381DD2013D9011B4631C89AF06AC5898 (U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___p0, const RuntimeMethod* method) 
{
	{
		// m_Points = m_Points.Select(p => p + m_Position).ToArray();
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___p0;
		CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* L_1 = __this->___U3CU3E4__this_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = L_1->___m_Position_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		L_3 = Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline(L_0, L_2, NULL);
		return L_3;
	}
}
// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c__DisplayClass23_0::<RenderPoints>b__1(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 U3CU3Ec__DisplayClass23_0_U3CRenderPointsU3Eb__1_m6E76FA998AF975862FF19F35A85976C8E737C919 (U3CU3Ec__DisplayClass23_0_t94267C47E462573F9C7B17DBC9BF3C8B5424CD18* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___p0, const RuntimeMethod* method) 
{
	{
		// m_Points = m_Points.Select(p => LocalTransform(p, tile)).ToArray();
		CollisionObject_tF721FB61FF5117FF11D3D130F096B1B8C1F2245A* L_0 = __this->___U3CU3E4__this_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___p0;
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_2 = __this->___tile_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		L_3 = CollisionObject_LocalTransform_m4BBC2EED1964CAC87D946F6B20C84A395BA06018(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.CollisionObject/<>c__DisplayClass26_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass26_0__ctor_mCBE0212AE0DBE64819687EB9DD21E60F17916C9C (U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// UnityEngine.Vector3 SuperTiled2Unity.CollisionObject/<>c__DisplayClass26_0::<ApplyRotationToPoints>b__0(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 U3CU3Ec__DisplayClass26_0_U3CApplyRotationToPointsU3Eb__0_m441B23E915236979FDC89E3044CEF47843C1F8CF (U3CU3Ec__DisplayClass26_0_tF72548199E48F91346A527A4D2E56F9F9BB812CB* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___p0, const RuntimeMethod* method) 
{
	{
		// m_Points = m_Points.Select(p => rotate.MultiplyPoint(p)).Select(v3 => (Vector2)v3).ToArray();
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* L_0 = (&__this->___rotate_0);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___p0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_1, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E(L_0, L_2, NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.CollisionObject/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m999AE66A4C40BEFFD48AB73918965F60BE4B4929 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77* L_0 = (U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77*)il2cpp_codegen_object_new(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m6383F2FDA8B99E0D3A293C2E26B2B149404F8EA2(L_0, NULL);
		((U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var))->___U3CU3E9_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77_il2cpp_TypeInfo_var))->___U3CU3E9_0), (void*)L_0);
		return;
	}
}
// System.Void SuperTiled2Unity.CollisionObject/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6383F2FDA8B99E0D3A293C2E26B2B149404F8EA2 (U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c::<ApplyRotationToPoints>b__26_1(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 U3CU3Ec_U3CApplyRotationToPointsU3Eb__26_1_mD8BB4E0ABE4DCB6ADD80368637F6E1CB2F0B7F1D (U3CU3Ec_t0B8C8C0F3B805453C14269DCA4E2A16D2C871D77* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___v30, const RuntimeMethod* method) 
{
	{
		// m_Points = m_Points.Select(p => rotate.MultiplyPoint(p)).Select(v3 => (Vector2)v3).ToArray();
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___v30;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1;
		L_1 = Vector2_op_Implicit_m8F73B300CB4E6F9B4EB5FB6130363D76CEAA230B_inline(L_0, NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean SuperTiled2Unity.CustomProperty::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CustomProperty_get_IsEmpty_mFA36C70BB3CBC229F110FE5E4CAFFC4359767092 (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* __this, const RuntimeMethod* method) 
{
	{
		// get { return string.IsNullOrEmpty(m_Name); }
		String_t* L_0 = __this->___m_Name_0;
		bool L_1;
		L_1 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_0, NULL);
		return L_1;
	}
}
// System.Void SuperTiled2Unity.CustomProperty::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomProperty__ctor_m587076D390A2AA41418C4C1057782040046FE2E4 (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean SuperTiled2Unity.CustomPropertyListExtensions::TryGetProperty(System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>,System.String,SuperTiled2Unity.CustomProperty&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CustomPropertyListExtensions_TryGetProperty_m262897E51BA2D6BB72B492DA7FF7D4FEC479BB5B (List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* ___list0, String_t* ___propertyName1, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** ___property2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Find_mE71D47CAE16DFDCE85DAEA6732CCF78DCB0AEEE5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tE3859600DB095616566FA8539A8605019C819898_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass0_0_U3CTryGetPropertyU3Eb__0_mFFDDC9E830ACFEAA3E57FE8B240756505F6683BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA* V_0 = NULL;
	{
		U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA* L_0 = (U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass0_0__ctor_mBAB88BBA8C5224AE6CAC9C848DD1643518E5E3A1(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA* L_1 = V_0;
		String_t* L_2 = ___propertyName1;
		L_1->___propertyName_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___propertyName_0), (void*)L_2);
		// if (list != null)
		List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* L_3 = ___list0;
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		// property = list.Find(p => String.Equals(p.m_Name, propertyName, StringComparison.OrdinalIgnoreCase));
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** L_4 = ___property2;
		List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* L_5 = ___list0;
		U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA* L_6 = V_0;
		Predicate_1_tE3859600DB095616566FA8539A8605019C819898* L_7 = (Predicate_1_tE3859600DB095616566FA8539A8605019C819898*)il2cpp_codegen_object_new(Predicate_1_tE3859600DB095616566FA8539A8605019C819898_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m84717899DF8ECB94CB79C4D3E190FBA288570664(L_7, L_6, (intptr_t)((void*)U3CU3Ec__DisplayClass0_0_U3CTryGetPropertyU3Eb__0_mFFDDC9E830ACFEAA3E57FE8B240756505F6683BC_RuntimeMethod_var), NULL);
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_8;
		L_8 = List_1_Find_mE71D47CAE16DFDCE85DAEA6732CCF78DCB0AEEE5(L_5, L_7, List_1_Find_mE71D47CAE16DFDCE85DAEA6732CCF78DCB0AEEE5_RuntimeMethod_var);
		*((RuntimeObject**)L_4) = (RuntimeObject*)L_8;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_4, (void*)(RuntimeObject*)L_8);
		// return property != null;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** L_9 = ___property2;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_10 = *((CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B**)L_9);
		return (bool)((!(((RuntimeObject*)(CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B*)L_10) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
	}

IL_002a:
	{
		// property = null;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** L_11 = ___property2;
		*((RuntimeObject**)L_11) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_11, (void*)(RuntimeObject*)NULL);
		// return false;
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass0_0__ctor_mBAB88BBA8C5224AE6CAC9C848DD1643518E5E3A1 (U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Boolean SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::<TryGetProperty>b__0(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass0_0_U3CTryGetPropertyU3Eb__0_mFFDDC9E830ACFEAA3E57FE8B240756505F6683BC (U3CU3Ec__DisplayClass0_0_tD6AF20B20F22358D06E311DE3E983FC9C3D3E2DA* __this, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___p0, const RuntimeMethod* method) 
{
	{
		// property = list.Find(p => String.Equals(p.m_Name, propertyName, StringComparison.OrdinalIgnoreCase));
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_0 = ___p0;
		String_t* L_1 = L_0->___m_Name_0;
		String_t* L_2 = __this->___propertyName_0;
		bool L_3;
		L_3 = String_Equals_m80124ECC809968E69F952E2A49EBC03F81A23E43(L_1, L_2, 5, NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String SuperTiled2Unity.CustomPropertyExtensions::GetValueAsString(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CustomPropertyExtensions_GetValueAsString_m692FA0C6470995727534A812C2FD7B7D1264D577 (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) 
{
	{
		// return property.m_Value;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_0 = ___property0;
		String_t* L_1 = L_0->___m_Value_2;
		return L_1;
	}
}
// UnityEngine.Color SuperTiled2Unity.CustomPropertyExtensions::GetValueAsColor(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F CustomPropertyExtensions_GetValueAsColor_mECD5B66015F0470AD850B9886361BF0EBB795422 (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) 
{
	{
		// return property.m_Value.ToColor();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_0 = ___property0;
		String_t* L_1 = L_0->___m_Value_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2;
		L_2 = StringExtensions_ToColor_mCA880BC04D74E0EC9D926C72B478F58027A082D1(L_1, NULL);
		return L_2;
	}
}
// System.Int32 SuperTiled2Unity.CustomPropertyExtensions::GetValueAsInt(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CustomPropertyExtensions_GetValueAsInt_m08EBBA564426968A0AD4A00BDC2E01E9A3C98B7D (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) 
{
	{
		// return property.m_Value.ToInt();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_0 = ___property0;
		String_t* L_1 = L_0->___m_Value_2;
		int32_t L_2;
		L_2 = StringExtensions_ToInt_m1B2045627BF608C15A803476AA9DB54B149B1E76(L_1, NULL);
		return L_2;
	}
}
// System.Single SuperTiled2Unity.CustomPropertyExtensions::GetValueAsFloat(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CustomPropertyExtensions_GetValueAsFloat_m7A0545C8D8D20891BC596F1D31A40FBD2B10F27C (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) 
{
	{
		// return property.m_Value.ToFloat();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_0 = ___property0;
		String_t* L_1 = L_0->___m_Value_2;
		float L_2;
		L_2 = StringExtensions_ToFloat_m41589007A7AF2104695878FDB4D80E3AFAAFA496(L_1, NULL);
		return L_2;
	}
}
// System.Boolean SuperTiled2Unity.CustomPropertyExtensions::GetValueAsBool(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CustomPropertyExtensions_GetValueAsBool_mC15F3B153F093F3249890AAFC2DCB0317685F28D (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) 
{
	{
		// return property.m_Value.ToBool();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_0 = ___property0;
		String_t* L_1 = L_0->___m_Value_2;
		bool L_2;
		L_2 = StringExtensions_ToBool_mF36AD979ADB49B4DE575A0CC84CBFFC6CA0795F0(L_1, NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean SuperTiled2Unity.GameObjectExtensions::TryGetCustomPropertySafe(UnityEngine.GameObject,System.String,SuperTiled2Unity.CustomProperty&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObjectExtensions_TryGetCustomPropertySafe_m9869D960C47BA789F6A08A718030FA7DB4D35053 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___go0, String_t* ___name1, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** ___property2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE_m62AF091848A5ACFB37052C7AF9F74C9484B0EE32_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* V_0 = NULL;
	{
		// property = null;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** L_0 = ___property2;
		*((RuntimeObject**)L_0) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_0, (void*)(RuntimeObject*)NULL);
		// if (go == null)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = ___go0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_000e:
	{
		// var customProperties = go.GetComponent<SuperCustomProperties>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = ___go0;
		SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* L_4;
		L_4 = GameObject_GetComponent_TisSuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE_m62AF091848A5ACFB37052C7AF9F74C9484B0EE32(L_3, GameObject_GetComponent_TisSuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE_m62AF091848A5ACFB37052C7AF9F74C9484B0EE32_RuntimeMethod_var);
		V_0 = L_4;
		// if (customProperties == null)
		SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* L_5 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_5, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_6)
		{
			goto IL_0020;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0020:
	{
		// return customProperties.TryGetCustomProperty(name, out property);
		SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* L_7 = V_0;
		String_t* L_8 = ___name1;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** L_9 = ___property2;
		bool L_10;
		L_10 = SuperCustomProperties_TryGetCustomProperty_m621D9CBDF999333FB4836A496DA074ED1BFDD981(L_7, L_8, L_9, NULL);
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Color SuperTiled2Unity.StringExtensions::ToColor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F StringExtensions_ToColor_mCA880BC04D74E0EC9D926C72B478F58027A082D1 (String_t* ___htmlString0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0B0FEB3147CE20EB2C90076367F895C59BCD14B3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9DEC6BA217C79567CA5C709BA89D4C7622E5FEEC);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	uint8_t V_1 = 0x0;
	uint8_t V_2 = 0x0;
	uint8_t V_3 = 0x0;
	uint8_t V_4 = 0x0;
	uint8_t V_5 = 0x0;
	{
		// string html = htmlString;
		String_t* L_0 = ___htmlString0;
		V_0 = L_0;
		// if (html.StartsWith("#"))
		String_t* L_1 = V_0;
		bool L_2;
		L_2 = String_StartsWith_mF75DBA1EB709811E711B44E26FF919C88A8E65C0(L_1, _stringLiteral0B0FEB3147CE20EB2C90076367F895C59BCD14B3, NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// html = html.Remove(0, 1);
		String_t* L_3 = V_0;
		String_t* L_4;
		L_4 = String_Remove_m4D7A58E2124F8D0D8AE3EEDE74B6AD6A863ABA68(L_3, 0, 1, NULL);
		V_0 = L_4;
	}

IL_0018:
	{
		// if (html.Length == 8)
		String_t* L_5 = V_0;
		int32_t L_6;
		L_6 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_5, NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)8))))
		{
			goto IL_007a;
		}
	}
	{
		// byte a = byte.Parse(html.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
		String_t* L_7 = V_0;
		String_t* L_8;
		L_8 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_7, 0, 2, NULL);
		uint8_t L_9;
		L_9 = Byte_Parse_m1D565A1D1F7DF553B5F6CF87AC883B9BE0A6A2D3(L_8, ((int32_t)515), NULL);
		V_1 = L_9;
		// byte r = byte.Parse(html.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
		String_t* L_10 = V_0;
		String_t* L_11;
		L_11 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_10, 2, 2, NULL);
		uint8_t L_12;
		L_12 = Byte_Parse_m1D565A1D1F7DF553B5F6CF87AC883B9BE0A6A2D3(L_11, ((int32_t)515), NULL);
		// byte g = byte.Parse(html.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
		String_t* L_13 = V_0;
		String_t* L_14;
		L_14 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_13, 4, 2, NULL);
		uint8_t L_15;
		L_15 = Byte_Parse_m1D565A1D1F7DF553B5F6CF87AC883B9BE0A6A2D3(L_14, ((int32_t)515), NULL);
		V_2 = L_15;
		// byte b = byte.Parse(html.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
		String_t* L_16 = V_0;
		String_t* L_17;
		L_17 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_16, 6, 2, NULL);
		uint8_t L_18;
		L_18 = Byte_Parse_m1D565A1D1F7DF553B5F6CF87AC883B9BE0A6A2D3(L_17, ((int32_t)515), NULL);
		V_3 = L_18;
		// return new Color32(r, g, b, a);
		uint8_t L_19 = V_2;
		uint8_t L_20 = V_3;
		uint8_t L_21 = V_1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_22;
		memset((&L_22), 0, sizeof(L_22));
		Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline((&L_22), L_12, L_19, L_20, L_21, /*hidden argument*/NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_23;
		L_23 = Color32_op_Implicit_m203A634DBB77053C9400C68065CA29529103D172_inline(L_22, NULL);
		return L_23;
	}

IL_007a:
	{
		// else if (html.Length == 6)
		String_t* L_24 = V_0;
		int32_t L_25;
		L_25 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_24, NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)6))))
		{
			goto IL_00d1;
		}
	}
	{
		// byte r = byte.Parse(html.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
		String_t* L_26 = V_0;
		String_t* L_27;
		L_27 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_26, 0, 2, NULL);
		uint8_t L_28;
		L_28 = Byte_Parse_m1D565A1D1F7DF553B5F6CF87AC883B9BE0A6A2D3(L_27, ((int32_t)515), NULL);
		// byte g = byte.Parse(html.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
		String_t* L_29 = V_0;
		String_t* L_30;
		L_30 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_29, 2, 2, NULL);
		uint8_t L_31;
		L_31 = Byte_Parse_m1D565A1D1F7DF553B5F6CF87AC883B9BE0A6A2D3(L_30, ((int32_t)515), NULL);
		V_4 = L_31;
		// byte b = byte.Parse(html.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
		String_t* L_32 = V_0;
		String_t* L_33;
		L_33 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_32, 4, 2, NULL);
		uint8_t L_34;
		L_34 = Byte_Parse_m1D565A1D1F7DF553B5F6CF87AC883B9BE0A6A2D3(L_33, ((int32_t)515), NULL);
		V_5 = L_34;
		// return new Color32(r, g, b, 255);
		uint8_t L_35 = V_4;
		uint8_t L_36 = V_5;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_37;
		memset((&L_37), 0, sizeof(L_37));
		Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline((&L_37), L_28, L_35, L_36, (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_38;
		L_38 = Color32_op_Implicit_m203A634DBB77053C9400C68065CA29529103D172_inline(L_37, NULL);
		return L_38;
	}

IL_00d1:
	{
		// Debug.LogErrorFormat("Could not convert '{0}' to a color.", htmlString);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_39 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_40 = L_39;
		String_t* L_41 = ___htmlString0;
		ArrayElementTypeCheck (L_40, L_41);
		(L_40)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_41);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogErrorFormat_mA33C95EF832A60D72A7EE26074E13A86BE7E30C6(_stringLiteral9DEC6BA217C79567CA5C709BA89D4C7622E5FEEC, L_40, NULL);
		// return Color.magenta;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_42;
		L_42 = Color_get_magenta_mF552F660CB0E42F18558AD59D516EBAC923F57E2_inline(NULL);
		return L_42;
	}
}
// System.Single SuperTiled2Unity.StringExtensions::ToFloat(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float StringExtensions_ToFloat_m41589007A7AF2104695878FDB4D80E3AFAAFA496 (String_t* ___str0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14612D0AE4B7E7FCE6B63EA53EC07A4E37A2C80E);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if (!float.TryParse(str, NumberStyles.Float, CultureInfo.InvariantCulture, out result))
		String_t* L_0 = ___str0;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_1;
		L_1 = CultureInfo_get_InvariantCulture_m78DAB8CBE8766445310782B6E61FB7A9983AD425(NULL);
		bool L_2;
		L_2 = Single_TryParse_m5920BF3A60EE1FFF0CC117021B4BF2A8114D1AE5(L_0, ((int32_t)167), L_1, (&V_0), NULL);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		// Debug.LogErrorFormat("Could not convert '{0}' to float.", str);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = L_3;
		String_t* L_5 = ___str0;
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_5);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogErrorFormat_mA33C95EF832A60D72A7EE26074E13A86BE7E30C6(_stringLiteral14612D0AE4B7E7FCE6B63EA53EC07A4E37A2C80E, L_4, NULL);
	}

IL_0028:
	{
		// return result;
		float L_6 = V_0;
		return L_6;
	}
}
// System.Int32 SuperTiled2Unity.StringExtensions::ToInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StringExtensions_ToInt_m1B2045627BF608C15A803476AA9DB54B149B1E76 (String_t* ___str0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA88B7770DB03C11888305F2EC564F231840512FD);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (!int.TryParse(str, NumberStyles.Integer, CultureInfo.InvariantCulture, out result))
		String_t* L_0 = ___str0;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_1;
		L_1 = CultureInfo_get_InvariantCulture_m78DAB8CBE8766445310782B6E61FB7A9983AD425(NULL);
		bool L_2;
		L_2 = Int32_TryParse_m3CB3A8252B2254BF929D207AFA9F2CD4DA3E3F79(L_0, 7, L_1, (&V_0), NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		// Debug.LogErrorFormat("Could not convert '{0}' to int.", str);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = L_3;
		String_t* L_5 = ___str0;
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_5);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogErrorFormat_mA33C95EF832A60D72A7EE26074E13A86BE7E30C6(_stringLiteralA88B7770DB03C11888305F2EC564F231840512FD, L_4, NULL);
	}

IL_0024:
	{
		// return result;
		int32_t L_6 = V_0;
		return L_6;
	}
}
// System.Boolean SuperTiled2Unity.StringExtensions::ToBool(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StringExtensions_ToBool_mF36AD979ADB49B4DE575A0CC84CBFFC6CA0795F0 (String_t* ___str0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CAEFDEE17C0117C16963C7A094BBDC99E09B94D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77D38C0623F92B292B925F6E72CF5CF99A20D4EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB7C45DD316C68ABF3429C20058C2981C652192F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (str.Equals("1") || str.Equals("true", StringComparison.OrdinalIgnoreCase))
		String_t* L_0 = ___str0;
		bool L_1;
		L_1 = String_Equals_mCD5F35DEDCAFE51ACD4E033726FC2EF8DF7E9B4D(L_0, _stringLiteralE91FE173F59B063D620A934CE1A010F2B114C1F3, NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_2 = ___str0;
		bool L_3;
		L_3 = String_Equals_m7BDFC0B951005B9DC2BAED464AFE68FF7E9ACE5A(L_2, _stringLiteralB7C45DD316C68ABF3429C20058C2981C652192F2, 5, NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		// return true;
		return (bool)1;
	}

IL_001d:
	{
		// else if (str.Equals("0") || str.Equals("false", StringComparison.OrdinalIgnoreCase))
		String_t* L_4 = ___str0;
		bool L_5;
		L_5 = String_Equals_mCD5F35DEDCAFE51ACD4E033726FC2EF8DF7E9B4D(L_4, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, NULL);
		if (L_5)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_6 = ___str0;
		bool L_7;
		L_7 = String_Equals_m7BDFC0B951005B9DC2BAED464AFE68FF7E9ACE5A(L_6, _stringLiteral77D38C0623F92B292B925F6E72CF5CF99A20D4EB, 5, NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}

IL_0038:
	{
		// return false;
		return (bool)0;
	}

IL_003a:
	{
		// Debug.LogErrorFormat("Could not convert '{0}' to bool.", str);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_8 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_9 = L_8;
		String_t* L_10 = ___str0;
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_10);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogErrorFormat_mA33C95EF832A60D72A7EE26074E13A86BE7E30C6(_stringLiteral6CAEFDEE17C0117C16963C7A094BBDC99E09B94D, L_9, NULL);
		// return false;
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean SuperTiled2Unity.SuperTileExtensions::TryGetProperty(SuperTiled2Unity.SuperTile,System.String,SuperTiled2Unity.CustomProperty&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** ___property2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// property = null;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** L_0 = ___property2;
		*((RuntimeObject**)L_0) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_0, (void*)(RuntimeObject*)NULL);
		// if (tile == null)
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_1 = ___tile0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_000e:
	{
		// if (tile.m_CustomProperties == null)
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_3 = ___tile0;
		List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* L_4 = L_3->___m_CustomProperties_18;
		if (L_4)
		{
			goto IL_0018;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0018:
	{
		// if (tile.m_CustomProperties.TryGetProperty(propName, out property))
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_5 = ___tile0;
		List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* L_6 = L_5->___m_CustomProperties_18;
		String_t* L_7 = ___propName1;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** L_8 = ___property2;
		bool L_9;
		L_9 = CustomPropertyListExtensions_TryGetProperty_m262897E51BA2D6BB72B492DA7FF7D4FEC479BB5B(L_6, L_7, L_8, NULL);
		if (!L_9)
		{
			goto IL_0029;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0029:
	{
		// return false;
		return (bool)0;
	}
}
// System.String SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsString(SuperTiled2Unity.SuperTile,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SuperTileExtensions_GetPropertyValueAsString_m3558F05D09E99AEC4991E35CED7734A0CEED3496 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return GetPropertyValueAsString(tile, propName, string.Empty);
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		String_t* L_3;
		L_3 = SuperTileExtensions_GetPropertyValueAsString_m2D9D3FDA56279F63FB2FD59FEDF28429B416AF5E(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// System.String SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsString(SuperTiled2Unity.SuperTile,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SuperTileExtensions_GetPropertyValueAsString_m2D9D3FDA56279F63FB2FD59FEDF28429B416AF5E (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, String_t* ___defaultValue2, const RuntimeMethod* method) 
{
	CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* V_0 = NULL;
	{
		// if (TryGetProperty(tile, propName, out property))
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		bool L_2;
		L_2 = SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84(L_0, L_1, (&V_0), NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		// return property.GetValueAsString();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_3 = V_0;
		String_t* L_4;
		L_4 = CustomPropertyExtensions_GetValueAsString_m692FA0C6470995727534A812C2FD7B7D1264D577_inline(L_3, NULL);
		return L_4;
	}

IL_0012:
	{
		// return defaultValue;
		String_t* L_5 = ___defaultValue2;
		return L_5;
	}
}
// System.Boolean SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsBool(SuperTiled2Unity.SuperTile,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SuperTileExtensions_GetPropertyValueAsBool_m87FCCCFA1E3E7339949F88AE760A16DB62ED6F8F (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, const RuntimeMethod* method) 
{
	{
		// return GetPropertyValueAsBool(tile, propName, false);
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		bool L_2;
		L_2 = SuperTileExtensions_GetPropertyValueAsBool_m7F16749D7A003164C8E166412E87955874412F83(L_0, L_1, (bool)0, NULL);
		return L_2;
	}
}
// System.Boolean SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsBool(SuperTiled2Unity.SuperTile,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SuperTileExtensions_GetPropertyValueAsBool_m7F16749D7A003164C8E166412E87955874412F83 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, bool ___defaultValue2, const RuntimeMethod* method) 
{
	CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* V_0 = NULL;
	{
		// if (TryGetProperty(tile, propName, out property))
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		bool L_2;
		L_2 = SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84(L_0, L_1, (&V_0), NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		// return property.GetValueAsBool();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_3 = V_0;
		bool L_4;
		L_4 = CustomPropertyExtensions_GetValueAsBool_mC15F3B153F093F3249890AAFC2DCB0317685F28D(L_3, NULL);
		return L_4;
	}

IL_0012:
	{
		// return defaultValue;
		bool L_5 = ___defaultValue2;
		return L_5;
	}
}
// System.Int32 SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsInt(SuperTiled2Unity.SuperTile,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SuperTileExtensions_GetPropertyValueAsInt_mCBCE77AAC99985EEA4FDA1E9DCE02A229B0F3776 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, const RuntimeMethod* method) 
{
	{
		// return GetPropertyValueAsInt(tile, propName, 0);
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		int32_t L_2;
		L_2 = SuperTileExtensions_GetPropertyValueAsInt_m3970CC724020E9F46C4387FA8C911F7908EA7E3C(L_0, L_1, 0, NULL);
		return L_2;
	}
}
// System.Int32 SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsInt(SuperTiled2Unity.SuperTile,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SuperTileExtensions_GetPropertyValueAsInt_m3970CC724020E9F46C4387FA8C911F7908EA7E3C (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, int32_t ___defaultValue2, const RuntimeMethod* method) 
{
	CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* V_0 = NULL;
	{
		// if (TryGetProperty(tile, propName, out property))
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		bool L_2;
		L_2 = SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84(L_0, L_1, (&V_0), NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		// return property.GetValueAsInt();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_3 = V_0;
		int32_t L_4;
		L_4 = CustomPropertyExtensions_GetValueAsInt_m08EBBA564426968A0AD4A00BDC2E01E9A3C98B7D(L_3, NULL);
		return L_4;
	}

IL_0012:
	{
		// return defaultValue;
		int32_t L_5 = ___defaultValue2;
		return L_5;
	}
}
// System.Single SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsFloat(SuperTiled2Unity.SuperTile,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SuperTileExtensions_GetPropertyValueAsFloat_m7AF951188326A939D105FAFB6F639187F973FA18 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, const RuntimeMethod* method) 
{
	{
		// return GetPropertyValueAsFloat(tile, propName, 0);
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		float L_2;
		L_2 = SuperTileExtensions_GetPropertyValueAsFloat_m7B0D6B7A93DE789BC97C462A9B9168C60B9523DC(L_0, L_1, (0.0f), NULL);
		return L_2;
	}
}
// System.Single SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsFloat(SuperTiled2Unity.SuperTile,System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SuperTileExtensions_GetPropertyValueAsFloat_m7B0D6B7A93DE789BC97C462A9B9168C60B9523DC (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, float ___defaultValue2, const RuntimeMethod* method) 
{
	CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* V_0 = NULL;
	{
		// if (TryGetProperty(tile, propName, out property))
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		bool L_2;
		L_2 = SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84(L_0, L_1, (&V_0), NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		// return property.GetValueAsFloat();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_3 = V_0;
		float L_4;
		L_4 = CustomPropertyExtensions_GetValueAsFloat_m7A0545C8D8D20891BC596F1D31A40FBD2B10F27C(L_3, NULL);
		return L_4;
	}

IL_0012:
	{
		// return defaultValue;
		float L_5 = ___defaultValue2;
		return L_5;
	}
}
// UnityEngine.Color SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsColor(SuperTiled2Unity.SuperTile,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F SuperTileExtensions_GetPropertyValueAsColor_mEC386C1FF460A9E361608047B11407206BE7735A (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, const RuntimeMethod* method) 
{
	{
		// return GetPropertyValueAsColor(tile, propName, Color.clear);
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2;
		L_2 = Color_get_clear_m8B58EA88C92F7DD2C66F0EC1BCC8AC697D631298_inline(NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_3;
		L_3 = SuperTileExtensions_GetPropertyValueAsColor_m612E3C0B943694EF561332143F07AA0862DB7E70(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// UnityEngine.Color SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsColor(SuperTiled2Unity.SuperTile,System.String,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F SuperTileExtensions_GetPropertyValueAsColor_m612E3C0B943694EF561332143F07AA0862DB7E70 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* ___tile0, String_t* ___propName1, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___defaultValue2, const RuntimeMethod* method) 
{
	CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* V_0 = NULL;
	{
		// if (TryGetProperty(tile, propName, out property))
		SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* L_0 = ___tile0;
		String_t* L_1 = ___propName1;
		bool L_2;
		L_2 = SuperTileExtensions_TryGetProperty_m3F08EB3A3C53E14D28F779A5F637B041E0E00E84(L_0, L_1, (&V_0), NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		// return property.GetValueAsColor();
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_3 = V_0;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4;
		L_4 = CustomPropertyExtensions_GetValueAsColor_mECD5B66015F0470AD850B9886361BF0EBB795422(L_3, NULL);
		return L_4;
	}

IL_0012:
	{
		// return defaultValue;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_5 = ___defaultValue2;
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean SuperTiled2Unity.FlipFlagsMask::FlippedHorizontally(SuperTiled2Unity.FlipFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlipFlagsMask_FlippedHorizontally_mEEFD6BFA6D3842CB5425125A29735904B90F03A1 (int32_t ___flags0, const RuntimeMethod* method) 
{
	{
		// return (flags & FlipFlags.Horizontal) != 0;
		int32_t L_0 = ___flags0;
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&4))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean SuperTiled2Unity.FlipFlagsMask::FlippedVertically(SuperTiled2Unity.FlipFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlipFlagsMask_FlippedVertically_mB596859EF886CE6219BC0B4E5697260B598269C3 (int32_t ___flags0, const RuntimeMethod* method) 
{
	{
		// return (flags & FlipFlags.Vertical) != 0;
		int32_t L_0 = ___flags0;
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&2))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean SuperTiled2Unity.FlipFlagsMask::RotatedDiagonally(SuperTiled2Unity.FlipFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlipFlagsMask_RotatedDiagonally_mECF1DF8F2B9DC31F7F2060FA362BDC8D7C47D2F5 (int32_t ___flags0, const RuntimeMethod* method) 
{
	{
		// return (flags & FlipFlags.Diagonal) != 0;
		int32_t L_0 = ___flags0;
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&1))) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Boolean SuperTiled2Unity.FlipFlagsMask::RotatedHexagonally120(SuperTiled2Unity.FlipFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlipFlagsMask_RotatedHexagonally120_m47701C0290627A54236243EF6079E27F53FF6B89 (int32_t ___flags0, const RuntimeMethod* method) 
{
	{
		// return (flags & FlipFlags.Hexagonal120) != 0;
		int32_t L_0 = ___flags0;
		return (bool)((!(((uint32_t)((int32_t)((int32_t)L_0&8))) <= ((uint32_t)0)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Tilemaps.TilemapRenderer/SortOrder SuperTiled2Unity.MapRenderConverter::Tiled2Unity(SuperTiled2Unity.MapRenderOrder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MapRenderConverter_Tiled2Unity_mA0B4704642C31A8710C7D3FD9D03A53AB78F89BE (int32_t ___order0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___order0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_001e;
			}
			case 2:
			{
				goto IL_0018;
			}
			case 3:
			{
				goto IL_001a;
			}
		}
	}
	{
		goto IL_0020;
	}

IL_0018:
	{
		// return TilemapRenderer.SortOrder.TopRight;
		return (int32_t)(3);
	}

IL_001a:
	{
		// return TilemapRenderer.SortOrder.BottomRight;
		return (int32_t)(1);
	}

IL_001c:
	{
		// return TilemapRenderer.SortOrder.TopLeft;
		return (int32_t)(2);
	}

IL_001e:
	{
		// return TilemapRenderer.SortOrder.BottomLeft;
		return (int32_t)(0);
	}

IL_0020:
	{
		// return TilemapRenderer.SortOrder.TopLeft;
		return (int32_t)(2);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Matrix4x4 SuperTiled2Unity.MatrixUtils::Rotate2d(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 MatrixUtils_Rotate2d_m94977337A24271488FF3F281D11DF921A20079DA (float ___m000, float ___m011, float ___m102, float ___m113, const RuntimeMethod* method) 
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var mat = Matrix4x4.identity;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0;
		L_0 = Matrix4x4_get_identity_m94A09872C449C26863FF10D0FDF87842D91BECD6_inline(NULL);
		V_0 = L_0;
		// mat.m00 = m00;
		float L_1 = ___m000;
		(&V_0)->___m00_0 = L_1;
		// mat.m01 = m01;
		float L_2 = ___m011;
		(&V_0)->___m01_4 = L_2;
		// mat.m10 = m10;
		float L_3 = ___m102;
		(&V_0)->___m10_1 = L_3;
		// mat.m11 = m11;
		float L_4 = ___m113;
		(&V_0)->___m11_5 = L_4;
		// return mat;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_5 = V_0;
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 SuperTiled2Unity.ObjectAlignmentToPivot::ToVector3(System.Single,System.Single,System.Single,SuperTiled2Unity.MapOrientation,SuperTiled2Unity.ObjectAlignment)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ObjectAlignmentToPivot_ToVector3_m03C9825A915AFCD26166818258EE018E51A33977 (float ___width0, float ___height1, float ___inversePPU2, int32_t ___mapOrientation3, int32_t ___objectAlignment4, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t G_B4_0 = 0;
	{
		// width *= inversePPU;
		float L_0 = ___width0;
		float L_1 = ___inversePPU2;
		___width0 = ((float)il2cpp_codegen_multiply(L_0, L_1));
		// height *= inversePPU;
		float L_2 = ___height1;
		float L_3 = ___inversePPU2;
		___height1 = ((float)il2cpp_codegen_multiply(L_2, L_3));
		// float dx = 0;
		V_0 = (0.0f);
		// float dy = 0;
		V_1 = (0.0f);
		// if (objectAlignment == ObjectAlignment.Unspecified)
		int32_t L_4 = ___objectAlignment4;
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		// objectAlignment = mapOrientation == MapOrientation.Isometric ? ObjectAlignment.Bottom : ObjectAlignment.BottomLeft;
		int32_t L_5 = ___mapOrientation3;
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 7;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 8;
	}

IL_0022:
	{
		___objectAlignment4 = G_B4_0;
	}

IL_0024:
	{
		int32_t L_6 = ___objectAlignment4;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, 1)))
		{
			case 0:
			{
				goto IL_0077;
			}
			case 1:
			{
				goto IL_0061;
			}
			case 2:
			{
				goto IL_006f;
			}
			case 3:
			{
				goto IL_0082;
			}
			case 4:
			{
				goto IL_0093;
			}
			case 5:
			{
				goto IL_00a7;
			}
			case 6:
			{
				goto IL_0053;
			}
			case 7:
			{
				goto IL_00b5;
			}
			case 8:
			{
				goto IL_00c6;
			}
		}
	}
	{
		goto IL_00cf;
	}

IL_0053:
	{
		// dx = 0;
		V_0 = (0.0f);
		// dy = 0;
		V_1 = (0.0f);
		// break;
		goto IL_00cf;
	}

IL_0061:
	{
		// dx = -width * 0.5f;
		float L_7 = ___width0;
		V_0 = ((float)il2cpp_codegen_multiply(((-L_7)), (0.5f)));
		// dy = -height;
		float L_8 = ___height1;
		V_1 = ((-L_8));
		// break;
		goto IL_00cf;
	}

IL_006f:
	{
		// dx = -width;
		float L_9 = ___width0;
		V_0 = ((-L_9));
		// dy = -height;
		float L_10 = ___height1;
		V_1 = ((-L_10));
		// break;
		goto IL_00cf;
	}

IL_0077:
	{
		// dx = 0;
		V_0 = (0.0f);
		// dy = -height;
		float L_11 = ___height1;
		V_1 = ((-L_11));
		// break;
		goto IL_00cf;
	}

IL_0082:
	{
		// dx = 0;
		V_0 = (0.0f);
		// dy = -height * 0.5f;
		float L_12 = ___height1;
		V_1 = ((float)il2cpp_codegen_multiply(((-L_12)), (0.5f)));
		// break;
		goto IL_00cf;
	}

IL_0093:
	{
		// dx = -width * 0.5f;
		float L_13 = ___width0;
		V_0 = ((float)il2cpp_codegen_multiply(((-L_13)), (0.5f)));
		// dy = -height * 0.5f;
		float L_14 = ___height1;
		V_1 = ((float)il2cpp_codegen_multiply(((-L_14)), (0.5f)));
		// break;
		goto IL_00cf;
	}

IL_00a7:
	{
		// dx = -width;
		float L_15 = ___width0;
		V_0 = ((-L_15));
		// dy = -height * 0.5f;
		float L_16 = ___height1;
		V_1 = ((float)il2cpp_codegen_multiply(((-L_16)), (0.5f)));
		// break;
		goto IL_00cf;
	}

IL_00b5:
	{
		// dx = -width * 0.5f;
		float L_17 = ___width0;
		V_0 = ((float)il2cpp_codegen_multiply(((-L_17)), (0.5f)));
		// dy = 0;
		V_1 = (0.0f);
		// break;
		goto IL_00cf;
	}

IL_00c6:
	{
		// dx = -width;
		float L_18 = ___width0;
		V_0 = ((-L_18));
		// dy = 0;
		V_1 = (0.0f);
	}

IL_00cf:
	{
		// return new Vector3(dx, dy, 0);
		float L_19 = V_0;
		float L_20 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
		memset((&L_21), 0, sizeof(L_21));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_21), L_19, L_20, (0.0f), /*hidden argument*/NULL);
		return L_21;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.ReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyAttribute__ctor_m0BAA811D5FE2D4958319538FE4EAD5FBEBFF6BFD (ReadOnlyAttribute_tC7D6593B643C74C635E5BEE10D4BF3383D6701FE* __this, const RuntimeMethod* method) 
{
	{
		PropertyAttribute__ctor_m19247686E165101F140615C7306DC2DA3953D97D(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperColliderComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperColliderComponent__ctor_m4EB807688DC0B776A283C84A53C1DD85F4B28557 (SuperColliderComponent_tC658CE43E321DE9458EF2777905A81C57426E419* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m65D107AE0F3BB61F0C374671D35DBB2181FF6F95_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Shape> m_PolygonShapes = new List<Shape>();
		List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17* L_0 = (List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17*)il2cpp_codegen_object_new(List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17_il2cpp_TypeInfo_var);
		List_1__ctor_m65D107AE0F3BB61F0C374671D35DBB2181FF6F95(L_0, List_1__ctor_m65D107AE0F3BB61F0C374671D35DBB2181FF6F95_RuntimeMethod_var);
		__this->___m_PolygonShapes_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_PolygonShapes_4), (void*)L_0);
		// public List<Shape> m_OutlineShapes = new List<Shape>();
		List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17* L_1 = (List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17*)il2cpp_codegen_object_new(List_1_t2BCF792CA05B4D1DFEDFBF58F913F80AAB5B3B17_il2cpp_TypeInfo_var);
		List_1__ctor_m65D107AE0F3BB61F0C374671D35DBB2181FF6F95(L_1, List_1__ctor_m65D107AE0F3BB61F0C374671D35DBB2181FF6F95_RuntimeMethod_var);
		__this->___m_OutlineShapes_5 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_OutlineShapes_5), (void*)L_1);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperColliderComponent/Shape::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shape__ctor_m4D0B7FB88E2324F5643E54E0B0EBC9240AA15D32 (Shape_tBA148987BC8B013C676DCCCFD4F112E273FBAF71* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean SuperTiled2Unity.SuperCustomProperties::TryGetCustomProperty(System.String,SuperTiled2Unity.CustomProperty&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SuperCustomProperties_TryGetCustomProperty_m621D9CBDF999333FB4836A496DA074ED1BFDD981 (SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* __this, String_t* ___name0, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** ___property1, const RuntimeMethod* method) 
{
	{
		// return m_Properties.TryGetProperty(name, out property);
		List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* L_0 = __this->___m_Properties_4;
		String_t* L_1 = ___name0;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B** L_2 = ___property1;
		bool L_3;
		L_3 = CustomPropertyListExtensions_TryGetProperty_m262897E51BA2D6BB72B492DA7FF7D4FEC479BB5B(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// System.Void SuperTiled2Unity.SuperCustomProperties::RemoveCustomProperty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperCustomProperties_RemoveCustomProperty_m0FF0B5F1C8F57C7F4ED96D1EFFBB9008FA643F5A (SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* __this, String_t* ___name0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAll_mFE2C6425D80C8A9E05C9B8ADB56C91773C9EC1C5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tE3859600DB095616566FA8539A8605019C819898_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass2_0_U3CRemoveCustomPropertyU3Eg__PredicateRemoveCustomPropertyU7C0_m25DC0B231A7E7E6E137D739178A6E283DD075AC9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452* V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452* L_0 = (U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass2_0__ctor_mABC57E0F328DE14C90D613679DBBFFB4FB78A8D8(L_0, NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452* L_1 = V_0;
		String_t* L_2 = ___name0;
		L_1->___name_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___name_0), (void*)L_2);
		// m_Properties.RemoveAll(PredicateRemoveCustomProperty);
		List_1_t1EEDF84F33806497CE2DD7537ECB4B4DB767E36D* L_3 = __this->___m_Properties_4;
		U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452* L_4 = V_0;
		Predicate_1_tE3859600DB095616566FA8539A8605019C819898* L_5 = (Predicate_1_tE3859600DB095616566FA8539A8605019C819898*)il2cpp_codegen_object_new(Predicate_1_tE3859600DB095616566FA8539A8605019C819898_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m84717899DF8ECB94CB79C4D3E190FBA288570664(L_5, L_4, (intptr_t)((void*)U3CU3Ec__DisplayClass2_0_U3CRemoveCustomPropertyU3Eg__PredicateRemoveCustomPropertyU7C0_m25DC0B231A7E7E6E137D739178A6E283DD075AC9_RuntimeMethod_var), NULL);
		int32_t L_6;
		L_6 = List_1_RemoveAll_mFE2C6425D80C8A9E05C9B8ADB56C91773C9EC1C5(L_3, L_5, List_1_RemoveAll_mFE2C6425D80C8A9E05C9B8ADB56C91773C9EC1C5_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.SuperCustomProperties::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperCustomProperties__ctor_m09B5D823FCD1C41B2808B92CDF27659D9A8E3DD2 (SuperCustomProperties_t87305FB1A8642AF78F3BDB21477E8E872CDE7DBE* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperCustomProperties/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mABC57E0F328DE14C90D613679DBBFFB4FB78A8D8 (U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Boolean SuperTiled2Unity.SuperCustomProperties/<>c__DisplayClass2_0::<RemoveCustomProperty>g__PredicateRemoveCustomProperty|0(SuperTiled2Unity.CustomProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass2_0_U3CRemoveCustomPropertyU3Eg__PredicateRemoveCustomPropertyU7C0_m25DC0B231A7E7E6E137D739178A6E283DD075AC9 (U3CU3Ec__DisplayClass2_0_t6794D8DAE33E33A7BD122B88955FD5248912F452* __this, CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___customProperty0, const RuntimeMethod* method) 
{
	{
		// return name.Equals(customProperty.m_Name);
		String_t* L_0 = __this->___name_0;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_1 = ___customProperty0;
		String_t* L_2 = L_1->___m_Name_0;
		bool L_3;
		L_3 = String_Equals_mCD5F35DEDCAFE51ACD4E033726FC2EF8DF7E9B4D(L_0, L_2, NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperGroupLayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperGroupLayer__ctor_m517B0645DCA484925C2AFA5D0DE8563FC4FFF4B0 (SuperGroupLayer_tC73291F06FA4A186FAB5BCBD8FE92853B5DE2AB1* __this, const RuntimeMethod* method) 
{
	{
		SuperLayer__ctor_m27C9812BCE3C7EF1A912DAB7F323C8C56BFE82E9(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperImageLayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperImageLayer__ctor_m2548E0F500D974FCBF85B1114F29739D8FD033FD (SuperImageLayer_t7023C8E04FA266112B5F0F2321FB2051A4791EDB* __this, const RuntimeMethod* method) 
{
	{
		SuperLayer__ctor_m27C9812BCE3C7EF1A912DAB7F323C8C56BFE82E9(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Color SuperTiled2Unity.SuperLayer::CalculateColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F SuperLayer_CalculateColor_m0CDBE161CBA50DE3F521F2211DF23117CB1658B0 (SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* V_1 = NULL;
	int32_t V_2 = 0;
	SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* V_3 = NULL;
	{
		// Color color = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		V_0 = L_0;
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_2;
		L_2 = GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140(L_1, GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = 0;
		goto IL_003c;
	}

IL_0016:
	{
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_3 = V_1;
		int32_t L_4 = V_2;
		int32_t L_5 = L_4;
		SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* L_6 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		V_3 = L_6;
		// color *= layer.m_TintColor;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_7 = V_0;
		SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* L_8 = V_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_9 = L_8->___m_TintColor_10;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_10;
		L_10 = Color_op_Multiply_mF17D278EB0ABC9AEB32E829D5CA98784E0D6B66F_inline(L_7, L_9, NULL);
		V_0 = L_10;
		// color.a *= layer.m_Opacity;
		float* L_11 = (&(&V_0)->___a_3);
		float* L_12 = L_11;
		float L_13 = *((float*)L_12);
		SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* L_14 = V_3;
		float L_15 = L_14->___m_Opacity_9;
		*((float*)L_12) = (float)((float)il2cpp_codegen_multiply(L_13, L_15));
		int32_t L_16 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_16, 1));
	}

IL_003c:
	{
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		int32_t L_17 = V_2;
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_18 = V_1;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length)))))
		{
			goto IL_0016;
		}
	}
	{
		// return color;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_19 = V_0;
		return L_19;
	}
}
// System.Single SuperTiled2Unity.SuperLayer::CalculateOpacity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SuperLayer_CalculateOpacity_m4343ED780CD03CCE9AB0E1AEDB474BC003419846 (SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* V_1 = NULL;
	int32_t V_2 = 0;
	SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* V_3 = NULL;
	{
		// float opacity = 1.0f;
		V_0 = (1.0f);
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_1;
		L_1 = GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140(L_0, GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140_RuntimeMethod_var);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0027;
	}

IL_0016:
	{
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_2 = V_1;
		int32_t L_3 = V_2;
		int32_t L_4 = L_3;
		SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		V_3 = L_5;
		// opacity *= layer.m_Opacity;
		float L_6 = V_0;
		SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* L_7 = V_3;
		float L_8 = L_7->___m_Opacity_9;
		V_0 = ((float)il2cpp_codegen_multiply(L_6, L_8));
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_0027:
	{
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		int32_t L_10 = V_2;
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))
		{
			goto IL_0016;
		}
	}
	{
		// return opacity;
		float L_12 = V_0;
		return L_12;
	}
}
// System.Void SuperTiled2Unity.SuperLayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperLayer__ctor_m27C9812BCE3C7EF1A912DAB7F323C8C56BFE82E9 (SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperMap::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperMap_Start_mCDD9A6B0489E2FBCD3CA6C0722D32D59E7EA416F (SuperMap_tBBCDA02EE1A6B1D4E3B0B7D85C4D6A129280177D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentsInChildren_TisTilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB_m3CFA932712E5F15C704FAA4A95AB809344841ED8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// foreach (var renderer in GetComponentsInChildren<TilemapRenderer>())
		TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53* L_0;
		L_0 = Component_GetComponentsInChildren_TisTilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB_m3CFA932712E5F15C704FAA4A95AB809344841ED8(__this, Component_GetComponentsInChildren_TisTilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB_m3CFA932712E5F15C704FAA4A95AB809344841ED8_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0018;
	}

IL_000b:
	{
		// foreach (var renderer in GetComponentsInChildren<TilemapRenderer>())
		TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		TilemapRenderer_t1A45FD335E86172CFBB77D657E1D6705A477A6CB* L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		// renderer.detectChunkCullingBounds = TilemapRenderer.DetectChunkCullingBounds.Auto;
		TilemapRenderer_set_detectChunkCullingBounds_m7EECA4EC0AE104C34E7B8BC240E6CBA6B8234153(L_4, 0, NULL);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0018:
	{
		// foreach (var renderer in GetComponentsInChildren<TilemapRenderer>())
		int32_t L_6 = V_1;
		TilemapRendererU5BU5D_t406CA9D5B669678F1489A738B558C35E35B55B53* L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// UnityEngine.Vector3Int SuperTiled2Unity.SuperMap::TiledIndexToGridCell(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 SuperMap_TiledIndexToGridCell_mCAD36FC70FE48D50197A57C7DAF555FB09A4D2EB (SuperMap_tBBCDA02EE1A6B1D4E3B0B7D85C4D6A129280177D* __this, int32_t ___index0, int32_t ___offset_x1, int32_t ___offset_y2, int32_t ___stride3, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// int x = index % stride;
		int32_t L_0 = ___index0;
		int32_t L_1 = ___stride3;
		V_0 = ((int32_t)(L_0%L_1));
		// int y = index / stride;
		int32_t L_2 = ___index0;
		int32_t L_3 = ___stride3;
		V_1 = ((int32_t)(L_2/L_3));
		// x += offset_x;
		int32_t L_4 = V_0;
		int32_t L_5 = ___offset_x1;
		V_0 = ((int32_t)il2cpp_codegen_add(L_4, L_5));
		// y += offset_y;
		int32_t L_6 = V_1;
		int32_t L_7 = ___offset_y2;
		V_1 = ((int32_t)il2cpp_codegen_add(L_6, L_7));
		// var pos3 = TiledCellToGridCell(x, y);
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_10;
		L_10 = SuperMap_TiledCellToGridCell_m5FEBC722CCC4F177AD34568AC83330C76C094681(__this, L_8, L_9, NULL);
		V_2 = L_10;
		// pos3.y = -pos3.y;
		int32_t L_11;
		L_11 = Vector3Int_get_y_m42F43000F85D356557CAF03442273E7AA08F7F72_inline((&V_2), NULL);
		Vector3Int_set_y_mA856F32D1BF187BD4091DDF3C6872FD01F7D3377_inline((&V_2), ((-L_11)), NULL);
		// return pos3;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_12 = V_2;
		return L_12;
	}
}
// UnityEngine.Vector3Int SuperTiled2Unity.SuperMap::TiledCellToGridCell(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 SuperMap_TiledCellToGridCell_m5FEBC722CCC4F177AD34568AC83330C76C094681 (SuperMap_tBBCDA02EE1A6B1D4E3B0B7D85C4D6A129280177D* __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method) 
{
	bool V_0 = false;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	bool V_5 = false;
	{
		// if (m_Orientation == MapOrientation.Isometric)
		int32_t L_0 = __this->___m_Orientation_6;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0013;
		}
	}
	{
		// return new Vector3Int(-y, x, 0);
		int32_t L_1 = ___y1;
		int32_t L_2 = ___x0;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline((&L_3), ((-L_1)), L_2, 0, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		// else if (m_Orientation == MapOrientation.Staggered)
		int32_t L_4 = __this->___m_Orientation_6;
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}
	{
		// var isStaggerX = m_StaggerAxis == StaggerAxis.X;
		int32_t L_5 = __this->___m_StaggerAxis_13;
		// var isStaggerOdd = m_StaggerIndex == StaggerIndex.Odd;
		int32_t L_6 = __this->___m_StaggerIndex_14;
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
		// if (isStaggerX)
		if (!((((int32_t)L_5) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0091;
		}
	}
	{
		// var pos = new Vector3Int(x - y, x + y, 0);
		int32_t L_7 = ___x0;
		int32_t L_8 = ___y1;
		int32_t L_9 = ___x0;
		int32_t L_10 = ___y1;
		Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline((&V_1), ((int32_t)il2cpp_codegen_subtract(L_7, L_8)), ((int32_t)il2cpp_codegen_add(L_9, L_10)), 0, NULL);
		// if (isStaggerOdd)
		bool L_11 = V_0;
		if (!L_11)
		{
			goto IL_006b;
		}
	}
	{
		// pos.x -= (x + 1) / 2;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* L_12 = (&V_1);
		int32_t L_13;
		L_13 = Vector3Int_get_x_m21C268D2AA4C03CE35AA49DF6155347C9748054C_inline(L_12, NULL);
		int32_t L_14 = ___x0;
		Vector3Int_set_x_m8745C5976D035EBBAC6F6191B5838D58631D8685_inline(L_12, ((int32_t)il2cpp_codegen_subtract(L_13, ((int32_t)(((int32_t)il2cpp_codegen_add(L_14, 1))/2)))), NULL);
		// pos.y -= x / 2;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* L_15 = (&V_1);
		int32_t L_16;
		L_16 = Vector3Int_get_y_m42F43000F85D356557CAF03442273E7AA08F7F72_inline(L_15, NULL);
		int32_t L_17 = ___x0;
		Vector3Int_set_y_mA856F32D1BF187BD4091DDF3C6872FD01F7D3377_inline(L_15, ((int32_t)il2cpp_codegen_subtract(L_16, ((int32_t)(L_17/2)))), NULL);
		goto IL_008f;
	}

IL_006b:
	{
		// pos.x -= x / 2;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* L_18 = (&V_1);
		int32_t L_19;
		L_19 = Vector3Int_get_x_m21C268D2AA4C03CE35AA49DF6155347C9748054C_inline(L_18, NULL);
		int32_t L_20 = ___x0;
		Vector3Int_set_x_m8745C5976D035EBBAC6F6191B5838D58631D8685_inline(L_18, ((int32_t)il2cpp_codegen_subtract(L_19, ((int32_t)(L_20/2)))), NULL);
		// pos.y -= (x + 1) / 2;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* L_21 = (&V_1);
		int32_t L_22;
		L_22 = Vector3Int_get_y_m42F43000F85D356557CAF03442273E7AA08F7F72_inline(L_21, NULL);
		int32_t L_23 = ___x0;
		Vector3Int_set_y_mA856F32D1BF187BD4091DDF3C6872FD01F7D3377_inline(L_21, ((int32_t)il2cpp_codegen_subtract(L_22, ((int32_t)(((int32_t)il2cpp_codegen_add(L_23, 1))/2)))), NULL);
	}

IL_008f:
	{
		// return pos;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_24 = V_1;
		return L_24;
	}

IL_0091:
	{
		// var pos = new Vector3Int(x, y + x, 0);
		int32_t L_25 = ___x0;
		int32_t L_26 = ___y1;
		int32_t L_27 = ___x0;
		Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline((&V_2), L_25, ((int32_t)il2cpp_codegen_add(L_26, L_27)), 0, NULL);
		// if (isStaggerOdd)
		bool L_28 = V_0;
		if (!L_28)
		{
			goto IL_00c4;
		}
	}
	{
		// var stagger = y / 2;
		int32_t L_29 = ___y1;
		V_3 = ((int32_t)(L_29/2));
		// pos.x -= stagger;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* L_30 = (&V_2);
		int32_t L_31;
		L_31 = Vector3Int_get_x_m21C268D2AA4C03CE35AA49DF6155347C9748054C_inline(L_30, NULL);
		int32_t L_32 = V_3;
		Vector3Int_set_x_m8745C5976D035EBBAC6F6191B5838D58631D8685_inline(L_30, ((int32_t)il2cpp_codegen_subtract(L_31, L_32)), NULL);
		// pos.y -= stagger;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* L_33 = (&V_2);
		int32_t L_34;
		L_34 = Vector3Int_get_y_m42F43000F85D356557CAF03442273E7AA08F7F72_inline(L_33, NULL);
		int32_t L_35 = V_3;
		Vector3Int_set_y_mA856F32D1BF187BD4091DDF3C6872FD01F7D3377_inline(L_33, ((int32_t)il2cpp_codegen_subtract(L_34, L_35)), NULL);
		goto IL_00eb;
	}

IL_00c4:
	{
		// var stagger = (y + 1) / 2;
		int32_t L_36 = ___y1;
		V_4 = ((int32_t)(((int32_t)il2cpp_codegen_add(L_36, 1))/2));
		// pos.x -= stagger;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* L_37 = (&V_2);
		int32_t L_38;
		L_38 = Vector3Int_get_x_m21C268D2AA4C03CE35AA49DF6155347C9748054C_inline(L_37, NULL);
		int32_t L_39 = V_4;
		Vector3Int_set_x_m8745C5976D035EBBAC6F6191B5838D58631D8685_inline(L_37, ((int32_t)il2cpp_codegen_subtract(L_38, L_39)), NULL);
		// pos.y -= stagger;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* L_40 = (&V_2);
		int32_t L_41;
		L_41 = Vector3Int_get_y_m42F43000F85D356557CAF03442273E7AA08F7F72_inline(L_40, NULL);
		int32_t L_42 = V_4;
		Vector3Int_set_y_mA856F32D1BF187BD4091DDF3C6872FD01F7D3377_inline(L_40, ((int32_t)il2cpp_codegen_subtract(L_41, L_42)), NULL);
	}

IL_00eb:
	{
		// return pos;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_43 = V_2;
		return L_43;
	}

IL_00ed:
	{
		// else if (m_Orientation == MapOrientation.Hexagonal)
		int32_t L_44 = __this->___m_Orientation_6;
		if ((!(((uint32_t)L_44) == ((uint32_t)3))))
		{
			goto IL_0140;
		}
	}
	{
		// var isStaggerX = m_StaggerAxis == StaggerAxis.X;
		int32_t L_45 = __this->___m_StaggerAxis_13;
		// var isStaggerOdd = m_StaggerIndex == StaggerIndex.Odd;
		int32_t L_46 = __this->___m_StaggerIndex_14;
		V_5 = (bool)((((int32_t)L_46) == ((int32_t)1))? 1 : 0);
		// if (isStaggerX)
		if (!((((int32_t)L_45) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0128;
		}
	}
	{
		// if (isStaggerOdd)
		bool L_47 = V_5;
		if (!L_47)
		{
			goto IL_011d;
		}
	}
	{
		// return new Vector3Int(-y, -x - 1, 0);
		int32_t L_48 = ___y1;
		int32_t L_49 = ___x0;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_50;
		memset((&L_50), 0, sizeof(L_50));
		Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline((&L_50), ((-L_48)), ((int32_t)il2cpp_codegen_subtract(((-L_49)), 1)), 0, /*hidden argument*/NULL);
		return L_50;
	}

IL_011d:
	{
		// return new Vector3Int(-y, -x, 0);
		int32_t L_51 = ___y1;
		int32_t L_52 = ___x0;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_53;
		memset((&L_53), 0, sizeof(L_53));
		Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline((&L_53), ((-L_51)), ((-L_52)), 0, /*hidden argument*/NULL);
		return L_53;
	}

IL_0128:
	{
		// if (isStaggerOdd)
		bool L_54 = V_5;
		if (!L_54)
		{
			goto IL_0135;
		}
	}
	{
		// return new Vector3Int(x, y, 0);
		int32_t L_55 = ___x0;
		int32_t L_56 = ___y1;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_57;
		memset((&L_57), 0, sizeof(L_57));
		Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline((&L_57), L_55, L_56, 0, /*hidden argument*/NULL);
		return L_57;
	}

IL_0135:
	{
		// return new Vector3Int(x, y + 1, 0);
		int32_t L_58 = ___x0;
		int32_t L_59 = ___y1;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_60;
		memset((&L_60), 0, sizeof(L_60));
		Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline((&L_60), L_58, ((int32_t)il2cpp_codegen_add(L_59, 1)), 0, /*hidden argument*/NULL);
		return L_60;
	}

IL_0140:
	{
		// return new Vector3Int(x, y, 0);
		int32_t L_61 = ___x0;
		int32_t L_62 = ___y1;
		Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 L_63;
		memset((&L_63), 0, sizeof(L_63));
		Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline((&L_63), L_61, L_62, 0, /*hidden argument*/NULL);
		return L_63;
	}
}
// System.Void SuperTiled2Unity.SuperMap::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperMap__ctor_mB9C726674494050FF4E16ADEF2C2B8B736379020 (SuperMap_tBBCDA02EE1A6B1D4E3B0B7D85C4D6A129280177D* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Color SuperTiled2Unity.SuperObject::CalculateColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F SuperObject_CalculateColor_m133FE769F9023B882EBBF47BE11813528C31EAD8 (SuperObject_t69DFDC213711BB2C894AB42B71BFF3238035C06E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* V_1 = NULL;
	int32_t V_2 = 0;
	SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* V_3 = NULL;
	{
		// Color color = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		V_0 = L_0;
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_2;
		L_2 = GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140(L_1, GameObject_GetComponentsInParent_TisSuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76_m8D68AE72E9EE146914B86B5F87F05183B890E140_RuntimeMethod_var);
		V_1 = L_2;
		V_2 = 0;
		goto IL_003c;
	}

IL_0016:
	{
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_3 = V_1;
		int32_t L_4 = V_2;
		int32_t L_5 = L_4;
		SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* L_6 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		V_3 = L_6;
		// color *= layer.m_TintColor;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_7 = V_0;
		SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* L_8 = V_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_9 = L_8->___m_TintColor_10;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_10;
		L_10 = Color_op_Multiply_mF17D278EB0ABC9AEB32E829D5CA98784E0D6B66F_inline(L_7, L_9, NULL);
		V_0 = L_10;
		// color.a *= layer.m_Opacity;
		float* L_11 = (&(&V_0)->___a_3);
		float* L_12 = L_11;
		float L_13 = *((float*)L_12);
		SuperLayer_t40054BB2BD0D7C72172A3E0F5F924958BAFF4E76* L_14 = V_3;
		float L_15 = L_14->___m_Opacity_9;
		*((float*)L_12) = (float)((float)il2cpp_codegen_multiply(L_13, L_15));
		int32_t L_16 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_16, 1));
	}

IL_003c:
	{
		// foreach (var layer in gameObject.GetComponentsInParent<SuperLayer>())
		int32_t L_17 = V_2;
		SuperLayerU5BU5D_t5202E207DDF8475D1A9C9EB45AA474B71DB89B52* L_18 = V_1;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length)))))
		{
			goto IL_0016;
		}
	}
	{
		// return color;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_19 = V_0;
		return L_19;
	}
}
// System.Void SuperTiled2Unity.SuperObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperObject__ctor_m981FD9F134B7AA0EA0DAC7636FA2EEA501F8B8A1 (SuperObject_t69DFDC213711BB2C894AB42B71BFF3238035C06E* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperObjectLayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperObjectLayer__ctor_m30B9FD7AC4E12FA244D88AF73FB53E27DF9CAEA2 (SuperObjectLayer_tE988ACCE14F2B9F055CFE9E0256B2597C0DCC995* __this, const RuntimeMethod* method) 
{
	{
		SuperLayer__ctor_m27C9812BCE3C7EF1A912DAB7F323C8C56BFE82E9(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::GetTransformMatrix(SuperTiled2Unity.FlipFlags,SuperTiled2Unity.MapOrientation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 SuperTile_GetTransformMatrix_m405A1814B7D9CF76DE6D99AFF22A1E04D67FBE77 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* __this, int32_t ___ff0, int32_t ___orientation1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_1;
	memset((&V_1), 0, sizeof(V_1));
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_8;
	memset((&V_8), 0, sizeof(V_8));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_9;
	memset((&V_9), 0, sizeof(V_9));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_10;
	memset((&V_10), 0, sizeof(V_10));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_11;
	memset((&V_11), 0, sizeof(V_11));
	float V_12 = 0.0f;
	{
		// var inversePPU = 1.0f / m_Sprite.pixelsPerUnit;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_0 = __this->___m_Sprite_10;
		float L_1;
		L_1 = Sprite_get_pixelsPerUnit_m5A5984BC298062DF4CD2CB3E8534443FFCF31826(L_0, NULL);
		V_0 = ((float)((1.0f)/L_1));
		// var offset = new Vector2(m_TileOffsetX * inversePPU, -m_TileOffsetY * inversePPU);
		float L_2 = __this->___m_TileOffsetX_15;
		float L_3 = V_0;
		float L_4 = __this->___m_TileOffsetY_16;
		float L_5 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), ((float)il2cpp_codegen_multiply(L_2, L_3)), ((float)il2cpp_codegen_multiply(((-L_4)), L_5)), /*hidden argument*/NULL);
		// var matOffset = Matrix4x4.Translate(offset);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_6, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_8;
		L_8 = Matrix4x4_Translate_m95D44EDD1A9856DD11C639692E47B7A35EF745E2(L_7, NULL);
		V_1 = L_8;
		// if (ff == FlipFlags.None)
		int32_t L_9 = ___ff0;
		if (L_9)
		{
			goto IL_0038;
		}
	}
	{
		// return matOffset;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_10 = V_1;
		return L_10;
	}

IL_0038:
	{
		// bool flipHorizontal = FlipFlagsMask.FlippedHorizontally(ff);
		int32_t L_11 = ___ff0;
		bool L_12;
		L_12 = FlipFlagsMask_FlippedHorizontally_mEEFD6BFA6D3842CB5425125A29735904B90F03A1(L_11, NULL);
		V_2 = L_12;
		// bool flipVertical = FlipFlagsMask.FlippedVertically(ff);
		int32_t L_13 = ___ff0;
		bool L_14;
		L_14 = FlipFlagsMask_FlippedVertically_mB596859EF886CE6219BC0B4E5697260B598269C3(L_13, NULL);
		V_3 = L_14;
		// bool flipDiagonal = FlipFlagsMask.RotatedDiagonally(ff);
		int32_t L_15 = ___ff0;
		bool L_16;
		L_16 = FlipFlagsMask_RotatedDiagonally_mECF1DF8F2B9DC31F7F2060FA362BDC8D7C47D2F5(L_15, NULL);
		V_4 = L_16;
		// bool rotateHex120 = FlipFlagsMask.RotatedHexagonally120(ff);
		int32_t L_17 = ___ff0;
		bool L_18;
		L_18 = FlipFlagsMask_RotatedHexagonally120_m47701C0290627A54236243EF6079E27F53FF6B89(L_17, NULL);
		V_5 = L_18;
		// float width = m_Width * inversePPU;
		float L_19 = __this->___m_Width_13;
		float L_20 = V_0;
		V_6 = ((float)il2cpp_codegen_multiply(L_19, L_20));
		// float height = m_Height * inversePPU;
		float L_21 = __this->___m_Height_14;
		float L_22 = V_0;
		V_7 = ((float)il2cpp_codegen_multiply(L_21, L_22));
		// var tileCenter = new Vector2(width, height) * 0.5f;
		float L_23 = V_6;
		float L_24 = V_7;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_25;
		memset((&L_25), 0, sizeof(L_25));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_25), L_23, L_24, /*hidden argument*/NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_26;
		L_26 = Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline(L_25, (0.5f), NULL);
		V_8 = L_26;
		// Matrix4x4 matTransIn = Matrix4x4.identity;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_27;
		L_27 = Matrix4x4_get_identity_m94A09872C449C26863FF10D0FDF87842D91BECD6_inline(NULL);
		V_9 = L_27;
		// Matrix4x4 matFlip = Matrix4x4.identity;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_28;
		L_28 = Matrix4x4_get_identity_m94A09872C449C26863FF10D0FDF87842D91BECD6_inline(NULL);
		V_10 = L_28;
		// Matrix4x4 matTransOut = Matrix4x4.identity;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_29;
		L_29 = Matrix4x4_get_identity_m94A09872C449C26863FF10D0FDF87842D91BECD6_inline(NULL);
		V_11 = L_29;
		// matTransIn = Matrix4x4.Translate(-tileCenter);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_30 = V_8;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_31;
		L_31 = Vector2_op_UnaryNegation_m47556D28F72B018AC4D5160710C83A805F10A783_inline(L_30, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32;
		L_32 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_31, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_33;
		L_33 = Matrix4x4_Translate_m95D44EDD1A9856DD11C639692E47B7A35EF745E2(L_32, NULL);
		V_9 = L_33;
		// if (orientation == MapOrientation.Hexagonal)
		int32_t L_34 = ___orientation1;
		if ((!(((uint32_t)L_34) == ((uint32_t)3))))
		{
			goto IL_0101;
		}
	}
	{
		// if (flipDiagonal)
		bool L_35 = V_4;
		if (!L_35)
		{
			goto IL_00bd;
		}
	}
	{
		// matFlip *= Rotate60Matrix;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_36 = V_10;
		il2cpp_codegen_runtime_class_init_inline(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_37 = ((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___Rotate60Matrix_7;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_38;
		L_38 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_36, L_37, NULL);
		V_10 = L_38;
	}

IL_00bd:
	{
		// if (rotateHex120)
		bool L_39 = V_5;
		if (!L_39)
		{
			goto IL_00cf;
		}
	}
	{
		// matFlip *= Rotate120Matrix;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_40 = V_10;
		il2cpp_codegen_runtime_class_init_inline(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_41 = ((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___Rotate120Matrix_8;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_42;
		L_42 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_40, L_41, NULL);
		V_10 = L_42;
	}

IL_00cf:
	{
		// if (flipHorizontal)
		bool L_43 = V_2;
		if (!L_43)
		{
			goto IL_00e0;
		}
	}
	{
		// matFlip *= HorizontalFlipMatrix;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_44 = V_10;
		il2cpp_codegen_runtime_class_init_inline(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_45 = ((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___HorizontalFlipMatrix_4;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_46;
		L_46 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_44, L_45, NULL);
		V_10 = L_46;
	}

IL_00e0:
	{
		// if (flipVertical)
		bool L_47 = V_3;
		if (!L_47)
		{
			goto IL_00f1;
		}
	}
	{
		// matFlip *= VerticalFlipMatrix;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_48 = V_10;
		il2cpp_codegen_runtime_class_init_inline(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_49 = ((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___VerticalFlipMatrix_5;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_50;
		L_50 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_48, L_49, NULL);
		V_10 = L_50;
	}

IL_00f1:
	{
		// matTransOut = Matrix4x4.Translate(tileCenter);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_51 = V_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_52;
		L_52 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_51, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_53;
		L_53 = Matrix4x4_Translate_m95D44EDD1A9856DD11C639692E47B7A35EF745E2(L_52, NULL);
		V_11 = L_53;
		goto IL_017e;
	}

IL_0101:
	{
		// if (flipHorizontal)
		bool L_54 = V_2;
		if (!L_54)
		{
			goto IL_0112;
		}
	}
	{
		// matFlip *= HorizontalFlipMatrix;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_55 = V_10;
		il2cpp_codegen_runtime_class_init_inline(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_56 = ((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___HorizontalFlipMatrix_4;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_57;
		L_57 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_55, L_56, NULL);
		V_10 = L_57;
	}

IL_0112:
	{
		// if (flipVertical)
		bool L_58 = V_3;
		if (!L_58)
		{
			goto IL_0123;
		}
	}
	{
		// matFlip *= VerticalFlipMatrix;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_59 = V_10;
		il2cpp_codegen_runtime_class_init_inline(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_60 = ((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___VerticalFlipMatrix_5;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_61;
		L_61 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_59, L_60, NULL);
		V_10 = L_61;
	}

IL_0123:
	{
		// if (flipDiagonal)
		bool L_62 = V_4;
		if (!L_62)
		{
			goto IL_0135;
		}
	}
	{
		// matFlip *= DiagonalFlipMatrix;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_63 = V_10;
		il2cpp_codegen_runtime_class_init_inline(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_64 = ((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___DiagonalFlipMatrix_6;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_65;
		L_65 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_63, L_64, NULL);
		V_10 = L_65;
	}

IL_0135:
	{
		// if (!flipDiagonal)
		bool L_66 = V_4;
		if (L_66)
		{
			goto IL_0149;
		}
	}
	{
		// matTransOut = Matrix4x4.Translate(tileCenter);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_67 = V_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_68;
		L_68 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_67, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_69;
		L_69 = Matrix4x4_Translate_m95D44EDD1A9856DD11C639692E47B7A35EF745E2(L_68, NULL);
		V_11 = L_69;
		goto IL_017e;
	}

IL_0149:
	{
		// float diff = (height - width) * 0.5f;
		float L_70 = V_7;
		float L_71 = V_6;
		V_12 = ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_70, L_71)), (0.5f)));
		// tileCenter.x += diff;
		float* L_72 = (&(&V_8)->___x_0);
		float* L_73 = L_72;
		float L_74 = *((float*)L_73);
		float L_75 = V_12;
		*((float*)L_73) = (float)((float)il2cpp_codegen_add(L_74, L_75));
		// tileCenter.y -= diff;
		float* L_76 = (&(&V_8)->___y_1);
		float* L_77 = L_76;
		float L_78 = *((float*)L_77);
		float L_79 = V_12;
		*((float*)L_77) = (float)((float)il2cpp_codegen_subtract(L_78, L_79));
		// matTransOut = Matrix4x4.Translate(tileCenter);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_80 = V_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_81;
		L_81 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_80, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_82;
		L_82 = Matrix4x4_Translate_m95D44EDD1A9856DD11C639692E47B7A35EF745E2(L_81, NULL);
		V_11 = L_82;
	}

IL_017e:
	{
		// return matOffset * matTransOut * matFlip * matTransIn;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_83 = V_1;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_84 = V_11;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_85;
		L_85 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_83, L_84, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_86 = V_10;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_87;
		L_87 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_85, L_86, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_88 = V_9;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_89;
		L_89 = Matrix4x4_op_Multiply_m7649669D493400913FF60AFB04B1C19F14E0FDB0(L_87, L_88, NULL);
		return L_89;
	}
}
// System.Void SuperTiled2Unity.SuperTile::GetTRS(SuperTiled2Unity.FlipFlags,SuperTiled2Unity.MapOrientation,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperTile_GetTRS_m525BD60277E27D9F0D26C5FB7AE5514074AE7BE9 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* __this, int32_t ___flags0, int32_t ___orientation1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___xfTranslate2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___xfRotate3, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___xfScale4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CGetTRSU3Eb__17_2_mD578367B5F4AC01E5653744DFA0E0BEBDFD38916_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CGetTRSU3Eb__17_3_m7C98132BC1339274334A35C8DFE5FA4078753870_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__0_m75AC6BEE8A89E221D9AF18BADEACA6EBC800448B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__1_mA2D7CBC425D13D83AF72FF71A1381AFBC1C389F5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* V_7 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_8;
	memset((&V_8), 0, sizeof(V_8));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_9;
	memset((&V_9), 0, sizeof(V_9));
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B10_0 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B9_0 = NULL;
	float G_B11_0 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B11_1 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B13_0 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B12_0 = NULL;
	float G_B14_0 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* G_B14_1 = NULL;
	Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* G_B18_0 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* G_B18_1 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* G_B18_2 = NULL;
	Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* G_B17_0 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* G_B17_1 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* G_B17_2 = NULL;
	Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* G_B20_0 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* G_B20_1 = NULL;
	Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* G_B19_0 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* G_B19_1 = NULL;
	{
		// float inversePPU = 1.0f / m_Sprite.pixelsPerUnit;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_0 = __this->___m_Sprite_10;
		float L_1;
		L_1 = Sprite_get_pixelsPerUnit_m5A5984BC298062DF4CD2CB3E8534443FFCF31826(L_0, NULL);
		V_0 = ((float)((1.0f)/L_1));
		// float width = m_Width * inversePPU;
		float L_2 = __this->___m_Width_13;
		float L_3 = V_0;
		V_1 = ((float)il2cpp_codegen_multiply(L_2, L_3));
		// float height = m_Height * inversePPU;
		float L_4 = __this->___m_Height_14;
		float L_5 = V_0;
		V_2 = ((float)il2cpp_codegen_multiply(L_4, L_5));
		// bool flippedHorizontally = FlipFlagsMask.FlippedHorizontally(flags);
		int32_t L_6 = ___flags0;
		bool L_7;
		L_7 = FlipFlagsMask_FlippedHorizontally_mEEFD6BFA6D3842CB5425125A29735904B90F03A1(L_6, NULL);
		V_3 = L_7;
		// bool flippedVertically = FlipFlagsMask.FlippedVertically(flags);
		int32_t L_8 = ___flags0;
		bool L_9;
		L_9 = FlipFlagsMask_FlippedVertically_mB596859EF886CE6219BC0B4E5697260B598269C3(L_8, NULL);
		V_4 = L_9;
		// bool rotatedDiagonally = FlipFlagsMask.RotatedDiagonally(flags);
		int32_t L_10 = ___flags0;
		bool L_11;
		L_11 = FlipFlagsMask_RotatedDiagonally_mECF1DF8F2B9DC31F7F2060FA362BDC8D7C47D2F5(L_10, NULL);
		V_5 = L_11;
		// bool rotatedHexagonally120 = FlipFlagsMask.RotatedHexagonally120(flags);
		int32_t L_12 = ___flags0;
		bool L_13;
		L_13 = FlipFlagsMask_RotatedHexagonally120_m47701C0290627A54236243EF6079E27F53FF6B89(L_12, NULL);
		V_6 = L_13;
		// xfTranslate = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_14 = ___xfTranslate2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_14 = L_15;
		// xfRotate = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_16 = ___xfRotate3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17;
		L_17 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_16 = L_17;
		// xfScale = Vector3.one;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_18 = ___xfScale4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_19;
		L_19 = Vector3_get_one_mE6A2D5C6578E94268024613B596BF09F990B1260_inline(NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_18 = L_19;
		// if (flags != FlipFlags.None)
		int32_t L_20 = ___flags0;
		if (!L_20)
		{
			goto IL_0261;
		}
	}
	{
		U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* L_21 = (U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass17_0__ctor_m97CDB1D70C523C541D148070D2B82323FE8EEAA6(L_21, NULL);
		V_7 = L_21;
		// if (orientation == MapOrientation.Hexagonal)
		int32_t L_22 = ___orientation1;
		if ((!(((uint32_t)L_22) == ((uint32_t)3))))
		{
			goto IL_00a1;
		}
	}
	{
		// if (rotatedDiagonally)
		bool L_23 = V_5;
		if (!L_23)
		{
			goto IL_008b;
		}
	}
	{
		// xfRotate.z -= 60;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_24 = ___xfRotate3;
		float* L_25 = (&L_24->___z_4);
		float* L_26 = L_25;
		float L_27 = *((float*)L_26);
		*((float*)L_26) = (float)((float)il2cpp_codegen_subtract(L_27, (60.0f)));
	}

IL_008b:
	{
		// if (rotatedHexagonally120)
		bool L_28 = V_6;
		if (!L_28)
		{
			goto IL_00c3;
		}
	}
	{
		// xfRotate.z -= 120;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_29 = ___xfRotate3;
		float* L_30 = (&L_29->___z_4);
		float* L_31 = L_30;
		float L_32 = *((float*)L_31);
		*((float*)L_31) = (float)((float)il2cpp_codegen_subtract(L_32, (120.0f)));
		goto IL_00c3;
	}

IL_00a1:
	{
		// else if (rotatedDiagonally)
		bool L_33 = V_5;
		if (!L_33)
		{
			goto IL_00c3;
		}
	}
	{
		// xfRotate.z = -90.0f;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_34 = ___xfRotate3;
		L_34->___z_4 = (-90.0f);
		// flippedHorizontally = FlipFlagsMask.FlippedVertically(flags);
		int32_t L_35 = ___flags0;
		bool L_36;
		L_36 = FlipFlagsMask_FlippedVertically_mB596859EF886CE6219BC0B4E5697260B598269C3(L_35, NULL);
		V_3 = L_36;
		// flippedVertically = !FlipFlagsMask.FlippedHorizontally(flags);
		int32_t L_37 = ___flags0;
		bool L_38;
		L_38 = FlipFlagsMask_FlippedHorizontally_mEEFD6BFA6D3842CB5425125A29735904B90F03A1(L_37, NULL);
		V_4 = (bool)((((int32_t)L_38) == ((int32_t)0))? 1 : 0);
	}

IL_00c3:
	{
		// xfScale.x = flippedHorizontally ? -1.0f : 1.0f;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_39 = ___xfScale4;
		bool L_40 = V_3;
		G_B9_0 = L_39;
		if (L_40)
		{
			G_B10_0 = L_39;
			goto IL_00cf;
		}
	}
	{
		G_B11_0 = (1.0f);
		G_B11_1 = G_B9_0;
		goto IL_00d4;
	}

IL_00cf:
	{
		G_B11_0 = (-1.0f);
		G_B11_1 = G_B10_0;
	}

IL_00d4:
	{
		G_B11_1->___x_2 = G_B11_0;
		// xfScale.y = flippedVertically ? -1.0f : 1.0f;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_41 = ___xfScale4;
		bool L_42 = V_4;
		G_B12_0 = L_41;
		if (L_42)
		{
			G_B13_0 = L_41;
			goto IL_00e6;
		}
	}
	{
		G_B14_0 = (1.0f);
		G_B14_1 = G_B12_0;
		goto IL_00eb;
	}

IL_00e6:
	{
		G_B14_0 = (-1.0f);
		G_B14_1 = G_B13_0;
	}

IL_00eb:
	{
		G_B14_1->___y_3 = G_B14_0;
		// var matScale = Matrix4x4.Scale(xfScale);
		U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* L_43 = V_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_44 = ___xfScale4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_45 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_44);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_46;
		L_46 = Matrix4x4_Scale_m389397AD581D1BB1A5D39B47021DD685A1EAA9AB(L_45, NULL);
		L_43->___matScale_0 = L_46;
		// var matRotate = Matrix4x4.Rotate(Quaternion.Euler(xfRotate));
		U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* L_47 = V_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_48 = ___xfRotate3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_49 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_48);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_50;
		L_50 = Quaternion_Euler_m66E346161C9778DF8486DB4FE823D8F81A54AF1D_inline(L_49, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_51;
		L_51 = Matrix4x4_Rotate_mE2C31B51EEC282F2969B9C2BE24BD73E312807E8(L_50, NULL);
		L_47->___matRotate_1 = L_51;
		// if (orientation == MapOrientation.Hexagonal)
		int32_t L_52 = ___orientation1;
		if ((!(((uint32_t)L_52) == ((uint32_t)3))))
		{
			goto IL_0183;
		}
	}
	{
		// var anchor = new Vector3(width * 0.5f, height * 0.5f);
		float L_53 = V_1;
		float L_54 = V_2;
		Vector3__ctor_m5F87930F9B0828E5652E2D9D01ED907C01122C86_inline((&V_8), ((float)il2cpp_codegen_multiply(L_53, (0.5f))), ((float)il2cpp_codegen_multiply(L_54, (0.5f))), NULL);
		// var transformed = matScale.MultiplyPoint(anchor);
		U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* L_55 = V_7;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* L_56 = (&L_55->___matScale_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_57 = V_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_58;
		L_58 = Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E(L_56, L_57, NULL);
		V_9 = L_58;
		// transformed = matRotate.MultiplyPoint(transformed);
		U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* L_59 = V_7;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* L_60 = (&L_59->___matRotate_1);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_61 = V_9;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_62;
		L_62 = Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E(L_60, L_61, NULL);
		V_9 = L_62;
		// xfTranslate.x = anchor.x - transformed.x;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_63 = ___xfTranslate2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_64 = V_8;
		float L_65 = L_64.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_66 = V_9;
		float L_67 = L_66.___x_2;
		L_63->___x_2 = ((float)il2cpp_codegen_subtract(L_65, L_67));
		// xfTranslate.y = anchor.y - transformed.y;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_68 = ___xfTranslate2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_69 = V_8;
		float L_70 = L_69.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_71 = V_9;
		float L_72 = L_71.___y_3;
		L_68->___y_3 = ((float)il2cpp_codegen_subtract(L_70, L_72));
		goto IL_0261;
	}

IL_0183:
	{
		// var points = new Vector3[]
		// {
		//     new Vector3(0, height, 0),
		//     new Vector3(width, height, 0),
		//     new Vector3(width, 0, 0),
		// };
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_73 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)3);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_74 = L_73;
		float L_75 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_76;
		memset((&L_76), 0, sizeof(L_76));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_76), (0.0f), L_75, (0.0f), /*hidden argument*/NULL);
		(L_74)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_76);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_77 = L_74;
		float L_78 = V_1;
		float L_79 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_80;
		memset((&L_80), 0, sizeof(L_80));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_80), L_78, L_79, (0.0f), /*hidden argument*/NULL);
		(L_77)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_80);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_81 = L_77;
		float L_82 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_83;
		memset((&L_83), 0, sizeof(L_83));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_83), L_82, (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_81)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_83);
		// points = points.Select(p => matScale.MultiplyPoint(p)).ToArray();
		U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* L_84 = V_7;
		Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4* L_85 = (Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4*)il2cpp_codegen_object_new(Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4_il2cpp_TypeInfo_var);
		Func_2__ctor_m3B1BCBC0885F3E93CDC21C75185F09A25FE0CC17(L_85, L_84, (intptr_t)((void*)U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__0_m75AC6BEE8A89E221D9AF18BADEACA6EBC800448B_RuntimeMethod_var), NULL);
		RuntimeObject* L_86;
		L_86 = Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5((RuntimeObject*)L_81, L_85, Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5_RuntimeMethod_var);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_87;
		L_87 = Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75(L_86, Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75_RuntimeMethod_var);
		// points = points.Select(p => matRotate.MultiplyPoint(p)).ToArray();
		U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* L_88 = V_7;
		Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4* L_89 = (Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4*)il2cpp_codegen_object_new(Func_2_t5FAD225BE5BF75673982C9FE55604AC36DBC1DB4_il2cpp_TypeInfo_var);
		Func_2__ctor_m3B1BCBC0885F3E93CDC21C75185F09A25FE0CC17(L_89, L_88, (intptr_t)((void*)U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__1_mA2D7CBC425D13D83AF72FF71A1381AFBC1C389F5_RuntimeMethod_var), NULL);
		RuntimeObject* L_90;
		L_90 = Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5((RuntimeObject*)L_87, L_89, Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m31AD5E2C766F9E61CE9A6AEF7B0B20BD079D0DC5_RuntimeMethod_var);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_91;
		L_91 = Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75(L_90, Enumerable_ToArray_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_m1721B059180EB47DA4C15C07E941CCFE8EEA4E75_RuntimeMethod_var);
		// var minX = points.Select(p => p.x).Min();
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_92 = L_91;
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var);
		Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* L_93 = ((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9__17_2_1;
		Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* L_94 = L_93;
		G_B17_0 = L_94;
		G_B17_1 = L_92;
		G_B17_2 = L_92;
		if (L_94)
		{
			G_B18_0 = L_94;
			G_B18_1 = L_92;
			G_B18_2 = L_92;
			goto IL_0218;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var);
		U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA* L_95 = ((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* L_96 = (Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC*)il2cpp_codegen_object_new(Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC_il2cpp_TypeInfo_var);
		Func_2__ctor_m8BACCCB996FEF1B06E74F85966B4231A9A24DAF0(L_96, L_95, (intptr_t)((void*)U3CU3Ec_U3CGetTRSU3Eb__17_2_mD578367B5F4AC01E5653744DFA0E0BEBDFD38916_RuntimeMethod_var), NULL);
		Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* L_97 = L_96;
		((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9__17_2_1 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9__17_2_1), (void*)L_97);
		G_B18_0 = L_97;
		G_B18_1 = G_B17_1;
		G_B18_2 = G_B17_2;
	}

IL_0218:
	{
		RuntimeObject* L_98;
		L_98 = Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B((RuntimeObject*)G_B18_1, G_B18_0, Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B_RuntimeMethod_var);
		float L_99;
		L_99 = Enumerable_Min_mAD5CE1B44AFFA09AEDCC1F32CE0C00ADAA445D3F(L_98, NULL);
		V_10 = L_99;
		// var minY = points.Select(p => p.y).Min();
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var);
		Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* L_100 = ((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9__17_3_2;
		Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* L_101 = L_100;
		G_B19_0 = L_101;
		G_B19_1 = G_B18_2;
		if (L_101)
		{
			G_B20_0 = L_101;
			G_B20_1 = G_B18_2;
			goto IL_0243;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var);
		U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA* L_102 = ((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* L_103 = (Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC*)il2cpp_codegen_object_new(Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC_il2cpp_TypeInfo_var);
		Func_2__ctor_m8BACCCB996FEF1B06E74F85966B4231A9A24DAF0(L_103, L_102, (intptr_t)((void*)U3CU3Ec_U3CGetTRSU3Eb__17_3_m7C98132BC1339274334A35C8DFE5FA4078753870_RuntimeMethod_var), NULL);
		Func_2_tDC72553AEF8707070A5FFB9D46F144F9BE06A9EC* L_104 = L_103;
		((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9__17_3_2 = L_104;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9__17_3_2), (void*)L_104);
		G_B20_0 = L_104;
		G_B20_1 = G_B19_1;
	}

IL_0243:
	{
		RuntimeObject* L_105;
		L_105 = Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B((RuntimeObject*)G_B20_1, G_B20_0, Enumerable_Select_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF756F92F161D5F77B13C00A7033A7E004467139B_RuntimeMethod_var);
		float L_106;
		L_106 = Enumerable_Min_mAD5CE1B44AFFA09AEDCC1F32CE0C00ADAA445D3F(L_105, NULL);
		V_11 = L_106;
		// xfTranslate.x = -minX;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_107 = ___xfTranslate2;
		float L_108 = V_10;
		L_107->___x_2 = ((-L_108));
		// xfTranslate.y = -minY;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_109 = ___xfTranslate2;
		float L_110 = V_11;
		L_109->___y_3 = ((-L_110));
	}

IL_0261:
	{
		// xfTranslate.x += m_TileOffsetX * inversePPU;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_111 = ___xfTranslate2;
		float* L_112 = (&L_111->___x_2);
		float* L_113 = L_112;
		float L_114 = *((float*)L_113);
		float L_115 = __this->___m_TileOffsetX_15;
		float L_116 = V_0;
		*((float*)L_113) = (float)((float)il2cpp_codegen_add(L_114, ((float)il2cpp_codegen_multiply(L_115, L_116))));
		// xfTranslate.y -= m_TileOffsetY * inversePPU;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_117 = ___xfTranslate2;
		float* L_118 = (&L_117->___y_3);
		float* L_119 = L_118;
		float L_120 = *((float*)L_119);
		float L_121 = __this->___m_TileOffsetY_16;
		float L_122 = V_0;
		*((float*)L_119) = (float)((float)il2cpp_codegen_subtract(L_120, ((float)il2cpp_codegen_multiply(L_121, L_122))));
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.SuperTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperTile_GetTileData_mD082BBDC5BEFE028C2E0F10CD3BDA41076C4BDE1 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___position0, ITilemap_tCD8B9C2D6A80DB1DFE9C934D91EACE6B8A018164* ___tilemap1, TileData_tFB814629D010ABD175127C0BE96FD96EA606E00F* ___tileData2, const RuntimeMethod* method) 
{
	{
		// tileData.sprite = m_Sprite;
		TileData_tFB814629D010ABD175127C0BE96FD96EA606E00F* L_0 = ___tileData2;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_1 = __this->___m_Sprite_10;
		TileData_set_sprite_m3566544847F9C9C27EDB154324B6FBDB446EFE94(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Boolean SuperTiled2Unity.SuperTile::GetTileAnimationData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileAnimationData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SuperTile_GetTileAnimationData_mA95A8339FF7A3B058B57177AF06DFD971E7559C4 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___position0, ITilemap_tCD8B9C2D6A80DB1DFE9C934D91EACE6B8A018164* ___tilemap1, TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149* ___tileAnimationData2, const RuntimeMethod* method) 
{
	{
		// if (m_AnimationSprites != null && m_AnimationSprites.Length > 1)
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_0 = __this->___m_AnimationSprites_11;
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_1 = __this->___m_AnimationSprites_11;
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))) <= ((int32_t)1)))
		{
			goto IL_0037;
		}
	}
	{
		// tileAnimationData.animatedSprites = m_AnimationSprites;
		TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149* L_2 = ___tileAnimationData2;
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_3 = __this->___m_AnimationSprites_11;
		TileAnimationData_set_animatedSprites_m315FE8DAB5071E1FA594AEA74B1B66BBF6A5C3E1(L_2, L_3, NULL);
		// tileAnimationData.animationSpeed = 1.0f;
		TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149* L_4 = ___tileAnimationData2;
		TileAnimationData_set_animationSpeed_mE1DB382A9D7F0385D70248A93B998405890D4611(L_4, (1.0f), NULL);
		// tileAnimationData.animationStartTime = 0;
		TileAnimationData_tB7419BC111545576349DD19CAB0DEFD240CAF149* L_5 = ___tileAnimationData2;
		TileAnimationData_set_animationStartTime_mBC2F61289403253C6B43C12576A98654B94A9B40(L_5, (0.0f), NULL);
		// return true;
		return (bool)1;
	}

IL_0037:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void SuperTiled2Unity.SuperTile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperTile__ctor_m23DF3B1098095B93E4F1EDBE23CA3715B3C25863 (SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898* __this, const RuntimeMethod* method) 
{
	{
		TileBase__ctor_mBFD0A0ACF9DB1F08783B9F3F35D4E61C9205D4A2(__this, NULL);
		return;
	}
}
// System.Void SuperTiled2Unity.SuperTile::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperTile__cctor_mCB8B3A4F115B1C1D4852F99C7C0C49911731ABE7 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly Matrix4x4 HorizontalFlipMatrix = MatrixUtils.Rotate2d(-1, 0, 0, 1);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0;
		L_0 = MatrixUtils_Rotate2d_m94977337A24271488FF3F281D11DF921A20079DA((-1.0f), (0.0f), (0.0f), (1.0f), NULL);
		((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___HorizontalFlipMatrix_4 = L_0;
		// private static readonly Matrix4x4 VerticalFlipMatrix = MatrixUtils.Rotate2d(1, 0, 0, -1);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_1;
		L_1 = MatrixUtils_Rotate2d_m94977337A24271488FF3F281D11DF921A20079DA((1.0f), (0.0f), (0.0f), (-1.0f), NULL);
		((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___VerticalFlipMatrix_5 = L_1;
		// private static readonly Matrix4x4 DiagonalFlipMatrix = MatrixUtils.Rotate2d(0, -1, -1, 0);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_2;
		L_2 = MatrixUtils_Rotate2d_m94977337A24271488FF3F281D11DF921A20079DA((0.0f), (-1.0f), (-1.0f), (0.0f), NULL);
		((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___DiagonalFlipMatrix_6 = L_2;
		// private static readonly Matrix4x4 Rotate60Matrix = Matrix4x4.Rotate(Quaternion.Euler(0, 0, -60));
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_3;
		L_3 = Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline((0.0f), (0.0f), (-60.0f), NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_4;
		L_4 = Matrix4x4_Rotate_mE2C31B51EEC282F2969B9C2BE24BD73E312807E8(L_3, NULL);
		((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___Rotate60Matrix_7 = L_4;
		// private static readonly Matrix4x4 Rotate120Matrix = Matrix4x4.Rotate(Quaternion.Euler(0, 0, -120));
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline((0.0f), (0.0f), (-120.0f), NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_6;
		L_6 = Matrix4x4_Rotate_mE2C31B51EEC282F2969B9C2BE24BD73E312807E8(L_5, NULL);
		((SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_StaticFields*)il2cpp_codegen_static_fields_for(SuperTile_t37B84369C233A236EE449F4688B8C78EC94CA898_il2cpp_TypeInfo_var))->___Rotate120Matrix_8 = L_6;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass17_0__ctor_m97CDB1D70C523C541D148070D2B82323FE8EEAA6 (U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// UnityEngine.Vector3 SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::<GetTRS>b__0(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__0_m75AC6BEE8A89E221D9AF18BADEACA6EBC800448B (U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___p0, const RuntimeMethod* method) 
{
	{
		// points = points.Select(p => matScale.MultiplyPoint(p)).ToArray();
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* L_0 = (&__this->___matScale_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___p0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E(L_0, L_1, NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 SuperTiled2Unity.SuperTile/<>c__DisplayClass17_0::<GetTRS>b__1(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 U3CU3Ec__DisplayClass17_0_U3CGetTRSU3Eb__1_mA2D7CBC425D13D83AF72FF71A1381AFBC1C389F5 (U3CU3Ec__DisplayClass17_0_t52C1341387083DB73177D4E6773532EFEC9F4F0E* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___p0, const RuntimeMethod* method) 
{
	{
		// points = points.Select(p => matRotate.MultiplyPoint(p)).ToArray();
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* L_0 = (&__this->___matRotate_1);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___p0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Matrix4x4_MultiplyPoint_m20E910B65693559BFDE99382472D8DD02C862E7E(L_0, L_1, NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperTile/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m7BBB440A1BEAAA8E43FF4405E6C99B2FDC5F74CB (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA* L_0 = (U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA*)il2cpp_codegen_object_new(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mAEBBF578A513958C40FF2051364E82C0A4C8188E(L_0, NULL);
		((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA_il2cpp_TypeInfo_var))->___U3CU3E9_0), (void*)L_0);
		return;
	}
}
// System.Void SuperTiled2Unity.SuperTile/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mAEBBF578A513958C40FF2051364E82C0A4C8188E (U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Single SuperTiled2Unity.SuperTile/<>c::<GetTRS>b__17_2(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec_U3CGetTRSU3Eb__17_2_mD578367B5F4AC01E5653744DFA0E0BEBDFD38916 (U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___p0, const RuntimeMethod* method) 
{
	{
		// var minX = points.Select(p => p.x).Min();
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___p0;
		float L_1 = L_0.___x_2;
		return L_1;
	}
}
// System.Single SuperTiled2Unity.SuperTile/<>c::<GetTRS>b__17_3(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec_U3CGetTRSU3Eb__17_3_m7C98132BC1339274334A35C8DFE5FA4078753870 (U3CU3Ec_t2B231FECACD69ED15DB0BF161CB40111A3679AEA* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___p0, const RuntimeMethod* method) 
{
	{
		// var minY = points.Select(p => p.y).Min();
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___p0;
		float L_1 = L_0.___y_3;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperTileLayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperTileLayer__ctor_m2650C41ACDBE8A29A7B026756BBF24D25D6CF2F0 (SuperTileLayer_t100FD3159D41613E8B5DE0EF08F08629B6FC1B69* __this, const RuntimeMethod* method) 
{
	{
		SuperLayer__ctor_m27C9812BCE3C7EF1A912DAB7F323C8C56BFE82E9(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperTilesAsObjectsTilemap::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperTilesAsObjectsTilemap__ctor_mC9D49805ED4EF2E17780F926EEA6D27F5EC0DC5D (SuperTilesAsObjectsTilemap_t541B0111712AA3121005663AF25E0440918656B4* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.SuperWorld::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperWorld__ctor_mA65BF6084579CC9D390265E18E63A01FE631A94A (SuperWorld_t7FDD3E6EC0C0633E0F9A88892A9C5B500E5DB97B* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SuperTiled2Unity.TileObjectAnimator::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TileObjectAnimator_Update_m6F4A7FF613C3DC5BA9F6F775825C142944299ADC (TileObjectAnimator_t136148EED3B0B64B585CB1A265930B96EF0E821F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// m_Timer += Time.deltaTime;
		float L_0 = __this->___m_Timer_6;
		float L_1;
		L_1 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		__this->___m_Timer_6 = ((float)il2cpp_codegen_add(L_0, L_1));
		// float frameTime = 1.0f / m_AnimationFramerate;
		float L_2 = __this->___m_AnimationFramerate_4;
		V_0 = ((float)((1.0f)/L_2));
		// if (m_Timer > frameTime)
		float L_3 = __this->___m_Timer_6;
		float L_4 = V_0;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0073;
		}
	}
	{
		// m_AnimationIndex++;
		int32_t L_5 = __this->___m_AnimationIndex_7;
		__this->___m_AnimationIndex_7 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		// if (m_AnimationIndex >= m_AnimationSprites.Length)
		int32_t L_6 = __this->___m_AnimationIndex_7;
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_7 = __this->___m_AnimationSprites_5;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))))
		{
			goto IL_004d;
		}
	}
	{
		// m_AnimationIndex = 0;
		__this->___m_AnimationIndex_7 = 0;
	}

IL_004d:
	{
		// var renderer = GetComponent<SpriteRenderer>();
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_8;
		L_8 = Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45(__this, Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var);
		// renderer.sprite = m_AnimationSprites[m_AnimationIndex];
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_9 = __this->___m_AnimationSprites_5;
		int32_t L_10 = __this->___m_AnimationIndex_7;
		int32_t L_11 = L_10;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_12 = (L_9)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_11));
		SpriteRenderer_set_sprite_m7B176E33955108C60CAE21DFC153A0FAC674CB53(L_8, L_12, NULL);
		// m_Timer = frameTime - m_Timer;
		float L_13 = V_0;
		float L_14 = __this->___m_Timer_6;
		__this->___m_Timer_6 = ((float)il2cpp_codegen_subtract(L_13, L_14));
	}

IL_0073:
	{
		// }
		return;
	}
}
// System.Void SuperTiled2Unity.TileObjectAnimator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TileObjectAnimator__ctor_mFBBDEF40740930CACC5A12FA3704E11F7E183B93 (TileObjectAnimator_t136148EED3B0B64B585CB1A265930B96EF0E821F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___zeroVector_2;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___b1;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___a0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___b1;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___v0, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___v0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___v0;
		float L_3 = L_2.___y_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Implicit_m8F73B300CB4E6F9B4EB5FB6130363D76CEAA230B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___v0, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___v0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___v0;
		float L_3 = L_2.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) 
{
	{
		__this->___rgba_0 = 0;
		uint8_t L_0 = ___r0;
		__this->___r_1 = L_0;
		uint8_t L_1 = ___g1;
		__this->___g_2 = L_1;
		uint8_t L_2 = ___b2;
		__this->___b_3 = L_2;
		uint8_t L_3 = ___a3;
		__this->___a_4 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color32_op_Implicit_m203A634DBB77053C9400C68065CA29529103D172_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___c0, const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_0 = ___c0;
		uint8_t L_1 = L_0.___r_1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_2 = ___c0;
		uint8_t L_3 = L_2.___g_2;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_4 = ___c0;
		uint8_t L_5 = L_4.___b_3;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_6 = ___c0;
		uint8_t L_7 = L_6.___a_4;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8;
		memset((&L_8), 0, sizeof(L_8));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_8), ((float)(((float)L_1)/(255.0f))), ((float)(((float)L_3)/(255.0f))), ((float)(((float)L_5)/(255.0f))), ((float)(((float)L_7)/(255.0f))), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_003d;
	}

IL_003d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_magenta_mF552F660CB0E42F18558AD59D516EBAC923F57E2_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* CustomPropertyExtensions_GetValueAsString_m692FA0C6470995727534A812C2FD7B7D1264D577_inline (CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* ___property0, const RuntimeMethod* method) 
{
	{
		// return property.m_Value;
		CustomProperty_t5454553CCA207199B2836950F3835DF672646F0B* L_0 = ___property0;
		String_t* L_1 = L_0->___m_Value_2;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_clear_m8B58EA88C92F7DD2C66F0EC1BCC8AC697D631298_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_get_identity_m94A09872C449C26863FF10D0FDF87842D91BECD6_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0 = ((Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_il2cpp_TypeInfo_var))->___identityMatrix_17;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_op_Multiply_mF17D278EB0ABC9AEB32E829D5CA98784E0D6B66F_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___a0, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___b1, const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___a0;
		float L_1 = L_0.___r_0;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2 = ___b1;
		float L_3 = L_2.___r_0;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4 = ___a0;
		float L_5 = L_4.___g_1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6 = ___b1;
		float L_7 = L_6.___g_1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8 = ___a0;
		float L_9 = L_8.___b_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_10 = ___b1;
		float L_11 = L_10.___b_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_12 = ___a0;
		float L_13 = L_12.___a_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_14 = ___b1;
		float L_15 = L_14.___a_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_16;
		memset((&L_16), 0, sizeof(L_16));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_16), ((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)), ((float)il2cpp_codegen_multiply(L_9, L_11)), ((float)il2cpp_codegen_multiply(L_13, L_15)), /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_003d;
	}

IL_003d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_17 = V_0;
		return L_17;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector3Int_get_y_m42F43000F85D356557CAF03442273E7AA08F7F72_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___m_Y_1;
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int_set_y_mA856F32D1BF187BD4091DDF3C6872FD01F7D3377_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___value0;
		__this->___m_Y_1 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int__ctor_mE06A86999D16FA579A7F2142B872AB7E3695C9E0_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___x0;
		__this->___m_X_0 = L_0;
		int32_t L_1 = ___y1;
		__this->___m_Y_1 = L_1;
		int32_t L_2 = ___z2;
		__this->___m_Z_2 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector3Int_get_x_m21C268D2AA4C03CE35AA49DF6155347C9748054C_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___m_X_0;
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int_set_x_m8745C5976D035EBBAC6F6191B5838D58631D8685_inline (Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___value0;
		__this->___m_X_0 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		float L_2 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___a0;
		float L_4 = L_3.___y_1;
		float L_5 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_UnaryNegation_m47556D28F72B018AC4D5160710C83A805F10A783_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___a0;
		float L_3 = L_2.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_4), ((-L_1)), ((-L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0017;
	}

IL_0017:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___zeroVector_5;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_one_mE6A2D5C6578E94268024613B596BF09F990B1260_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___oneVector_6;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_m66E346161C9778DF8486DB4FE823D8F81A54AF1D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___euler0, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___euler0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_0, (0.0174532924f), NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_2;
		L_2 = Quaternion_Internal_FromEulerRad_m2842B9FFB31CDC0F80B7C2172E22831D11D91E93(L_1, NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m5F87930F9B0828E5652E2D9D01ED907C01122C86_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		__this->___z_4 = (0.0f);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_3, (0.0174532924f), NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Quaternion_Internal_FromEulerRad_m2842B9FFB31CDC0F80B7C2172E22831D11D91E93(L_4, NULL);
		V_0 = L_5;
		goto IL_001b;
	}

IL_001b:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = V_0;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		float L_2 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___a0;
		float L_4 = L_3.___y_3;
		float L_5 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___a0;
		float L_7 = L_6.___z_4;
		float L_8 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
