﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AnimationOffset::Start()
extern void AnimationOffset_Start_mAB48F0F1DB4784086752CC8B83B236CF97446774 (void);
// 0x00000002 System.Void AnimationOffset::Update()
extern void AnimationOffset_Update_m88EF806B8B91E9DC1AE9452DDFE1D9AC9C634347 (void);
// 0x00000003 System.Void AnimationOffset::.ctor()
extern void AnimationOffset__ctor_mFACFFA04E3783D2375C9D14167FCD012B31DD60C (void);
// 0x00000004 System.Void AudioManager::Start()
extern void AudioManager_Start_m3C0FEAF19F58B6D28A9E6D815B3AAF94FEA21B69 (void);
// 0x00000005 System.Void AudioManager::PlayAudio(UnityEngine.AudioClip)
extern void AudioManager_PlayAudio_mA1BA760AD75979871349E13474F0B92B890EC47B (void);
// 0x00000006 System.Void AudioManager::.ctor()
extern void AudioManager__ctor_mA793A9DF6B975D03690B7C953972EFE41AE4D5E6 (void);
// 0x00000007 System.Void AudioManager::<Start>b__7_0()
extern void AudioManager_U3CStartU3Eb__7_0_m59072A4EB2597DEA31FD6DD26C2261772926D720 (void);
// 0x00000008 System.Void AudioManager::<Start>b__7_1()
extern void AudioManager_U3CStartU3Eb__7_1_mA116CA555139E8E5734C655FFADCFE078275D3EC (void);
// 0x00000009 System.Void AudioManager::<Start>b__7_2()
extern void AudioManager_U3CStartU3Eb__7_2_m41603B041D89E71881560EE7E2963F33664563D7 (void);
// 0x0000000A System.Void BarSlots::Start()
extern void BarSlots_Start_m7922E3371296D34E48E408D984EF305614904C9C (void);
// 0x0000000B System.Boolean BarSlots::PlaceOnSlot(System.Int32,UnityEngine.GameObject)
extern void BarSlots_PlaceOnSlot_m6D9F833F194525AD00926AE22E07C4A5FBE4BFE4 (void);
// 0x0000000C UnityEngine.GameObject BarSlots::PickupFromSlot(System.Int32)
extern void BarSlots_PickupFromSlot_mE3CB05C9C8D6974A42684E6121243A37413AAC3E (void);
// 0x0000000D UnityEngine.GameObject BarSlots::CheckSlot(System.Int32)
extern void BarSlots_CheckSlot_m419A341B11874F1F13F4A5BC5648C71656706CB7 (void);
// 0x0000000E System.Void BarSlots::ClearSlot(System.Int32)
extern void BarSlots_ClearSlot_m77982D0A64B0928DDEBD3D480B67AE984A1EEC1F (void);
// 0x0000000F System.Void BarSlots::.ctor()
extern void BarSlots__ctor_mACD7013D4A8B56270BB350CCE7599E1674DAA994 (void);
// 0x00000010 System.Void Customer::Start()
extern void Customer_Start_m1BE1C72CF993A76B1A2811D4CECC8CD51D3DC133 (void);
// 0x00000011 System.Void Customer::Update()
extern void Customer_Update_m7E7D6016AC72D503AC50C5CE7FCF7D16CAA4135E (void);
// 0x00000012 System.Void Customer::SetSpawner(CustomerSpawner)
extern void Customer_SetSpawner_m3924319C4C135CE0957559F9CE98345B7109B363 (void);
// 0x00000013 System.Void Customer::PlaceOrder()
extern void Customer_PlaceOrder_mE98BFF025670AB04ADF4CCD864EEC87F0AC7972E (void);
// 0x00000014 System.Void Customer::OnCollisionEnter2D()
extern void Customer_OnCollisionEnter2D_m11A3432E97F7EA2055290EED7FCFADECE97B22A7 (void);
// 0x00000015 System.Void Customer::OnCollisionExit2D()
extern void Customer_OnCollisionExit2D_m6A118546BF053E162DE275DA5AF26C987BB374E0 (void);
// 0x00000016 System.Void Customer::.ctor()
extern void Customer__ctor_m7BC9A866BB65665A89BFBA768C11B783E462BD01 (void);
// 0x00000017 System.Void Customer::<Start>b__18_0()
extern void Customer_U3CStartU3Eb__18_0_m5D02EAB5155CBC8E56F10AB5A061D8FEB89507BE (void);
// 0x00000018 System.Void CustomerSpawner::Start()
extern void CustomerSpawner_Start_m1885221FED7B13FC114C920D19E2E4CA3868238B (void);
// 0x00000019 System.Void CustomerSpawner::Update()
extern void CustomerSpawner_Update_mB02FE25B268795B9BAE63EF2B318CF5466FBD98B (void);
// 0x0000001A UnityEngine.Vector2 CustomerSpawner::GetNextTarget(Customer,System.Boolean)
extern void CustomerSpawner_GetNextTarget_mD6F7141B4A7B7AD997C5D00628985BEDA7E58DC6 (void);
// 0x0000001B System.Boolean CustomerSpawner::HaveEmptySlot()
extern void CustomerSpawner_HaveEmptySlot_m3FCA41FEC4229AB2FC0114681A46A0C12E48BAE1 (void);
// 0x0000001C UnityEngine.GameObject CustomerSpawner::GetAvailableSlot()
extern void CustomerSpawner_GetAvailableSlot_mF313844BF2B8422A4FEC97451AB523E1646BE083 (void);
// 0x0000001D UnityEngine.Vector2 CustomerSpawner::GetDanceLocation()
extern void CustomerSpawner_GetDanceLocation_mCE0434A51255121AD4064128F9431AEA1CA9F3BB (void);
// 0x0000001E System.Void CustomerSpawner::.ctor()
extern void CustomerSpawner__ctor_m31099B22B7383FC7722DB067C298A5C9C16E56F2 (void);
// 0x0000001F System.Void CustomerSpawner::<Update>b__20_0()
extern void CustomerSpawner_U3CUpdateU3Eb__20_0_m340A5CDB2382AED7F2AEB5F1E968B01A5197D512 (void);
// 0x00000020 System.Void CustomerSpawner/<>c::.cctor()
extern void U3CU3Ec__cctor_m16C4A6DD31FED7B5D25AEF8B895A39C94F771017 (void);
// 0x00000021 System.Void CustomerSpawner/<>c::.ctor()
extern void U3CU3Ec__ctor_m25E109481D0CACD8DEE7EF8E6366CFDB217986D7 (void);
// 0x00000022 System.Void CustomerSpawner/<>c::<Update>b__20_1()
extern void U3CU3Ec_U3CUpdateU3Eb__20_1_m88A81DE663A94F72A7C5D64AE70B1BDAD87400C6 (void);
// 0x00000023 System.Void DataManager::Awake()
extern void DataManager_Awake_mBED5EDC55313601164C69104F2D67B067AC6183E (void);
// 0x00000024 System.Void DataManager::Update()
extern void DataManager_Update_m11CFD8D41031CFD57FAFAA2FDC724881200965E6 (void);
// 0x00000025 System.Void DataManager::.ctor()
extern void DataManager__ctor_mD735B7F80F3DE13E2BB45118CDA7FA619330DCDA (void);
// 0x00000026 System.Void DontDestroy::Awake()
extern void DontDestroy_Awake_m9287C31D3DBDBA0B2656ECE30A671D75C9EE155F (void);
// 0x00000027 System.Void DontDestroy::.ctor()
extern void DontDestroy__ctor_mFFA472546E2A8A3DEDECF67B6D6B24D8C55DD302 (void);
// 0x00000028 System.Void DayStartInput::Start()
extern void DayStartInput_Start_m9EF0AC009DCCE038BF217DD6B4F2A120E6933645 (void);
// 0x00000029 System.Void DayStartInput::Update()
extern void DayStartInput_Update_m1BCEA0731DB76DF46E707329773A7F71832278B8 (void);
// 0x0000002A System.Void DayStartInput::OnPickup()
extern void DayStartInput_OnPickup_mF430BF264118BD9C40219E13B8DDFB531299516C (void);
// 0x0000002B System.Void DayStartInput::.ctor()
extern void DayStartInput__ctor_m156F74BACC6BF226F45D45D62388C313F32A590C (void);
// 0x0000002C System.Void LoseScreenInput::OnPickup()
extern void LoseScreenInput_OnPickup_m6185F5B50821BC0AA07E6F46070F5F181754638E (void);
// 0x0000002D System.Void LoseScreenInput::.ctor()
extern void LoseScreenInput__ctor_mB160D09FBD838F561B41C7135B4BC130986315FD (void);
// 0x0000002E System.Void PlayerControls::Start()
extern void PlayerControls_Start_m31F1265A18F322E8BCD0379BA4ECB57479776660 (void);
// 0x0000002F System.Void PlayerControls::Update()
extern void PlayerControls_Update_m1FB8C4FBC7768E727B22A107B203C853DBECCC44 (void);
// 0x00000030 System.Void PlayerControls::OnMove(UnityEngine.InputSystem.InputValue)
extern void PlayerControls_OnMove_m28B8BA5444DB0ADC39EE5040528CD1A9F69A9706 (void);
// 0x00000031 System.Void PlayerControls::OnPickup(UnityEngine.InputSystem.InputValue)
extern void PlayerControls_OnPickup_m906C24EA0E3C58219398867C6FCDDEE078CB3366 (void);
// 0x00000032 System.Void PlayerControls::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerControls_OnCollisionEnter_m84462695C3F1E1F81138AE8942E78A9191C6893B (void);
// 0x00000033 System.Void PlayerControls::ApplyHoldingDirection()
extern void PlayerControls_ApplyHoldingDirection_mA3573781857F36F513F71DB7A6D3B9EE94CE42A4 (void);
// 0x00000034 System.Void PlayerControls::.ctor()
extern void PlayerControls__ctor_m332F79A4E651845074B3ABFAEF8F6F3604244309 (void);
// 0x00000035 System.Void PlayerControls::<Start>b__17_0(System.String,UnityEngine.Vector3Int)
extern void PlayerControls_U3CStartU3Eb__17_0_mA41C37AB1862C7B0F06CAFDE4AEA88C5828972D4 (void);
// 0x00000036 System.Void ItemManager::Start()
extern void ItemManager_Start_m27E6E757B4015A0ECBEAC17975C553C04BC1EC19 (void);
// 0x00000037 UnityEngine.GameObject ItemManager::GetItem(System.String)
extern void ItemManager_GetItem_m39D1A325A33395ED3F56A7E6C2841FFD111C6B72 (void);
// 0x00000038 System.Void ItemManager::.ctor()
extern void ItemManager__ctor_m678789B8F593735A09C7A4264DDBF60A8F8810DC (void);
// 0x00000039 System.Void BayBreeze::.ctor()
extern void BayBreeze__ctor_m358BDB0FB8FBC22A365968136642C2CDACFC1B43 (void);
// 0x0000003A System.Void BayBreeze::Start()
extern void BayBreeze_Start_m86D6EFEB56FF38FAC1BFB535B16157B87258A95D (void);
// 0x0000003B UnityEngine.GameObject BayBreeze::Interact(System.String)
extern void BayBreeze_Interact_mCC61EB70DD9608BA3DF6BBB49A75FF74B831B934 (void);
// 0x0000003C System.Void BayBreezeUngarnished::.ctor()
extern void BayBreezeUngarnished__ctor_m81E94866B2FFFDD363527855460D002CCCC29C90 (void);
// 0x0000003D System.Void BayBreezeUngarnished::Start()
extern void BayBreezeUngarnished_Start_m7A5403522894B58EB77DBB9296AF8075B143A1B4 (void);
// 0x0000003E UnityEngine.GameObject BayBreezeUngarnished::Interact(System.String)
extern void BayBreezeUngarnished_Interact_m55F852E855C147322D45BA5C17978C573701BF77 (void);
// 0x0000003F System.Void Beer::.ctor()
extern void Beer__ctor_m1B874FD9B23A3FDBD927D6CCB17B087EC39B7A51 (void);
// 0x00000040 System.Void Beer::Start()
extern void Beer_Start_mAE7D29F3BF2F5B74C77C1CFEACE5CD1FC5BC28BF (void);
// 0x00000041 System.Void Beer::Update()
extern void Beer_Update_m2570398071F63AB608A4BC2DC328705DADF511DA (void);
// 0x00000042 UnityEngine.GameObject Beer::Interact(System.String)
extern void Beer_Interact_m1108C46A407B67E1C1B295008E45A9ADAC351AB1 (void);
// 0x00000043 System.Void BlueHawaii::.ctor()
extern void BlueHawaii__ctor_m372AEF8C3FEC117CB6427269F209E2FC7CB2ECEE (void);
// 0x00000044 System.Void BlueHawaii::Start()
extern void BlueHawaii_Start_m6CEE4BC51A47617E34111FA6C0B25977A0803952 (void);
// 0x00000045 UnityEngine.GameObject BlueHawaii::Interact(System.String)
extern void BlueHawaii_Interact_m321E37CC7C9C22FABDBCD6903D7ECA038564236A (void);
// 0x00000046 System.Void BlueHawaiiPineapple::.ctor()
extern void BlueHawaiiPineapple__ctor_m15FCB772DC349E3D90A79BA6B907FC996CA088A4 (void);
// 0x00000047 System.Void BlueHawaiiPineapple::Start()
extern void BlueHawaiiPineapple_Start_m5908E2DED09E89D2F9C167FAA8515EC2BDA29964 (void);
// 0x00000048 UnityEngine.GameObject BlueHawaiiPineapple::Interact(System.String)
extern void BlueHawaiiPineapple_Interact_m157C85D85A377DF2588DB1A67043D0B518708429 (void);
// 0x00000049 System.Void BlueHawaiiUmbrella::.ctor()
extern void BlueHawaiiUmbrella__ctor_m38D79AE4C8C7A97AC55C1DDDDCA2544F96355B6A (void);
// 0x0000004A System.Void BlueHawaiiUmbrella::Start()
extern void BlueHawaiiUmbrella_Start_m94B104089912E59F6CD6D7821390A45D85718B12 (void);
// 0x0000004B UnityEngine.GameObject BlueHawaiiUmbrella::Interact(System.String)
extern void BlueHawaiiUmbrella_Interact_m6D3B867F15758C7233E72D7B74A2D57BE100C0A1 (void);
// 0x0000004C System.Void BlueHawaiiUngarnished::.ctor()
extern void BlueHawaiiUngarnished__ctor_m7217AA189481B90A13AE4CF29BC64227F61C0D0C (void);
// 0x0000004D System.Void BlueHawaiiUngarnished::Start()
extern void BlueHawaiiUngarnished_Start_m3946233AC5FEE042875FCF9D120058710F396D0D (void);
// 0x0000004E UnityEngine.GameObject BlueHawaiiUngarnished::Interact(System.String)
extern void BlueHawaiiUngarnished_Interact_m1BE89A93CB0E4A46652DB8EA84C4A6CBB0D6F7B8 (void);
// 0x0000004F System.Void Cococup::.ctor()
extern void Cococup__ctor_m72C6411A983B9E58A610436CE8BABB14370F429B (void);
// 0x00000050 System.Void Cococup::Start()
extern void Cococup_Start_m5EBEA7D2AA7ABEAAD09E78D32FF6E7E210415175 (void);
// 0x00000051 UnityEngine.GameObject Cococup::Interact(System.String)
extern void Cococup_Interact_m055E5D0874214844084115F0C14BEBA2BBE8EF94 (void);
// 0x00000052 System.Void CococupPineapple::.ctor()
extern void CococupPineapple__ctor_mB43FE627F55B4DDA2F7B8FA1DAB120A5738EE64D (void);
// 0x00000053 System.Void CococupPineapple::Start()
extern void CococupPineapple_Start_m3F7172D3F640DE67F49874EB0762A579B5979A7C (void);
// 0x00000054 UnityEngine.GameObject CococupPineapple::Interact(System.String)
extern void CococupPineapple_Interact_m79ACF77B758E288CDAB6A5684E550098BEB84D64 (void);
// 0x00000055 System.Void CococupRum::.ctor()
extern void CococupRum__ctor_mAE1F6F9AA003550A15EFC773BD988302D404A42E (void);
// 0x00000056 System.Void CococupRum::Start()
extern void CococupRum_Start_mF5D021EA7CA77FA12EA4619E3558B89C63C744ED (void);
// 0x00000057 UnityEngine.GameObject CococupRum::Interact(System.String)
extern void CococupRum_Interact_mB3206E9B522FEF47DC0A780736B66A0BF9E18AD5 (void);
// 0x00000058 System.Void Coconut::.ctor()
extern void Coconut__ctor_mAEE36E6E6BD368656DCAFE3D32B3D47551C97FB8 (void);
// 0x00000059 System.Void Coconut::Start()
extern void Coconut_Start_m151E85CBB0D96FE79283D17E70643CA1C7DC532F (void);
// 0x0000005A UnityEngine.GameObject Coconut::Interact(System.String)
extern void Coconut_Interact_m06841EBA068659B56ACBC89DB4B577F0D44B9FF3 (void);
// 0x0000005B System.Void GreenMarg::.ctor()
extern void GreenMarg__ctor_m5F3CD4BAC7B148D8BEFD823F8EBF8024E9B49384 (void);
// 0x0000005C UnityEngine.GameObject GreenMarg::Interact(System.String)
extern void GreenMarg_Interact_mCA31B599AC9F396D0C47F6E5B07D9B874A5C2AD4 (void);
// 0x0000005D UnityEngine.GameObject Item::Interact(System.String)
// 0x0000005E System.Void Item::.ctor()
extern void Item__ctor_m741D59B05082743C60D2F1149112B571E89CAFAF (void);
// 0x0000005F System.Void ItemList::Start()
extern void ItemList_Start_m7A7021750C12B9D50B1D4512AC7EB3F9641CFC0C (void);
// 0x00000060 System.Void ItemList::Update()
extern void ItemList_Update_m9F043571BA7FDA91764EB35C94D9F0B100C114AC (void);
// 0x00000061 System.Void ItemList::.ctor()
extern void ItemList__ctor_m116C9E744CBF4F62CC7D7F7281A0E0F389A96904 (void);
// 0x00000062 System.Void MargGlass::.ctor()
extern void MargGlass__ctor_m50911687D6E96BB451FB05B753F4336017FD4BE3 (void);
// 0x00000063 System.Void MargGlass::Start()
extern void MargGlass_Start_mE2149B6229A4B8FC06AFC471E26246299260E166 (void);
// 0x00000064 UnityEngine.GameObject MargGlass::Interact(System.String)
extern void MargGlass_Interact_mE7D18B4A4A548F82CDDDE99F45E7536936717FAC (void);
// 0x00000065 System.Void Mojito::.ctor()
extern void Mojito__ctor_mA979AF87575A11803B88EB6DB9BEF8D331D01EC5 (void);
// 0x00000066 System.Void Mojito::Start()
extern void Mojito_Start_mBFE2C41404440BE613DF9B3C59525906A6E0EDF8 (void);
// 0x00000067 UnityEngine.GameObject Mojito::Interact(System.String)
extern void Mojito_Interact_m6E9AE3EB8ED883E5F26992304F61BE15A622373E (void);
// 0x00000068 System.Void MojitoUngarnished::.ctor()
extern void MojitoUngarnished__ctor_mE7872875CDD8364ABFFC120684D14F5424ABFBBD (void);
// 0x00000069 System.Void MojitoUngarnished::Start()
extern void MojitoUngarnished_Start_mF843D1A4B7DB77335871A4B7D4E46C3AD409D84A (void);
// 0x0000006A UnityEngine.GameObject MojitoUngarnished::Interact(System.String)
extern void MojitoUngarnished_Interact_m0A333519D693E394C15D8BBC7E83177163F1FC03 (void);
// 0x0000006B System.Void PinaColada::.ctor()
extern void PinaColada__ctor_mD1436FCD6B371C920BF0057A2F231668430E5FC2 (void);
// 0x0000006C System.Void PinaColada::Start()
extern void PinaColada_Start_m0BB1A04B870CA58663ACFE857032D05516A43F4F (void);
// 0x0000006D UnityEngine.GameObject PinaColada::Interact(System.String)
extern void PinaColada_Interact_mEDDF2EE01D495DC86D669F5E2212E31D09C97AC0 (void);
// 0x0000006E System.Void PinaColadaUngarnished::.ctor()
extern void PinaColadaUngarnished__ctor_m7986A4FD414E91BAD46E5F0CED228366CE7473B4 (void);
// 0x0000006F System.Void PinaColadaUngarnished::Start()
extern void PinaColadaUngarnished_Start_m12E83A2067499D4A733AD0A07524200240C6D3E3 (void);
// 0x00000070 UnityEngine.GameObject PinaColadaUngarnished::Interact(System.String)
extern void PinaColadaUngarnished_Interact_m4EF8CC1D7525F87EDFF7AD56F03408F22DCAF7C0 (void);
// 0x00000071 System.Void RedMarg::.ctor()
extern void RedMarg__ctor_m300B1BF6165A3855383AA842BF758890666F3A52 (void);
// 0x00000072 UnityEngine.GameObject RedMarg::Interact(System.String)
extern void RedMarg_Interact_m5220C421B46AF9093895A6C74A75A5D3D7ECE6E1 (void);
// 0x00000073 System.Void RumShot::.ctor()
extern void RumShot__ctor_m3135D4071413CA1A74E395103ABA814F72924F33 (void);
// 0x00000074 System.Void RumShot::Start()
extern void RumShot_Start_m3217C1FE8317EAF180A2C2E63B61D49A2D6A8D57 (void);
// 0x00000075 UnityEngine.GameObject RumShot::Interact(System.String)
extern void RumShot_Interact_mEBE6533F4D36511AE438F78A5267A37BE1E311F7 (void);
// 0x00000076 System.Void ShotGlass::.ctor()
extern void ShotGlass__ctor_m8287F0104C0025CBAB785C24CA21B377B59F463D (void);
// 0x00000077 System.Void ShotGlass::Start()
extern void ShotGlass_Start_mDD917A51DB8A7A45AE5E9D5B75B53C556CEAC419 (void);
// 0x00000078 UnityEngine.GameObject ShotGlass::Interact(System.String)
extern void ShotGlass_Interact_m781C612B235C9B7D4491D507D1FEA70813A17115 (void);
// 0x00000079 System.Void TallGlass::.ctor()
extern void TallGlass__ctor_m9020B930E3907CCFD303DDBBE20C9862951BB532 (void);
// 0x0000007A System.Void TallGlass::Start()
extern void TallGlass_Start_m47B53680C0435563F2D91485509B559020431796 (void);
// 0x0000007B UnityEngine.GameObject TallGlass::Interact(System.String)
extern void TallGlass_Interact_m89B042A30EC5759CC6EAD3A3634DE65575DA0CDD (void);
// 0x0000007C System.Void TallGlassBlue::.ctor()
extern void TallGlassBlue__ctor_m8F0516DA51848F57C553E0E683FF945EC854ED82 (void);
// 0x0000007D System.Void TallGlassBlue::Start()
extern void TallGlassBlue_Start_m010AC89F3116BD2B3AED497B9E62EC2C089D5789 (void);
// 0x0000007E UnityEngine.GameObject TallGlassBlue::Interact(System.String)
extern void TallGlassBlue_Interact_m265BA14F7AB4B315EEF679BCBA640ABBC7C2C234 (void);
// 0x0000007F System.Void TallGlassBlueRum::.ctor()
extern void TallGlassBlueRum__ctor_m3FD522ACD503A27DA95014CA76A1568F32A40C11 (void);
// 0x00000080 System.Void TallGlassBlueRum::Start()
extern void TallGlassBlueRum_Start_m13C0594A4D036228EF4757506AA052DCCC7932B8 (void);
// 0x00000081 UnityEngine.GameObject TallGlassBlueRum::Interact(System.String)
extern void TallGlassBlueRum_Interact_m03D04939FF098C43DCAEC794766DCEA833F33088 (void);
// 0x00000082 System.Void TallGlassBlueVodka::.ctor()
extern void TallGlassBlueVodka__ctor_mEB82E802BB15127F041BDD7E8BAD0A3C7E243BCC (void);
// 0x00000083 System.Void TallGlassBlueVodka::Start()
extern void TallGlassBlueVodka_Start_mF007AD2BB5CC8F8D99883B87F5D5BA1B398D3A3D (void);
// 0x00000084 UnityEngine.GameObject TallGlassBlueVodka::Interact(System.String)
extern void TallGlassBlueVodka_Interact_m8F3BF7F463FBE0B54FD398DCB7F566939705B405 (void);
// 0x00000085 System.Void TallGlassJuice::.ctor()
extern void TallGlassJuice__ctor_mCC557D862A5EA2C04BCCE0827AD9FC5A7FE8BD70 (void);
// 0x00000086 System.Void TallGlassJuice::Start()
extern void TallGlassJuice_Start_m9EA5D58491C89E5491F5600BEE8BECCCBBFA0348 (void);
// 0x00000087 UnityEngine.GameObject TallGlassJuice::Interact(System.String)
extern void TallGlassJuice_Interact_m678A6706BB0D2DAEA398DCF2F60297F18EE634FF (void);
// 0x00000088 System.Void TallGlassLime::.ctor()
extern void TallGlassLime__ctor_m55AAB2212A68D175DBC9E6CA25FDBAE3F7D272C5 (void);
// 0x00000089 System.Void TallGlassLime::Start()
extern void TallGlassLime_Start_m31159CEA9C7B089C77AE3BB8D74A89C0716265CC (void);
// 0x0000008A UnityEngine.GameObject TallGlassLime::Interact(System.String)
extern void TallGlassLime_Interact_m04B2D83E21106EEC5C41A1975227BA1BBC128D8F (void);
// 0x0000008B System.Void TallGlassLimePineapple::.ctor()
extern void TallGlassLimePineapple__ctor_m1AE7D0A0D977780E0B474EE156FF3CADBA2A7EAA (void);
// 0x0000008C System.Void TallGlassLimePineapple::Start()
extern void TallGlassLimePineapple_Start_m064586DC7716172644384D1A7CBB6D4D1B2E8A2B (void);
// 0x0000008D UnityEngine.GameObject TallGlassLimePineapple::Interact(System.String)
extern void TallGlassLimePineapple_Interact_m23BCF3ED3C69D6D208F5AB16173EA56348A81FE8 (void);
// 0x0000008E System.Void TallGlassLimeRum::.ctor()
extern void TallGlassLimeRum__ctor_m58AAF89655F24C5CB1999796C0E7ACEA370F84BF (void);
// 0x0000008F System.Void TallGlassLimeRum::Start()
extern void TallGlassLimeRum_Start_m4FD19960D409C098E5A11CA631B573B9187ADBB8 (void);
// 0x00000090 UnityEngine.GameObject TallGlassLimeRum::Interact(System.String)
extern void TallGlassLimeRum_Interact_m2C31A1BDCC66D9BD3374049D4E50F75264CD5E61 (void);
// 0x00000091 System.Void TallGlassPineapple::.ctor()
extern void TallGlassPineapple__ctor_m77F950469CBCE61D53A1C79E453065B77CC2AD6A (void);
// 0x00000092 System.Void TallGlassPineapple::Start()
extern void TallGlassPineapple_Start_m10C524DF581B2649A5235996F110A64B389C458D (void);
// 0x00000093 UnityEngine.GameObject TallGlassPineapple::Interact(System.String)
extern void TallGlassPineapple_Interact_mA02E2E7D2568D92CE85300035F597E6B714243DD (void);
// 0x00000094 System.Void TallGlassPineappleRum::.ctor()
extern void TallGlassPineappleRum__ctor_mD0896A694C486B785436C31E4F8A26DB7F6BB964 (void);
// 0x00000095 System.Void TallGlassPineappleRum::Start()
extern void TallGlassPineappleRum_Start_mD5309B36C00F6F9B9861192EC34027293CF952C5 (void);
// 0x00000096 UnityEngine.GameObject TallGlassPineappleRum::Interact(System.String)
extern void TallGlassPineappleRum_Interact_m7F9247990B6358139EB2BD619AB5E74590BFE8F4 (void);
// 0x00000097 System.Void TallGlassRum::.ctor()
extern void TallGlassRum__ctor_m77BA2365D4D70E1749120943C9682687E1174D5A (void);
// 0x00000098 System.Void TallGlassRum::Start()
extern void TallGlassRum_Start_m6C271BED22481C089BEC59EF2A0A30F8F6623F50 (void);
// 0x00000099 UnityEngine.GameObject TallGlassRum::Interact(System.String)
extern void TallGlassRum_Interact_m7ABE10D2AB92293A7A2A13375D99A81640D93A60 (void);
// 0x0000009A System.Void TallGlassRumVodka::.ctor()
extern void TallGlassRumVodka__ctor_mBE5119746D385738E9EF0D886E024AA9A67F4CC7 (void);
// 0x0000009B System.Void TallGlassRumVodka::Start()
extern void TallGlassRumVodka_Start_m90F5D7AF570316D99234791CB6247142FD747E95 (void);
// 0x0000009C UnityEngine.GameObject TallGlassRumVodka::Interact(System.String)
extern void TallGlassRumVodka_Interact_m1BCB22B660DA40E67B9ACC188B9BE7B3668E6205 (void);
// 0x0000009D System.Void TallGlassVodka::.ctor()
extern void TallGlassVodka__ctor_m45550C204A424F98A83471E4A8922C42DE6DE6CE (void);
// 0x0000009E System.Void TallGlassVodka::Start()
extern void TallGlassVodka_Start_m89F68782844586E12C88DA3F57C4BA7A6F6DDB56 (void);
// 0x0000009F UnityEngine.GameObject TallGlassVodka::Interact(System.String)
extern void TallGlassVodka_Interact_m748B2EF4F352C6DC5AE2A44DD9521E5D8BE7FB53 (void);
// 0x000000A0 System.Void VodkaShot::.ctor()
extern void VodkaShot__ctor_m116371B81AFF7AFE3D46E47CFE5E72E7ED3224A2 (void);
// 0x000000A1 System.Void VodkaShot::Start()
extern void VodkaShot_Start_mEBBBB69B7B89FBFC7E37B84B16BCDF30AB223FB2 (void);
// 0x000000A2 UnityEngine.GameObject VodkaShot::Interact(System.String)
extern void VodkaShot_Interact_m509E6C4BF757DA7D831B736E6EC9D451E6EF2957 (void);
// 0x000000A3 System.Void LightingManager::Update()
extern void LightingManager_Update_mEC759E1396607F39BCFF255009CD69FB33EFE412 (void);
// 0x000000A4 System.Void LightingManager::.ctor()
extern void LightingManager__ctor_mD6D6D5B3FD3C84741257E07AE072EDA1C6FBC9DC (void);
// 0x000000A5 System.Void MainMenu::Start()
extern void MainMenu_Start_m1729BDE6D096D9F4C92DBE72B392BA89E9A9ECAD (void);
// 0x000000A6 System.Void MainMenu::Update()
extern void MainMenu_Update_m6D9E8EB1A42CC68CFAA865B9CF18FAEB81595C5C (void);
// 0x000000A7 System.Void MainMenu::OnPlay()
extern void MainMenu_OnPlay_mE4D27394B42386CE1073CDAD5430748B8FF679DE (void);
// 0x000000A8 System.Void MainMenu::OnStory()
extern void MainMenu_OnStory_mC31BA14A04F9040E263B1B6493AA9701AFF4061E (void);
// 0x000000A9 System.Void MainMenu::OnEndless()
extern void MainMenu_OnEndless_mDD5FF767A3F2690937BC3BC1A09E8B0B41EEF7D0 (void);
// 0x000000AA System.Void MainMenu::OnOptions()
extern void MainMenu_OnOptions_m8C1537D9BD4A57D9D6E89B86004144D2A7FBB2B4 (void);
// 0x000000AB System.Void MainMenu::OnEasy()
extern void MainMenu_OnEasy_m1EFFD8E28D9C4496742063E4DF5F0D9824CD8078 (void);
// 0x000000AC System.Void MainMenu::OnNormal()
extern void MainMenu_OnNormal_mE0642EF7A1F4DD5E8BCE3418B82280903D9B54E6 (void);
// 0x000000AD System.Void MainMenu::OnHard()
extern void MainMenu_OnHard_mEAEDC19EE6309973B462FEAC9F174504C43CE08C (void);
// 0x000000AE System.Void MainMenu::OnOptionsClose()
extern void MainMenu_OnOptionsClose_m622D8E36991E9753F7B3B7D64747D56F75A18E3F (void);
// 0x000000AF System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m8209CEC1D907C87A96D777961F4D0536E6E948DD (void);
// 0x000000B0 System.Void MapManager::Start()
extern void MapManager_Start_mCDA9E95FE73A0F74ACECEEF9C1EE6AD538ED4DC5 (void);
// 0x000000B1 System.Void MapManager::Update()
extern void MapManager_Update_m3AC546D2067FB939B2DEE8A596590C3A6E1F18E9 (void);
// 0x000000B2 UnityEngine.Tilemaps.Tilemap MapManager::GetInteractable()
extern void MapManager_GetInteractable_m767A80B5CDB8D8FE31197CF75E75F847F54BE463 (void);
// 0x000000B3 System.Void MapManager::.ctor()
extern void MapManager__ctor_m622D319D4B07043DDB690FB88FD531C149C63A7F (void);
// 0x000000B4 System.Void OrderList::Start()
extern void OrderList_Start_m4F224EAB59ECB3D9BFBEAF9AE11165268E35132A (void);
// 0x000000B5 System.Void OrderList::Update()
extern void OrderList_Update_m30E369789CE6CC4D81321A7A3CD087F3D4352D93 (void);
// 0x000000B6 System.Void OrderList::.ctor()
extern void OrderList__ctor_mE0CC33D6F81009797F4C538921244D7B2B359CF0 (void);
// 0x000000B7 System.Void PositionManager::Start()
extern void PositionManager_Start_mA40E4F7EDA4D16A819D0C9E54685D4C13C42CDA2 (void);
// 0x000000B8 System.Void PositionManager::Update()
extern void PositionManager_Update_m9628599F84EB319DB90B5114459FA7A22DD01DDC (void);
// 0x000000B9 System.Void PositionManager::UpdatePlayerPosition(UnityEngine.Vector2,SpriteSwapper/Direction)
extern void PositionManager_UpdatePlayerPosition_m6025117B45819F0F2BE3A815471DFF6A32CFA060 (void);
// 0x000000BA UnityEngine.Vector3Int PositionManager::GetCellPos(UnityEngine.Vector2)
extern void PositionManager_GetCellPos_mC98BA7097AC476846CE8F5B77F9A0A3269B8681F (void);
// 0x000000BB System.Void PositionManager::.ctor()
extern void PositionManager__ctor_m923696030E90B73B0F3BF182EEC9727C462475F2 (void);
// 0x000000BC System.Void ProgressBar::Update()
extern void ProgressBar_Update_m04A17A4B9D006FD9DB6141847BF0E3E1A52ECFAD (void);
// 0x000000BD System.Void ProgressBar::StartCountdown()
extern void ProgressBar_StartCountdown_m27E007133D2577CD3B22095C460E91F2166F869A (void);
// 0x000000BE System.Void ProgressBar::StopCountdown()
extern void ProgressBar_StopCountdown_m9343B668A38FC7617709A0A2ED5482B6CDBFAA39 (void);
// 0x000000BF System.Void ProgressBar::StartIncrease()
extern void ProgressBar_StartIncrease_mC15245796D1CE6034753BC9FDBE1A7128D6BA1EA (void);
// 0x000000C0 System.Void ProgressBar::StopIncrease()
extern void ProgressBar_StopIncrease_m442E8241DC92730D5073B7983556A200D71FE9BB (void);
// 0x000000C1 System.Void ProgressBar::.ctor()
extern void ProgressBar__ctor_mF6AF0DB3866AC00A9C3A84543C332986CC3B9690 (void);
// 0x000000C2 System.Void Slot::.ctor()
extern void Slot__ctor_m67B2CD572C432B806CDC5EEE03B93D62B001CCDB (void);
// 0x000000C3 System.Void SpriteSwapper::Start()
extern void SpriteSwapper_Start_m962D878D5E6127D0626670B78E18212969013EE5 (void);
// 0x000000C4 System.Void SpriteSwapper::Update()
extern void SpriteSwapper_Update_m63C8ACC7E3A8CAB48CD7CBADF1D66C5146596FCD (void);
// 0x000000C5 System.Void SpriteSwapper::SwapSprite(SpriteSwapper/Direction)
extern void SpriteSwapper_SwapSprite_mD189AB0D5196D38C0172C6010EA90E8B5B42C7CF (void);
// 0x000000C6 System.Void SpriteSwapper::.ctor()
extern void SpriteSwapper__ctor_m800AE26A32693B9A8CC781D0DAA947077AA77BB5 (void);
// 0x000000C7 System.Void DayStartScreen::Start()
extern void DayStartScreen_Start_mCF1526A862E03B2EC9EB9A5937B710E784AC84FD (void);
// 0x000000C8 System.Boolean DayStartScreen::MoveToDay()
extern void DayStartScreen_MoveToDay_m2A397F348A590C4DE4D5BCBFC71FC995F03A6518 (void);
// 0x000000C9 System.Void DayStartScreen::.ctor()
extern void DayStartScreen__ctor_m0761454C9E659572914803A9D6FBC101FC4E39DD (void);
// 0x000000CA System.Void LoseManager::Update()
extern void LoseManager_Update_mCA03B35AF292CB0B9696CC8435CC39BF0A92A180 (void);
// 0x000000CB System.Void LoseManager::.ctor()
extern void LoseManager__ctor_m3AB13F7D4106309D8ED49CB88FA85B431057E278 (void);
// 0x000000CC System.Void Typewriter::Awake()
extern void Typewriter_Awake_m4942945505E21C61D3681A353FCAF9E8B4BDE824 (void);
// 0x000000CD System.Void Typewriter::Update()
extern void Typewriter_Update_m5B437A6B6EC8627580A943D2CC83D1580196BAAF (void);
// 0x000000CE System.Void Typewriter::UpdateText()
extern void Typewriter_UpdateText_mCEC8DDC298A15AB37634A2CD55EA6456E3273B1C (void);
// 0x000000CF System.Boolean Typewriter::IsDone()
extern void Typewriter_IsDone_m43C5ACD58377F19C16A484D5715ABF294DE629AD (void);
// 0x000000D0 System.Void Typewriter::Complete()
extern void Typewriter_Complete_mD5C7DDC2E2FD239D121594D8737BEE0D8BD9629A (void);
// 0x000000D1 System.Void Typewriter::.ctor()
extern void Typewriter__ctor_mF04F746490FF9626264CD59208047B32EDBF6D45 (void);
// 0x000000D2 System.Void UIManager::Start()
extern void UIManager_Start_m113F392674AB08A26877728CD36F06332E869080 (void);
// 0x000000D3 System.Void UIManager::Update()
extern void UIManager_Update_m95D2E80B8F461F15C1B9BD6DB0811F5CC18571AB (void);
// 0x000000D4 System.Void UIManager::UpdateHelper(System.String)
extern void UIManager_UpdateHelper_m8942187F8AAABFE788E0C9C34D7C451B17811375 (void);
// 0x000000D5 System.Void UIManager::.ctor()
extern void UIManager__ctor_mC9DC2B8984E76F424E73C1860AD4BD3DEBF6573F (void);
// 0x000000D6 System.Void UIManager::<Start>b__3_0(System.String,UnityEngine.Vector3Int)
extern void UIManager_U3CStartU3Eb__3_0_m9852054DBD210DD81B8757439DEAC5A904E38488 (void);
// 0x000000D7 System.Void WinManager::Start()
extern void WinManager_Start_m9477527F80755BEE0C1F243EE9110DC209FF2774 (void);
// 0x000000D8 System.Void WinManager::Update()
extern void WinManager_Update_m8C4F6E06A088D6704BE3023A7BC1BB2FFB554221 (void);
// 0x000000D9 System.Void WinManager::Go()
extern void WinManager_Go_mD45CE4DC77861DDE18747668B73369DE52068489 (void);
// 0x000000DA System.Void WinManager::.ctor()
extern void WinManager__ctor_mEEAA883F1B3E65D5DD90067B4244554C1EEC58BA (void);
static Il2CppMethodPointer s_methodPointers[218] = 
{
	AnimationOffset_Start_mAB48F0F1DB4784086752CC8B83B236CF97446774,
	AnimationOffset_Update_m88EF806B8B91E9DC1AE9452DDFE1D9AC9C634347,
	AnimationOffset__ctor_mFACFFA04E3783D2375C9D14167FCD012B31DD60C,
	AudioManager_Start_m3C0FEAF19F58B6D28A9E6D815B3AAF94FEA21B69,
	AudioManager_PlayAudio_mA1BA760AD75979871349E13474F0B92B890EC47B,
	AudioManager__ctor_mA793A9DF6B975D03690B7C953972EFE41AE4D5E6,
	AudioManager_U3CStartU3Eb__7_0_m59072A4EB2597DEA31FD6DD26C2261772926D720,
	AudioManager_U3CStartU3Eb__7_1_mA116CA555139E8E5734C655FFADCFE078275D3EC,
	AudioManager_U3CStartU3Eb__7_2_m41603B041D89E71881560EE7E2963F33664563D7,
	BarSlots_Start_m7922E3371296D34E48E408D984EF305614904C9C,
	BarSlots_PlaceOnSlot_m6D9F833F194525AD00926AE22E07C4A5FBE4BFE4,
	BarSlots_PickupFromSlot_mE3CB05C9C8D6974A42684E6121243A37413AAC3E,
	BarSlots_CheckSlot_m419A341B11874F1F13F4A5BC5648C71656706CB7,
	BarSlots_ClearSlot_m77982D0A64B0928DDEBD3D480B67AE984A1EEC1F,
	BarSlots__ctor_mACD7013D4A8B56270BB350CCE7599E1674DAA994,
	Customer_Start_m1BE1C72CF993A76B1A2811D4CECC8CD51D3DC133,
	Customer_Update_m7E7D6016AC72D503AC50C5CE7FCF7D16CAA4135E,
	Customer_SetSpawner_m3924319C4C135CE0957559F9CE98345B7109B363,
	Customer_PlaceOrder_mE98BFF025670AB04ADF4CCD864EEC87F0AC7972E,
	Customer_OnCollisionEnter2D_m11A3432E97F7EA2055290EED7FCFADECE97B22A7,
	Customer_OnCollisionExit2D_m6A118546BF053E162DE275DA5AF26C987BB374E0,
	Customer__ctor_m7BC9A866BB65665A89BFBA768C11B783E462BD01,
	Customer_U3CStartU3Eb__18_0_m5D02EAB5155CBC8E56F10AB5A061D8FEB89507BE,
	CustomerSpawner_Start_m1885221FED7B13FC114C920D19E2E4CA3868238B,
	CustomerSpawner_Update_mB02FE25B268795B9BAE63EF2B318CF5466FBD98B,
	CustomerSpawner_GetNextTarget_mD6F7141B4A7B7AD997C5D00628985BEDA7E58DC6,
	CustomerSpawner_HaveEmptySlot_m3FCA41FEC4229AB2FC0114681A46A0C12E48BAE1,
	CustomerSpawner_GetAvailableSlot_mF313844BF2B8422A4FEC97451AB523E1646BE083,
	CustomerSpawner_GetDanceLocation_mCE0434A51255121AD4064128F9431AEA1CA9F3BB,
	CustomerSpawner__ctor_m31099B22B7383FC7722DB067C298A5C9C16E56F2,
	CustomerSpawner_U3CUpdateU3Eb__20_0_m340A5CDB2382AED7F2AEB5F1E968B01A5197D512,
	U3CU3Ec__cctor_m16C4A6DD31FED7B5D25AEF8B895A39C94F771017,
	U3CU3Ec__ctor_m25E109481D0CACD8DEE7EF8E6366CFDB217986D7,
	U3CU3Ec_U3CUpdateU3Eb__20_1_m88A81DE663A94F72A7C5D64AE70B1BDAD87400C6,
	DataManager_Awake_mBED5EDC55313601164C69104F2D67B067AC6183E,
	DataManager_Update_m11CFD8D41031CFD57FAFAA2FDC724881200965E6,
	DataManager__ctor_mD735B7F80F3DE13E2BB45118CDA7FA619330DCDA,
	DontDestroy_Awake_m9287C31D3DBDBA0B2656ECE30A671D75C9EE155F,
	DontDestroy__ctor_mFFA472546E2A8A3DEDECF67B6D6B24D8C55DD302,
	DayStartInput_Start_m9EF0AC009DCCE038BF217DD6B4F2A120E6933645,
	DayStartInput_Update_m1BCEA0731DB76DF46E707329773A7F71832278B8,
	DayStartInput_OnPickup_mF430BF264118BD9C40219E13B8DDFB531299516C,
	DayStartInput__ctor_m156F74BACC6BF226F45D45D62388C313F32A590C,
	LoseScreenInput_OnPickup_m6185F5B50821BC0AA07E6F46070F5F181754638E,
	LoseScreenInput__ctor_mB160D09FBD838F561B41C7135B4BC130986315FD,
	PlayerControls_Start_m31F1265A18F322E8BCD0379BA4ECB57479776660,
	PlayerControls_Update_m1FB8C4FBC7768E727B22A107B203C853DBECCC44,
	PlayerControls_OnMove_m28B8BA5444DB0ADC39EE5040528CD1A9F69A9706,
	PlayerControls_OnPickup_m906C24EA0E3C58219398867C6FCDDEE078CB3366,
	PlayerControls_OnCollisionEnter_m84462695C3F1E1F81138AE8942E78A9191C6893B,
	PlayerControls_ApplyHoldingDirection_mA3573781857F36F513F71DB7A6D3B9EE94CE42A4,
	PlayerControls__ctor_m332F79A4E651845074B3ABFAEF8F6F3604244309,
	PlayerControls_U3CStartU3Eb__17_0_mA41C37AB1862C7B0F06CAFDE4AEA88C5828972D4,
	ItemManager_Start_m27E6E757B4015A0ECBEAC17975C553C04BC1EC19,
	ItemManager_GetItem_m39D1A325A33395ED3F56A7E6C2841FFD111C6B72,
	ItemManager__ctor_m678789B8F593735A09C7A4264DDBF60A8F8810DC,
	BayBreeze__ctor_m358BDB0FB8FBC22A365968136642C2CDACFC1B43,
	BayBreeze_Start_m86D6EFEB56FF38FAC1BFB535B16157B87258A95D,
	BayBreeze_Interact_mCC61EB70DD9608BA3DF6BBB49A75FF74B831B934,
	BayBreezeUngarnished__ctor_m81E94866B2FFFDD363527855460D002CCCC29C90,
	BayBreezeUngarnished_Start_m7A5403522894B58EB77DBB9296AF8075B143A1B4,
	BayBreezeUngarnished_Interact_m55F852E855C147322D45BA5C17978C573701BF77,
	Beer__ctor_m1B874FD9B23A3FDBD927D6CCB17B087EC39B7A51,
	Beer_Start_mAE7D29F3BF2F5B74C77C1CFEACE5CD1FC5BC28BF,
	Beer_Update_m2570398071F63AB608A4BC2DC328705DADF511DA,
	Beer_Interact_m1108C46A407B67E1C1B295008E45A9ADAC351AB1,
	BlueHawaii__ctor_m372AEF8C3FEC117CB6427269F209E2FC7CB2ECEE,
	BlueHawaii_Start_m6CEE4BC51A47617E34111FA6C0B25977A0803952,
	BlueHawaii_Interact_m321E37CC7C9C22FABDBCD6903D7ECA038564236A,
	BlueHawaiiPineapple__ctor_m15FCB772DC349E3D90A79BA6B907FC996CA088A4,
	BlueHawaiiPineapple_Start_m5908E2DED09E89D2F9C167FAA8515EC2BDA29964,
	BlueHawaiiPineapple_Interact_m157C85D85A377DF2588DB1A67043D0B518708429,
	BlueHawaiiUmbrella__ctor_m38D79AE4C8C7A97AC55C1DDDDCA2544F96355B6A,
	BlueHawaiiUmbrella_Start_m94B104089912E59F6CD6D7821390A45D85718B12,
	BlueHawaiiUmbrella_Interact_m6D3B867F15758C7233E72D7B74A2D57BE100C0A1,
	BlueHawaiiUngarnished__ctor_m7217AA189481B90A13AE4CF29BC64227F61C0D0C,
	BlueHawaiiUngarnished_Start_m3946233AC5FEE042875FCF9D120058710F396D0D,
	BlueHawaiiUngarnished_Interact_m1BE89A93CB0E4A46652DB8EA84C4A6CBB0D6F7B8,
	Cococup__ctor_m72C6411A983B9E58A610436CE8BABB14370F429B,
	Cococup_Start_m5EBEA7D2AA7ABEAAD09E78D32FF6E7E210415175,
	Cococup_Interact_m055E5D0874214844084115F0C14BEBA2BBE8EF94,
	CococupPineapple__ctor_mB43FE627F55B4DDA2F7B8FA1DAB120A5738EE64D,
	CococupPineapple_Start_m3F7172D3F640DE67F49874EB0762A579B5979A7C,
	CococupPineapple_Interact_m79ACF77B758E288CDAB6A5684E550098BEB84D64,
	CococupRum__ctor_mAE1F6F9AA003550A15EFC773BD988302D404A42E,
	CococupRum_Start_mF5D021EA7CA77FA12EA4619E3558B89C63C744ED,
	CococupRum_Interact_mB3206E9B522FEF47DC0A780736B66A0BF9E18AD5,
	Coconut__ctor_mAEE36E6E6BD368656DCAFE3D32B3D47551C97FB8,
	Coconut_Start_m151E85CBB0D96FE79283D17E70643CA1C7DC532F,
	Coconut_Interact_m06841EBA068659B56ACBC89DB4B577F0D44B9FF3,
	GreenMarg__ctor_m5F3CD4BAC7B148D8BEFD823F8EBF8024E9B49384,
	GreenMarg_Interact_mCA31B599AC9F396D0C47F6E5B07D9B874A5C2AD4,
	NULL,
	Item__ctor_m741D59B05082743C60D2F1149112B571E89CAFAF,
	ItemList_Start_m7A7021750C12B9D50B1D4512AC7EB3F9641CFC0C,
	ItemList_Update_m9F043571BA7FDA91764EB35C94D9F0B100C114AC,
	ItemList__ctor_m116C9E744CBF4F62CC7D7F7281A0E0F389A96904,
	MargGlass__ctor_m50911687D6E96BB451FB05B753F4336017FD4BE3,
	MargGlass_Start_mE2149B6229A4B8FC06AFC471E26246299260E166,
	MargGlass_Interact_mE7D18B4A4A548F82CDDDE99F45E7536936717FAC,
	Mojito__ctor_mA979AF87575A11803B88EB6DB9BEF8D331D01EC5,
	Mojito_Start_mBFE2C41404440BE613DF9B3C59525906A6E0EDF8,
	Mojito_Interact_m6E9AE3EB8ED883E5F26992304F61BE15A622373E,
	MojitoUngarnished__ctor_mE7872875CDD8364ABFFC120684D14F5424ABFBBD,
	MojitoUngarnished_Start_mF843D1A4B7DB77335871A4B7D4E46C3AD409D84A,
	MojitoUngarnished_Interact_m0A333519D693E394C15D8BBC7E83177163F1FC03,
	PinaColada__ctor_mD1436FCD6B371C920BF0057A2F231668430E5FC2,
	PinaColada_Start_m0BB1A04B870CA58663ACFE857032D05516A43F4F,
	PinaColada_Interact_mEDDF2EE01D495DC86D669F5E2212E31D09C97AC0,
	PinaColadaUngarnished__ctor_m7986A4FD414E91BAD46E5F0CED228366CE7473B4,
	PinaColadaUngarnished_Start_m12E83A2067499D4A733AD0A07524200240C6D3E3,
	PinaColadaUngarnished_Interact_m4EF8CC1D7525F87EDFF7AD56F03408F22DCAF7C0,
	RedMarg__ctor_m300B1BF6165A3855383AA842BF758890666F3A52,
	RedMarg_Interact_m5220C421B46AF9093895A6C74A75A5D3D7ECE6E1,
	RumShot__ctor_m3135D4071413CA1A74E395103ABA814F72924F33,
	RumShot_Start_m3217C1FE8317EAF180A2C2E63B61D49A2D6A8D57,
	RumShot_Interact_mEBE6533F4D36511AE438F78A5267A37BE1E311F7,
	ShotGlass__ctor_m8287F0104C0025CBAB785C24CA21B377B59F463D,
	ShotGlass_Start_mDD917A51DB8A7A45AE5E9D5B75B53C556CEAC419,
	ShotGlass_Interact_m781C612B235C9B7D4491D507D1FEA70813A17115,
	TallGlass__ctor_m9020B930E3907CCFD303DDBBE20C9862951BB532,
	TallGlass_Start_m47B53680C0435563F2D91485509B559020431796,
	TallGlass_Interact_m89B042A30EC5759CC6EAD3A3634DE65575DA0CDD,
	TallGlassBlue__ctor_m8F0516DA51848F57C553E0E683FF945EC854ED82,
	TallGlassBlue_Start_m010AC89F3116BD2B3AED497B9E62EC2C089D5789,
	TallGlassBlue_Interact_m265BA14F7AB4B315EEF679BCBA640ABBC7C2C234,
	TallGlassBlueRum__ctor_m3FD522ACD503A27DA95014CA76A1568F32A40C11,
	TallGlassBlueRum_Start_m13C0594A4D036228EF4757506AA052DCCC7932B8,
	TallGlassBlueRum_Interact_m03D04939FF098C43DCAEC794766DCEA833F33088,
	TallGlassBlueVodka__ctor_mEB82E802BB15127F041BDD7E8BAD0A3C7E243BCC,
	TallGlassBlueVodka_Start_mF007AD2BB5CC8F8D99883B87F5D5BA1B398D3A3D,
	TallGlassBlueVodka_Interact_m8F3BF7F463FBE0B54FD398DCB7F566939705B405,
	TallGlassJuice__ctor_mCC557D862A5EA2C04BCCE0827AD9FC5A7FE8BD70,
	TallGlassJuice_Start_m9EA5D58491C89E5491F5600BEE8BECCCBBFA0348,
	TallGlassJuice_Interact_m678A6706BB0D2DAEA398DCF2F60297F18EE634FF,
	TallGlassLime__ctor_m55AAB2212A68D175DBC9E6CA25FDBAE3F7D272C5,
	TallGlassLime_Start_m31159CEA9C7B089C77AE3BB8D74A89C0716265CC,
	TallGlassLime_Interact_m04B2D83E21106EEC5C41A1975227BA1BBC128D8F,
	TallGlassLimePineapple__ctor_m1AE7D0A0D977780E0B474EE156FF3CADBA2A7EAA,
	TallGlassLimePineapple_Start_m064586DC7716172644384D1A7CBB6D4D1B2E8A2B,
	TallGlassLimePineapple_Interact_m23BCF3ED3C69D6D208F5AB16173EA56348A81FE8,
	TallGlassLimeRum__ctor_m58AAF89655F24C5CB1999796C0E7ACEA370F84BF,
	TallGlassLimeRum_Start_m4FD19960D409C098E5A11CA631B573B9187ADBB8,
	TallGlassLimeRum_Interact_m2C31A1BDCC66D9BD3374049D4E50F75264CD5E61,
	TallGlassPineapple__ctor_m77F950469CBCE61D53A1C79E453065B77CC2AD6A,
	TallGlassPineapple_Start_m10C524DF581B2649A5235996F110A64B389C458D,
	TallGlassPineapple_Interact_mA02E2E7D2568D92CE85300035F597E6B714243DD,
	TallGlassPineappleRum__ctor_mD0896A694C486B785436C31E4F8A26DB7F6BB964,
	TallGlassPineappleRum_Start_mD5309B36C00F6F9B9861192EC34027293CF952C5,
	TallGlassPineappleRum_Interact_m7F9247990B6358139EB2BD619AB5E74590BFE8F4,
	TallGlassRum__ctor_m77BA2365D4D70E1749120943C9682687E1174D5A,
	TallGlassRum_Start_m6C271BED22481C089BEC59EF2A0A30F8F6623F50,
	TallGlassRum_Interact_m7ABE10D2AB92293A7A2A13375D99A81640D93A60,
	TallGlassRumVodka__ctor_mBE5119746D385738E9EF0D886E024AA9A67F4CC7,
	TallGlassRumVodka_Start_m90F5D7AF570316D99234791CB6247142FD747E95,
	TallGlassRumVodka_Interact_m1BCB22B660DA40E67B9ACC188B9BE7B3668E6205,
	TallGlassVodka__ctor_m45550C204A424F98A83471E4A8922C42DE6DE6CE,
	TallGlassVodka_Start_m89F68782844586E12C88DA3F57C4BA7A6F6DDB56,
	TallGlassVodka_Interact_m748B2EF4F352C6DC5AE2A44DD9521E5D8BE7FB53,
	VodkaShot__ctor_m116371B81AFF7AFE3D46E47CFE5E72E7ED3224A2,
	VodkaShot_Start_mEBBBB69B7B89FBFC7E37B84B16BCDF30AB223FB2,
	VodkaShot_Interact_m509E6C4BF757DA7D831B736E6EC9D451E6EF2957,
	LightingManager_Update_mEC759E1396607F39BCFF255009CD69FB33EFE412,
	LightingManager__ctor_mD6D6D5B3FD3C84741257E07AE072EDA1C6FBC9DC,
	MainMenu_Start_m1729BDE6D096D9F4C92DBE72B392BA89E9A9ECAD,
	MainMenu_Update_m6D9E8EB1A42CC68CFAA865B9CF18FAEB81595C5C,
	MainMenu_OnPlay_mE4D27394B42386CE1073CDAD5430748B8FF679DE,
	MainMenu_OnStory_mC31BA14A04F9040E263B1B6493AA9701AFF4061E,
	MainMenu_OnEndless_mDD5FF767A3F2690937BC3BC1A09E8B0B41EEF7D0,
	MainMenu_OnOptions_m8C1537D9BD4A57D9D6E89B86004144D2A7FBB2B4,
	MainMenu_OnEasy_m1EFFD8E28D9C4496742063E4DF5F0D9824CD8078,
	MainMenu_OnNormal_mE0642EF7A1F4DD5E8BCE3418B82280903D9B54E6,
	MainMenu_OnHard_mEAEDC19EE6309973B462FEAC9F174504C43CE08C,
	MainMenu_OnOptionsClose_m622D8E36991E9753F7B3B7D64747D56F75A18E3F,
	MainMenu__ctor_m8209CEC1D907C87A96D777961F4D0536E6E948DD,
	MapManager_Start_mCDA9E95FE73A0F74ACECEEF9C1EE6AD538ED4DC5,
	MapManager_Update_m3AC546D2067FB939B2DEE8A596590C3A6E1F18E9,
	MapManager_GetInteractable_m767A80B5CDB8D8FE31197CF75E75F847F54BE463,
	MapManager__ctor_m622D319D4B07043DDB690FB88FD531C149C63A7F,
	OrderList_Start_m4F224EAB59ECB3D9BFBEAF9AE11165268E35132A,
	OrderList_Update_m30E369789CE6CC4D81321A7A3CD087F3D4352D93,
	OrderList__ctor_mE0CC33D6F81009797F4C538921244D7B2B359CF0,
	PositionManager_Start_mA40E4F7EDA4D16A819D0C9E54685D4C13C42CDA2,
	PositionManager_Update_m9628599F84EB319DB90B5114459FA7A22DD01DDC,
	PositionManager_UpdatePlayerPosition_m6025117B45819F0F2BE3A815471DFF6A32CFA060,
	PositionManager_GetCellPos_mC98BA7097AC476846CE8F5B77F9A0A3269B8681F,
	PositionManager__ctor_m923696030E90B73B0F3BF182EEC9727C462475F2,
	ProgressBar_Update_m04A17A4B9D006FD9DB6141847BF0E3E1A52ECFAD,
	ProgressBar_StartCountdown_m27E007133D2577CD3B22095C460E91F2166F869A,
	ProgressBar_StopCountdown_m9343B668A38FC7617709A0A2ED5482B6CDBFAA39,
	ProgressBar_StartIncrease_mC15245796D1CE6034753BC9FDBE1A7128D6BA1EA,
	ProgressBar_StopIncrease_m442E8241DC92730D5073B7983556A200D71FE9BB,
	ProgressBar__ctor_mF6AF0DB3866AC00A9C3A84543C332986CC3B9690,
	Slot__ctor_m67B2CD572C432B806CDC5EEE03B93D62B001CCDB,
	SpriteSwapper_Start_m962D878D5E6127D0626670B78E18212969013EE5,
	SpriteSwapper_Update_m63C8ACC7E3A8CAB48CD7CBADF1D66C5146596FCD,
	SpriteSwapper_SwapSprite_mD189AB0D5196D38C0172C6010EA90E8B5B42C7CF,
	SpriteSwapper__ctor_m800AE26A32693B9A8CC781D0DAA947077AA77BB5,
	DayStartScreen_Start_mCF1526A862E03B2EC9EB9A5937B710E784AC84FD,
	DayStartScreen_MoveToDay_m2A397F348A590C4DE4D5BCBFC71FC995F03A6518,
	DayStartScreen__ctor_m0761454C9E659572914803A9D6FBC101FC4E39DD,
	LoseManager_Update_mCA03B35AF292CB0B9696CC8435CC39BF0A92A180,
	LoseManager__ctor_m3AB13F7D4106309D8ED49CB88FA85B431057E278,
	Typewriter_Awake_m4942945505E21C61D3681A353FCAF9E8B4BDE824,
	Typewriter_Update_m5B437A6B6EC8627580A943D2CC83D1580196BAAF,
	Typewriter_UpdateText_mCEC8DDC298A15AB37634A2CD55EA6456E3273B1C,
	Typewriter_IsDone_m43C5ACD58377F19C16A484D5715ABF294DE629AD,
	Typewriter_Complete_mD5C7DDC2E2FD239D121594D8737BEE0D8BD9629A,
	Typewriter__ctor_mF04F746490FF9626264CD59208047B32EDBF6D45,
	UIManager_Start_m113F392674AB08A26877728CD36F06332E869080,
	UIManager_Update_m95D2E80B8F461F15C1B9BD6DB0811F5CC18571AB,
	UIManager_UpdateHelper_m8942187F8AAABFE788E0C9C34D7C451B17811375,
	UIManager__ctor_mC9DC2B8984E76F424E73C1860AD4BD3DEBF6573F,
	UIManager_U3CStartU3Eb__3_0_m9852054DBD210DD81B8757439DEAC5A904E38488,
	WinManager_Start_m9477527F80755BEE0C1F243EE9110DC209FF2774,
	WinManager_Update_m8C4F6E06A088D6704BE3023A7BC1BB2FFB554221,
	WinManager_Go_mD45CE4DC77861DDE18747668B73369DE52068489,
	WinManager__ctor_mEEAA883F1B3E65D5DD90067B4244554C1EEC58BA,
};
static const int32_t s_InvokerIndices[218] = 
{
	6473,
	6473,
	6473,
	6473,
	5102,
	6473,
	6473,
	6473,
	6473,
	6473,
	1656,
	4498,
	4498,
	5073,
	6473,
	6473,
	6473,
	5102,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	2209,
	6245,
	6345,
	6461,
	6473,
	6473,
	11691,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	5102,
	5102,
	5102,
	6473,
	6473,
	2803,
	6473,
	4505,
	6473,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	4505,
	0,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	4505,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6345,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	2868,
	4713,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	5073,
	6473,
	6473,
	6245,
	6473,
	6473,
	6473,
	6473,
	6473,
	6473,
	6245,
	6473,
	6473,
	6473,
	6473,
	5102,
	6473,
	2803,
	6473,
	6473,
	6473,
	6473,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	218,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
